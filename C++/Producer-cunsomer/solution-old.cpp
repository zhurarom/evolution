#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
#include <bitset>
using namespace std;
#endif /* __PROGTEST__ */ 

class CSentinelHacker
{
  public:
    static bool              SeqSolve                      ( const vector<uint64_t> & fragments,
                                                             CBigInt         & res );
    void                     AddTransmitter                ( ATransmitter      x );
    void                     AddReceiver                   ( AReceiver         x );
    void                     AddFragment                   ( uint64_t          x );
    void                     Start                         ( unsigned          thrCount );
    void                     Stop                          ( void );
  private:

    void processWorker();
    void processReceiver(AReceiver receiver);
    void processTransmitter(ATransmitter transmitter);

    vector<thread> m_thread_worker;
    vector<thread> m_thread_receiver;
    vector<thread> m_thread_transmitter;
    vector<AReceiver> m_receiver;
    vector<ATransmitter> m_transmitter;

    //For processWorker
    mutex m_receiver_worker;
    map<uint32_t, std::vector<uint64_t>> m_receiver_worker_map;
    queue<uint32_t> m_receiver_worker_queue;
    condition_variable m_receiver_worker_check;

    //For processTransmitter
    mutex m_worked_transmitter;
    condition_variable m_worked_transmitter_check;
    queue<uint32_t> m_worked_transmitter_queue;
    map<uint32_t, CBigInt> m_worked_transmitter_map;

    //Stop
    bool m_stop = false;
    bool m_receivers_finished = false;
    bool m_workers_finished = false;
    unsigned receiver_counter = 0;
    unsigned worker_counter = 0;
};


void CSentinelHacker::AddFragment(uint64_t x) {
    
    uint32_t id = x >> SHIFT_MSG_ID;

    unique_lock<mutex> locker(m_receiver_worker);
    m_receiver_worker_queue.push(id);
    m_receiver_worker_map[id].push_back(x);

    m_receiver_worker_check.notify_one(); 
}

void CSentinelHacker::AddReceiver(AReceiver x) {
    m_receiver.push_back(x);    
    receiver_counter++;
}

void CSentinelHacker::AddTransmitter(ATransmitter x) {
    m_transmitter.push_back(x);    
}


void CSentinelHacker::processReceiver(AReceiver receiver) {

    uint64_t fragment;

    while(receiver->Recv(fragment)) {
        AddFragment(fragment);
    }
}

void CSentinelHacker::processWorker() {

    while(true) {
        unique_lock<mutex> locker(m_receiver_worker);
        CBigInt res = CBigInt();
        vector<uint64_t> fragments;

        while(!m_receivers_finished && m_receiver_worker_queue.empty())
            m_receiver_worker_check.wait(locker);
        
        if(m_receivers_finished && m_receiver_worker_queue.empty())
        {
            break;
        }

        uint32_t id = m_receiver_worker_queue.front();
        m_receiver_worker_queue.pop();

        fragments = m_receiver_worker_map[id];

        SeqSolve(fragments, res);
        //locker.unlock();

        //TODO: Make divide for Send and Incomplete
        if(!res.IsZero())
        {
            /*cout << "msg: " << id << " fragments: {";
            for(size_t i = 0; i < fragments.size(); i++) 
            {
                cout << hex << fragments.data()[i] << dec << ",";
            }
            cout << "}" << endl;*/
            
            unique_lock<mutex> locker(m_worked_transmitter);
            m_receiver_worker_map.erase(id);
            m_worked_transmitter_queue.push(id);

            m_worked_transmitter_map[id] = res;

            m_worked_transmitter_check.notify_one();
        }
        if(m_receivers_finished && m_receiver_worker_queue.empty())
        {
            break;
        }
    }
}


void CSentinelHacker::processTransmitter(ATransmitter transmitter) {
    
    while(true) {
        unique_lock<mutex> locker(m_worked_transmitter);
        
        while(!m_workers_finished && m_worked_transmitter_queue.empty())
            m_worked_transmitter_check.wait(locker);

        if(m_workers_finished && m_worked_transmitter_queue.empty())
        {
            break;
        }

        uint32_t id = m_worked_transmitter_queue.front();
        m_worked_transmitter_queue.pop();

        CBigInt res = m_worked_transmitter_map[id];
        m_worked_transmitter_map.erase(id);
        
        transmitter->Send(id, res);
        
        if(m_workers_finished && m_worked_transmitter_queue.empty())
        {
            break;
        }
    }
    
    
    while(true) {
        unique_lock<mutex> locker(m_receiver_worker);
        if(m_receiver_worker_map.size() == 0)
            break;
        
        uint32_t id = m_receiver_worker_map.begin()->first;
        m_receiver_worker_map.erase(id);
        
        transmitter->Incomplete(id);

        if(m_receiver_worker_map.size() == 0)
            break;
    }
}
 
void CSentinelHacker::Stop() {
    for(thread& thread_receiver : m_thread_receiver)
        thread_receiver.join();
    m_receivers_finished = true;
    //cout << "Receivers finished" << endl;
    
    m_receiver_worker_check.notify_all();
    for(thread& thread_worker : m_thread_worker)
        thread_worker.join();
    m_workers_finished = true;    
    //cout << "Workers finished" << endl;
    
    m_worked_transmitter_check.notify_all();
    for(thread& thread_transmitter : m_thread_transmitter)
        thread_transmitter.join();
    //cout << "Transmitters finished" << endl;
}

void CSentinelHacker::Start(unsigned thrCount) {
    worker_counter = thrCount;

    for(AReceiver receiver : m_receiver)
        m_thread_receiver.push_back(thread(&CSentinelHacker::processReceiver, this, receiver));

    for(unsigned i = 0; i < thrCount; i++)
        m_thread_worker.push_back(thread(&CSentinelHacker::processWorker, this));

    for(ATransmitter transmitter : m_transmitter)
      m_thread_transmitter.push_back(thread(&CSentinelHacker::processTransmitter, this, transmitter));

}

bool CSentinelHacker::SeqSolve(const vector<uint64_t> &fragments, CBigInt &res) {
    bool found = false;

    FindPermutations(fragments.data(), fragments.size(), [&](const uint8_t *payload, size_t len) {
        CBigInt tmp = CountExpressions(payload + 4, len - 32);
        if (res.CompareTo(tmp) == -1 || !found) 
            res = tmp;
        found = true;
    });
    
    return !res.IsZero();
}
// TODO: CSentinelHacker implementation goes here
//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int                main                                    ( void )
{
  using namespace std::placeholders;
  for ( const auto & x : g_TestSets )
  { 
    CBigInt res;
    assert ( CSentinelHacker::SeqSolve ( x . m_Fragments, res ) );
    assert ( CBigInt ( x . m_Result ) . CompareTo ( res ) == 0 );
  }
  
  CSentinelHacker test;
  auto            trans = make_shared<CExampleTransmitter> ();
  AReceiver       recv  = make_shared<CExampleReceiver> ( initializer_list<uint64_t> { 0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755 } );
  
  test . AddTransmitter ( trans ); 
  test . AddReceiver ( recv ); 
  test . Start ( 3 );
  
  static initializer_list<uint64_t> t1Data = { 0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9, 0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e };
  thread t1 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t1Data );
  
  static initializer_list<uint64_t> t2Data = { 0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb };
  thread t2 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t2Data );
  FragmentSender ( bind ( &CSentinelHacker::AddFragment, &test, _1 ), initializer_list<uint64_t> { 0x017f4cb42a68, 0x02260000000d, 0x072500000025 } );
  t1 . join ();
  t2 . join ();

  test . Stop ();
  assert ( trans -> TotalSent () == 4 );
  assert ( trans -> TotalIncomplete () == 2 );
  return 0;  
}
#endif /* __PROGTEST__ */ 
