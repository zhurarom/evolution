#include <stdlib.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <cstring>
#include <string>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;

void make_free(FILE * file, FILE * file_o, FILE* file_key, unsigned char* privkey) {
    fclose(file);
    fclose(file_o);
    fclose(file_key);
    free(privkey);
}

int Encrypt(const char* Key, const char* inputFile, const char* outputFile, char * cipher_name) {
    OpenSSL_add_all_ciphers();

    // Use it for generate random value of a key and IV
    if (RAND_load_file("/dev/random", 32) != 32) {
        puts("Cannot seed the random generator!");
        exit(1);
    }

    // Init of cipher for generating of in our case we use: "aes-256-cbc" 
    const EVP_CIPHER* cipher = EVP_get_cipherbyname(cipher_name);
    if (!cipher) {
        return 1;
    }


    FILE* file = fopen(inputFile, "rb");
    if (file == NULL) {
        return 1;
    }

    EVP_PKEY* pubkey;
    FILE* file_key = fopen(Key, "rb");
    if (file_key == NULL) {
        fclose(file);
        return 1;
    }

    FILE* file_o = fopen(outputFile, "wb");
    if (file_o == NULL) {
        fclose(file_key);
        fclose(file);
        return 1;
    }

    pubkey = PEM_read_PUBKEY(file_key, NULL, NULL, NULL);
    unsigned char iv[EVP_MAX_IV_LENGTH] = "govoritgav";
    unsigned char* privkey = (unsigned char*)malloc(EVP_PKEY_size(pubkey));
    EVP_CIPHER_CTX* ctx;

    ctx = EVP_CIPHER_CTX_new();
    int privkey_len = 0;

    if (!EVP_SealInit(ctx, cipher, &privkey, &privkey_len, iv, &pubkey, 1)) {
        make_free(file, file_o, file_key, privkey);
        EVP_CIPHER_CTX_free(ctx);
        return 1;
    }

    if (!(fprintf(file_o, "%d\n", EVP_MAX_IV_LENGTH)) || !(fwrite(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, file_o)) || !(fwrite(privkey, sizeof(unsigned char), privkey_len, file_o))) {
        make_free(file, file_o, file_key, privkey);
        EVP_CIPHER_CTX_free(ctx);
        return 1;
    }

    unsigned char OT[256];
    unsigned char ST[256];
    int len_of_OT = 0;
    int len_of_ST = 0;
    int tmp = 0;

    while ((tmp = fread(OT, sizeof(unsigned char), 256, file)) > 0) {
        if (tmp < 0) {
            make_free(file, file_o, file_key, privkey);
            EVP_CIPHER_CTX_free(ctx);
            return 1;
        }

        if (!EVP_SealUpdate(ctx, ST, &len_of_ST, OT, tmp)) {
            make_free(file, file_o, file_key, privkey);
            EVP_CIPHER_CTX_free(ctx);
            return 1;
        }

        if (!(fwrite(ST, sizeof(unsigned char), len_of_ST, file_o))) {
            make_free(file, file_o, file_key, privkey);
            EVP_CIPHER_CTX_free(ctx);
            return 1;
        }

        len_of_OT += len_of_ST;
    }

    if (!EVP_SealFinal(ctx, ST, &len_of_ST)) {
        make_free(file, file_o, file_key, privkey);
        EVP_CIPHER_CTX_free(ctx);
        return 1;
    }

    if (!(fwrite(ST, sizeof(unsigned char), len_of_ST, file_o))) {
        make_free(file, file_o, file_key, privkey);
        EVP_CIPHER_CTX_free(ctx);
        return 1;
    }


    // the end 
    make_free(file, file_o, file_key, privkey);
    cout << "OK" << endl;
    EVP_CIPHER_CTX_free(ctx);
    return 0;
}


int main(int argc, char* argv[]) {
    if(argc != 6) {
        cout << "Error: you should have 5 parametres." << endl;
        return 1;
    }

    if((strcmp(argv[1], "make_encrypt") != 0 && strcmp(argv[5], "aes-256-cbc") != 0)) {
        cout << "Wrong action" << endl;
        return 1;
    }

    if (strcmp(argv[1], "make_encrypt") == 0) {
        if (Encrypt(argv[2], argv[3], argv[4], argv[5]) != 0) {
            cout << "Error e00" << endl;
            return 1;
        }
    }
    return 0;
}
