// Created by: Roman Zhuravskyi - zhurarom

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstring>
#include <openssl/des.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

// enum mode{
//     ecb,
//     cbc,
//     none
// };


//Here we write big endian
void writeBitMap(uint32_t len, ofstream &outFile) {
    char tmp[4];

    tmp[0] = (char) ((len) & 0xFF);
    tmp[1] = (char) ((len >> 8) & 0xFF);
    tmp[2] = (char) ((len >> 16) & 0xFF);
    tmp[3] = (char) ((len >> 24) & 0xFF);

    outFile.write(tmp, 4);
}

//Herce we read big endian
uint32_t readBitMap(ifstream &inputFile) {
    char tmp[4];
    inputFile.read(tmp, 4);

    uint32_t result = (uint32_t)(unsigned char) tmp[0]      |
               (uint32_t)(unsigned char) tmp[1] << 8        |
               (uint32_t)(unsigned char) tmp[2] << 16       |
               (uint32_t)(unsigned char) tmp[3] << 24       ;

    return result;
}

//It will return true if our string was in it
bool makeReplace(string &what, const string &from, const string &to) {
    size_t begin_position = what.find(from);

    // This is a special value equal to the maximum value representable by the type size_type
    if(begin_position == string::npos)
        return false;
    
    what.replace(begin_position, from.length(), to);
    return true;
}

string createFile(string filename, const char * mod, bool action) {
    string outFile = filename;

    // cout << "Create File" << endl;
    if(action) {    //encrypt
        // cout << "Create File encrypt" << endl;
        if(!makeReplace(outFile, ".bmp", "-" + (string)mod +".bmp"))
            outFile += "-" + (string)mod + ".bmp";
    } else {        //decrypt
        // cout << "Create File decrypt" << endl;
        if(!makeReplace(outFile, ".bmp", "-dec.bmp"))
            outFile +=  "-dec.bmp";
    }   

    return outFile;
}

int makeDecryptEncrypt(ifstream &inputFile, uint32_t length, uint32_t begin, 
                const char * mod,    string filename, bool action) {
        
    // cout << "Start makeDecrypt" << endl;

    inputFile.seekg(0, ios::beg);
    char * header = new char[begin];
    inputFile.read(header, begin);

    ofstream outFile;
    outFile.open(createFile(filename, mod, action), ios::binary);
    outFile.write(header, begin);
    delete [] header;

    OpenSSL_add_all_ciphers();
    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();

    if(ctx == NULL)
        exit(2);

    int result;
    unsigned char key[] = "Pes Nikita govorit gav-gav";  // klic pro sifrovani
    unsigned char iv[] = "inicial. vektor";  // inicializacni vektor


    // //Now let`s check mod
    if(!strcmp(mod, "ecb")) {
        result = EVP_CipherInit(ctx, EVP_des_ecb(), key, iv, (int) action);
    } else {
        result = EVP_CipherInit(ctx, EVP_des_cbc(), key, iv, (int) action);
    }

    //Problem
    if(result != 1) {
        cout << "Problem with the encryption or decryption before main loop" << endl;
        return 2;
    }

    uint32_t offset = begin;
    uint32_t size_of_block = 32;
    char * info =  new char[size_of_block];
    unsigned char * cipher = new unsigned char[size_of_block * 3];
    int cipher_len = 0;
    uint32_t new_len = begin;


    while(offset < length) {
        uint32_t read_len = (uint32_t) min(size_of_block, length - offset);
        
        inputFile.read(info, read_len);
        offset += read_len;
        
        result = EVP_CipherUpdate(ctx, cipher, &cipher_len, reinterpret_cast<unsigned char *> (info), read_len);
        
        if(result != 1) {
            cout << "Problem with decrypting or encrypting inside of while" << endl; 
            return 8;
        }

        outFile.write(reinterpret_cast<char *> (cipher), cipher_len);
        new_len += cipher_len;
    }

    result = EVP_CipherFinal(ctx, cipher, &cipher_len);
    if(result != 1) {
        cout << "Problem with decrypting or encrypting after main loop" << endl; 
        return 3;
    }

    outFile.write(reinterpret_cast<char *> (cipher), cipher_len);
    new_len += cipher_len;

    outFile.seekp(2, ios::beg);
    writeBitMap(new_len, outFile);

    delete [] info;
    delete [] cipher;

    inputFile.close();
    outFile.close();

    // cout << "END makeDecrypt" << endl;
    return 0;
}

bool checkIfBitMap(FILE * input) {
    char tmpBM[2];
    if(fread(tmpBM, 1, 2, input) == 2) {
        if(!(tmpBM[0] == 'B' || tmpBM[1] == 'M')) {
            cout << "Not BitMap!" << endl;
            rewind(input);
            return false;
        }
    }
    rewind(input);
    return true;
}

int startProcessing(const char * input, const char * mod, bool action) {
    //Processing of image
    // cout << "StartProcessing" << endl;
    FILE * inputForCheck = fopen(input, "rb");

    if(inputForCheck == NULL)
        return 4;
    
    // cout << "1" << endl;


    if(!checkIfBitMap(inputForCheck)) {
        fclose(inputForCheck);
        return 5;
    }
    // cout << "2" << endl;

    fclose(inputForCheck);

    ifstream inputFile;
    inputFile.open(input, ios::binary | ios::ate);
    
    if(!inputFile.is_open())
        return 1;
    // cout << "3" << endl;
    //Check lenght

    uint64_t lenght = (uint64_t)inputFile.tellg();
    if(lenght < 14) {//Here can`t be a header
        inputFile.close();
        return 2;
    }
    // cout << "4" << endl;

    inputFile.seekg(2, ios::beg);
    uint32_t lenght_act = readBitMap(inputFile);
    if(lenght_act != lenght) {    //incorrect header
        inputFile.close();
        return 3;
    }
    // cout << "5" << endl;


    inputFile.seekg(10, ios::beg);
    uint32_t beginning = readBitMap(inputFile);
    if(beginning > lenght) {    //We start after an end of the file
        inputFile.close();
        return 4;
    }
    // cout << "6" << endl;


    // cout << "END StartProcessing" << endl;
    return makeDecryptEncrypt(inputFile, lenght_act, beginning, mod, input, action);
}

int main(int argc, char * argv[]) {
    
    bool action = false; //false - decrypt; true - encrypt

    //Chech for right count of arguments
    if(argc != 4) 
        return 1;
    
    //Here we make parsing of our arguments
    if(!strcmp(argv[1], "make_decrypt")){
        if(strcmp(argv[3], "ecb") && strcmp(argv[3], "cbc"))
            return 1;
    } else if(!strcmp(argv[1], "make_encrypt")) {
        if(strcmp(argv[3], "ecb") && strcmp(argv[3], "cbc"))
            return 1;
        action = true;
    }

    startProcessing(argv[2], argv[3], action);      //argv[2] - filename
    return 0;
}