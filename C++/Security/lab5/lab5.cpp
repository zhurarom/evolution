#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/ssl.h>
#include <openssl/pem.h>

#define IP "147.32.232.212"
#define BUFFER_LEN 1024
#define HOST "fit.cvut.cz"


void make_read(SSL * ssl, char buff[], FILE * outfile) {
	int result;
	size_t bytes = 0;

	while ((result = SSL_read(ssl, buff, BUFFER_LEN)) > 0) {
		fwrite(buff, sizeof(char), result, outfile);
		bytes += result;
	}

}
 
int main(int argc, char* argv[]) {
	//Create connection
	int sockfd;
	struct sockaddr_in servaddr;

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr(IP); 
	servaddr.sin_port = htons(443);			  

	// TCP connection
	if (0 != connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) {
		perror("Error connect");
		exit(1);
	}

	SSL_library_init();

	SSL_CTX *ctx = SSL_CTX_new(SSLv23_client_method());
	if (!ctx) {
		perror("Error SSL_CTX_new");
		exit(1);
	}

	SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

	SSL *ssl = SSL_new(ctx);
	if (!ssl) {
		perror("Error SSL_new");
		exit(1);
	}

	// Make linking of ssl 
	if (!SSL_set_fd(ssl, sockfd)) {
		perror("Error SSL_set_fd");
		exit(1);
	}

	SSL_set_tlsext_host_name(ssl, HOST);

	//SSL communication
	if (0 >= SSL_connect(ssl)) {
		perror("Error SSL_connect");
		exit(1);
	}

	char buff[BUFFER_LEN] = "GET /student/odkazy HTTP/1.1\r\nHost: fit.cvut.cz\r\nConnection: close\r\n\r\n";

	//SSL request
	if (0 >= SSL_write(ssl, buff, strlen(buff) + 1)) {
		perror("Error SSL_write");
		exit(1);
	}

	//Write data
	FILE *outfile = fopen(argv[1], "w");
	make_read(ssl, buff, outfile);
	fprintf(outfile, "\n");
	fclose(outfile);

	X509 *cert = SSL_get_peer_certificate(ssl);
	if (!cert) {
		perror("Error SSL_get_peer_certificate");
		exit(1);
	}

	FILE *pem = fopen(argv[2], "wb");
	if (!PEM_write_X509(pem, cert)) {
		perror("Error while opening");
		exit(1);
	}

	fclose(pem);
	printf("Cert is in %s\n", argv[2]);

	SSL_shutdown(ssl);
	close(sockfd);
	SSL_free(ssl);
	SSL_CTX_free(ctx);

	return 0;
}