#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string.h>
#include <openssl/evp.h>
using namespace std;

#define len 5

int main(int argc, char* argv[]) {
	char hashFunction[]="sha256";

    printf("----------------------RUNNING OF TASK 1------------------------\n");

	EVP_MD_CTX *ctx;
	const EVP_MD* type;
	unsigned char hash[EVP_MAX_MD_SIZE];
	int length;

	OpenSSL_add_all_digests();
	type = EVP_get_digestbyname(hashFunction);

	
	if(!type) {
		printf("Hash %s neexistuje.\n", hashFunction);
		exit(1);
	}

    ctx = EVP_MD_CTX_create(); // create context for hashing
    if(ctx == NULL) exit(2);

    //Init od our text with size
	unsigned char* text = new unsigned char[len];
	for(int x = 0; x < len; x++)
		text[x] = 0;

	bool helper = false;
	//Here we will make iteration to find a text
    while(true) { 
		text[0]++;
		helper = false;

		if(text[0] == 0)
			helper = true;

		for(int x = 1; x < len; x++) {
			if(helper) {
				text[x]++;
				helper = false;
			}

			if(text[x] == 0)
				helper = true;
		}

		EVP_DigestInit(ctx, type);
		EVP_DigestUpdate(ctx, text, len);
		EVP_DigestFinal(ctx, hash, (unsigned int*) &length);

        if(hash[0] == 0xaa && hash[1] == 0xbb) {
		    cout << "Found text: ";
		    for(int x = 0; x < len; x++)
		    	cout << (int)text[x] << " ";
		    cout << endl;
    	    break;
	    }	
	}

    cout << endl << endl << endl;
    EVP_MD_CTX_destroy(ctx); // destroy the context
	return 0;
}