#include <stdlib.h>
#include <openssl/evp.h>
#include <string.h>
#include <fstream> 
#include <iostream>
#include <iomanip>

using namespace std;

void make_print(unsigned char hash [1024]) {
  	printf("Hash ST: ");
  	for(unsigned i = 0; i < strlen((const char *) hash); i++)
    	printf("%#x ", hash[i]);
}

void make_encrypting( unsigned char a [1024], int *counter) {
    unsigned char ot[1024];
    strcpy((char *) ot, (const char *) a);
    unsigned char st[1024] = "";  // sifrovany text
    unsigned char key[EVP_MAX_KEY_LENGTH] = "Pes Nikita govorit gav gav";  // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial vektor";  // inicializacni vektor

    const char cipherName[] = "RC4";
    const EVP_CIPHER * cipher;  
    OpenSSL_add_all_ciphers();
    /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
    cipher = EVP_get_cipherbyname(cipherName);
    if(!cipher) {
      printf("Sifra %s neexistuje.\n", cipherName);
      exit(1);
    }   

    int otLength = strlen((const char*) ot);
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX *ctx; // struktura pro kontext
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) exit(2);

    printf("Text %d: %s\n", *counter, ot);
    (*counter)++;

    int res;
    /* Sifrovani */
    res = EVP_EncryptInit(ctx, cipher, key, iv);  // nastaveni kontextu pro sifrovani
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx,  st, &stLength, ot, otLength);  // sifrovani ot
    if(res != 1) exit(4);
    res = EVP_EncryptFinal(ctx, st + stLength, &tmpLength);  // ziskani sifrovaneho textu z kontextu
    if(res != 1) exit(5);
    stLength += tmpLength;

    /* Desifrovani */
    res = EVP_DecryptInit(ctx, cipher, key, iv);  // nastaveni kontextu pro desifrovani
    if(res != 1) exit(6);
    res = EVP_DecryptUpdate(ctx, ot, &otLength,  st, stLength);  // desifrovani st
    if(res != 1) exit(7);
    res = EVP_DecryptFinal(ctx, ot + otLength, &tmpLength);  // ziskani desifrovaneho textu z kontextu
    if(res != 1) exit(8);
    otLength += tmpLength;

    /*Make print*/
    make_print(st); 
    printf("\n");
    EVP_CIPHER_CTX_free(ctx);
  
}

int main(void) {
    int counter = 1;
    
    printf("----------------------RUNNING OF TASK 2_1----------------------\n");
    printf("Text for encrypting:\n");
    unsigned char text_1[1024] = "Don't let your dreams be dreams";
    unsigned char text_2[1024] = "The God is exists!";

    //For getting of hash
    make_encrypting(text_1, &counter);
    make_encrypting(text_2, &counter);

    cout << endl << endl << endl;
    return 0;
}