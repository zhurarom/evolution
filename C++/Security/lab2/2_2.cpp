#include <iostream>
#include <openssl/evp.h>
#include <string>
#include <fstream>
#include <stdexcept>
#include <iomanip>
#include <bits/stdc++.h> 

using namespace std;


uint8_t hex_to_val(char hex_digit) {
    switch (hex_digit) {
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        return hex_digit - '0';

    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
        return hex_digit - 'A' + 10;

    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
        return hex_digit - 'a' + 10;
    }
    throw std::invalid_argument("Invalid hex digit");
}

void printText(const string hash) {
    for(unsigned i = 0; i < hash.length(); i+=2 )
        cout << "0x" << hash[i] << hash[i + 1] << " ";
    cout << endl; 
}

int main(int argc, char *argv[]) {
    string ct1;  
    string ct2; 
    string filename;
    ifstream datafile;

    filename = argv[1];
    // datafile.open( filename.c_str() );
    datafile.open(filename);
    if(!datafile)
        return 1;
    
    getline(datafile, ct1, '\n');
    getline(datafile, ct2, '\n');

    datafile.close();

    //Plain text, took from courses
    //Plaine text for input_2.txt : Don't let your dreams be dreams
    string pt;
    ifstream pt_file;
    pt_file.open(argv[2]);

    if(!pt_file)
        return 1;

    getline(pt_file, pt, '\n');
    pt_file.close();

    cout << "Plain text: " << pt << endl;
    
    cout << "Hash 1: ";
    printText(ct1);
    
    cout << "Hash 2: ";
    printText(ct2);

    uint32_t len = (uint32_t) min(min(ct1.length() / 2, ct2.length() / 2), pt.length());

    ofstream answer("output_task_2.txt");
    cout << "Result: ";
    //But first we need to transform it to another format
    for (uint32_t i = 0; i < len; ++i) {
        uint8_t hex_1 = hex_to_val(ct1[2 * i]) * (uint8_t) 16 + hex_to_val(ct1[2 * i + 1]);
        uint8_t hex_2 = hex_to_val(ct2[2 * i]) * (uint8_t) 16 + hex_to_val(ct2[2 * i + 1]);

        cout << (char) (hex_1 ^ hex_2 ^ (uint8_t) pt[i]);
        answer << (char) (hex_1 ^ hex_2 ^ (uint8_t) pt[i]);
    }

    cout << endl << endl << endl;
    answer.close();
    return 0;
}
