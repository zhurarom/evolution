
#ifndef MATRIXCALC_MATRIXBUILDER_H
#define MATRIXCALC_MATRIXBUILDER_H

#include "Matrix.h"
#include "SparseMatrix.h"
#include "DenseMatrix.h"

/**
 * Class for building of a matrix and for identity type of matrix
*/
class MatrixBuilder {
public:
    /**
     * Here we build a matrix using a result from function InternalBuildMatrix
     * @param rows
     * @param columns
     * @param data
     * @return newMatrix 
    */
    static BaseMatrixPtr BuildMatrix(unsigned int rows, unsigned int columns, const RawData& data);
private:
    /**
     * This function make a choice between sparse and dense matrices by comparing count of zero
     * @param rows
     * @param columns
     * @param data
     * @return newMatrix
    */
    static BaseMatrixPtr InternalBuildMatrix(unsigned int rows, unsigned int columns, const RawData& data);
};


#endif //MATRIXCALC_MATRIXBUILDER_H