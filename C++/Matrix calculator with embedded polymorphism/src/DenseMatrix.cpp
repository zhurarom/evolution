#include <iostream>
#include <cmath>
#include <random>
#include "MatrixBuilder.h"
#include "Exception.h"
#include "DenseMatrix.h"

DenseMatrix::DenseMatrix(unsigned int rows, unsigned int cols, const DenceData& dataIn) : BaseMatrix(rows, cols), data(dataIn) {}

double DenseMatrix::At(unsigned int row, unsigned int column) const
{
    unsigned int position = row * cols + column;
    return data.at(position);
}