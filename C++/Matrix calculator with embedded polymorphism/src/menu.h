#ifndef MATRIXCALC_MENU_H
#define MATRIXCALC_MENU_H

#include "MatrixBuilder.h"
#include "Matrix.h"
#include "Exception.h"


/**
 * Basic class is used to represent a main menu
*/
class menu {
    public:
        /**
         * Here we have main menu
        */
        void start();
    private:
        /**
         * Here will check our choice
        */
        int choice = -1;
        /**
         * Count of rows and cols for first matrix
        */
        unsigned int rows1 = 0, cols1 = 0;
        /**
         * Count of rows and cols for first matrix
        */
        unsigned int rows2 = 0, cols2 = 0;    
};

#endif //MATRIXCALC_MENU_H