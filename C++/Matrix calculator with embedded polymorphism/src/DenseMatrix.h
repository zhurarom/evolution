#ifndef MATRIXCALC_DENSEMATRIX_H
#define MATRIXCALC_DENSEMATRIX_H

#include <memory>
#include <vector>
#include <map>
#include "Matrix.h"

/**
 *  Declaration of class BaseMatrix 
*/
class BaseMatrix;

/**
 * Data about dense matrix
*/
typedef std::vector<double> DenceData;

/**
 * Matrix class for dense matrices
 */
class DenseMatrix : public BaseMatrix {
public:
    /**
     * Constructor
     * @param rows
     * @param cols
     * @param dataIn
     */
    DenseMatrix(unsigned int rows, unsigned int cols, const DenceData& dataIn);

private:
    /**
     * @inherit
    */
    virtual double At(unsigned int row, unsigned int column) const;

    /**
     * Data about dense matrix
    */
    DenceData data;
};

#endif  //MATRIXCALC_DENSEMATRIX_H
