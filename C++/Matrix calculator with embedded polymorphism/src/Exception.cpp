#include <iostream>
#include "Exception.h"

void BadDimensionException::message() {
    cout << "Wrog size of matrix" << endl;
}

void InvalidArgument::message() {
    cout << "Determinant not supported for rect matrix" << endl;
}

void DimensionSumException::message() {
    cout << "Dimensions of summable matrices must match" << endl;
}

void DimensionProductException::message() {
    cout << "The number of columns of the first matrix must match the number of rows of the second matrix" << endl;
}

void ZeroDivideException::message() {
    cout << "Division by 0" << endl;
}

void RangeError::message() {
    cout << "Matrix out of bounds" << endl;
}
