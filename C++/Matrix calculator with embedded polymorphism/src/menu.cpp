#include <limits>
#include <iostream>     
#include <sstream>      
#include <cmath>
#include <iostream>

#include "menu.h"

using namespace std;

void menu::start() {
    try {
    int choice = -1;

    unsigned int rows1 = 0, cols1 = 0;
    unsigned int rows2 = 0, cols2 = 0;    

    BaseMatrixPtr matrix1;
    BaseMatrixPtr matrix2;

    cout << "======================================================================================" << endl;

    cout << "Enter rows and columns for first matrix: " << endl;

    while(true) {
        cin >> rows1 >> cols1;
        if(cin.good()) {
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        }
        cin.clear();
        cout << "Please enter valid input!" << endl;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    cout << "Do you want to generate a first matrix?(0 - yes, 1 - no)" << endl;

    while(true) {
        cin >> choice;

        if(cin.good() && choice == 0) {
            matrix1 = BaseMatrix::Generate(rows1, cols1);
            cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
            break;
        } else if(cin.good() && choice == 1){
            cout << "======================================================================================" << endl;
            cout << "input " << rows1 * cols1 << " elements into your first matrix: \n";
            matrix1 = BaseMatrix::Input(rows1, cols1);
            cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
            break;
        }
            cin.clear();
            cout << "Please enter valid input!" << endl;
            cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
    }

    cout << "Enter rows and columns for second matrix: " << endl;
    while(true) {
        cin >> rows2 >> cols2;
        if(cin.good()) {
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        }
        cin.clear();
        cout << "Please enter valid input!" << endl;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
   
    cout << "======================================================================================" << endl;

    cout << "Do you want to generate a second matrix?(0 - yes, 1 - no)" << endl;

     while(true) {
        cin >> choice;

        if(cin.good() && choice == 0) {
            matrix2 = BaseMatrix::Generate(rows2, cols2);
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        } else if(cin.good() && choice == 1){
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "======================================================================================" << endl;
            cout << "input " << rows2 * cols2 << " elements into your second matrix: \n";
            matrix2 = BaseMatrix::Input(rows2, cols2);
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        }
            cin.clear();
            cout << "Please enter valid input!" << endl;
            cin.ignore(numeric_limits<streamsize>::max(), '\n'); 
    }

cout << "======================================================================================" << endl;

    while(choice != 13) {
        cout << "First matrix:\n";
        matrix1->Print();
        cout << "Second matrix:\n";
        matrix2->Print();

cout << "======================================================================================" << endl;

        cout << "\n** You choose from the following **\n\n"
            << "        1 -  Addition\n"
            << "        2 -  Substraction\n"
            << "        3 -  Multiplication of matrices\n"
            << "        4 -  Transpose\n"
            << "        5 -  DET\n"
            << "        6 -  Split left\n"
            << "        7 -  Inverse of a Matrix\n"
            << "        8 -  Generate matrix\n"
            << "        9 -  Split down\n"
            << "        10 - Lower triangular matrix\n"
            << "        11 - Upper triangular matrix\n"
            << "        12 - Multiplication of number and matrix\n"
            << "        13 - Quit\n\n"

            << "        enter your choice: ";

		while(!(cin >> choice)) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cerr << "\nERROR: Please enter valid input!\n" 
				 << "Enter your choice again: ";
		}             

    switch(choice) {

            case 1 : {
                if(rows1 != rows2 || cols1 != cols2) {
                    throw DimensionSumException();
                } else {
                    BaseMatrixPtr addMatrix;
                    cout << "Addition of Matrix A and Matrix B is:\n";
                    addMatrix = matrix1 + matrix2; 
                    addMatrix->Print();
                }
            }
                break;


            case 2 :    {
                if(rows1 != rows2 || cols1 != cols2) {
                    throw DimensionSumException();
                } else {
                    BaseMatrixPtr subtractMatrix;
                    cout << "Subtraction of Matrix A and Matrix B is:\n";

                    subtractMatrix = matrix1 - matrix2;  
                    subtractMatrix->Print();
                }
            }
                break;


            case 3 :    {
                if(cols1 != rows2) {
                    throw DimensionProductException();
                } else {
                    BaseMatrixPtr multMatrix;   
                    cout << "Mutiplication of Matrix A and Matrix B is:\n";

                    multMatrix = matrix1 * matrix2; 
                    multMatrix->Print();
                }
            }
                break;

            case 4: {
                BaseMatrixPtr transpose;

                cout << "After transpose of first matrix!" << endl;
                transpose = matrix1->Transpose();
                transpose->Print();

                cout << "After transpose of second matrix!" << endl;
                transpose = matrix2->Transpose();
                transpose->Print();
                break;
            }

            case 5: {
                if(cols1 != rows1) {
                    throw BadDimensionException();
                } else {
                    double det;
                    det = matrix1->Determinant();
                    cout << "Determinant of first matrix is: " << det << endl;
    
                    det = matrix2->Determinant();
                    cout << "Determinant of second matrix is: " << det << endl;
                }
                break;
            }

            case 6: {
                BaseMatrixPtr split;

                cout << "Left split of Matrix A and Matrix B is:\n";
                split = matrix1 & matrix2;  
                split->Print();
                break;
            }

            case 7: {
                if(rows1 != cols1 || rows2 != cols2) {
                   throw BadDimensionException();
                } else {
                    BaseMatrixPtr inv;
                    
                    cout << "First matrix: " << endl;
                    inv = matrix1->Inverse();
                    inv->Print();

                    cout << "Second matrix: " << endl;
                    inv = matrix2->Inverse();
                    inv->Print();
                }
                break;
            }

            case 8: {
                BaseMatrixPtr gen;

                cout << "First matrix after new regeneration: " << endl;
                gen = BaseMatrix::Generate(rows1, cols1);
                gen->Print();

                cout << "Second matrix after new regeneration: " << endl;
                gen = BaseMatrix::Generate(rows2, cols2);
                gen->Print();
                break;
            }

            case 9: {
                BaseMatrixPtr split;

                cout << "Left split of Matrix A and Matrix B is:\n";
                split = matrix1 % matrix2;  
                split->Print();
                break;
            }

            case 10: {
                BaseMatrixPtr low;

                cout << "Lower triangular matrix from first matrix" << endl;
                low = matrix1->Lower();
                low->Print();

                cout << "Lower triangular matrix from second matrix" << endl;
                low = matrix2->Lower();
                low->Print();
                break;
            }

            case 11: {
                BaseMatrixPtr up;
                cout << "Upper triangular matrix from first matrix" << endl;
                up = matrix1->Upper();
                up->Print();

                cout << "Upper triangular matrix from first matrix" << endl;
                up = matrix2->Upper();
                up->Print();
                break;
            }

            case 12: {
                double number;
                BaseMatrixPtr res1, res2;

                cout << "Enter a number on which you want multiply first matrix" << endl;
                while(true) {
                    cin >> number;
                    if(cin.good()) {
                        cout << "First matrix after multiplication: " << endl; 
                        res1 = number * matrix1; 
                        res1->Print();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        break;
                    }
                    cin.clear();
                    cout << "Please enter valid input!" << endl;
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                }

                cout << "Enter a number on which you want multiply second matrix" << endl;
                while(true) {
                    cin >> number;
                    if(cin.good()) {
                        cout << "Second matrix after multiplication: " << endl; 
                        res2 = number * matrix2; 
                        res2->Print();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        break;
                    }
                    cin.clear();
                    cout << "Please enter valid input!" << endl;
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                }
                break;
            }

            case 13: {
                cout << "Exit" << endl;
                matrix1.~shared_ptr();
                matrix2.~shared_ptr();

                break;
            }                   
                                    
            default :
                cout << "That's not a choice.\n";
        }

        cout << "\npress any key...\n";

		// clears input buffer and wait for input:
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cin.get();  
    }               
} catch(Exception & e) {
    e.message();
}
}