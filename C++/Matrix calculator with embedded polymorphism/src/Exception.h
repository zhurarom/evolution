#ifndef MATRIXCALC_EXCEPTION_H
#define MATRIXCALC_EXCEPTION_H

#include <iostream>

using namespace std;

/**
 * This class is the base class for exception handling
*/

class Exception {
    public:
        /**
         * Print a message about exception
        */
        virtual void message() = 0;
};

/**
 * Class of exception associated with incorrect
 * specifying the dimensions of the matrix 
*/
class BadDimensionException : public Exception {
    public: 
        /**
         * @inherit
        */
        void message();
};

/**
 * Class exception for invalid argument
 * 
*/
class InvalidArgument : public Exception {
    public:
        /**
         * @inherit
        */
        void message();
};

/**
 * Elimination class size mismatch
 * arising from the summation of matrices
*/
class DimensionSumException : public Exception {
    public:
        /**
         * @inherit
        */
        void message();
};

/**
 * Elimination class size mismatch
 * arising from the multiplication of matrices
*/
class DimensionProductException : public Exception {
    public:
        /**
         * @inherit
        */
        void message();
};

/**
 * Class of exception arising from the calculation of the inverse
 * matrices - division by determinant equal to 0
*/
class ZeroDivideException : public Exception {
    public:
        /**
         * @inherit
        */
        void message();
};

/**
 * Class of exception for range error
 * 
*/
class RangeError : public Exception {
    public:
        /**
         * @inherit
        */
        void message();
};

#endif  //MATRIXCALC_EXCEPTION_H