#ifndef MATRIXCALC_MATRIX_H
#define MATRIXCALC_MATRIX_H

#include <memory>
#include <vector>
#include <map>

/**
 * declaration of a basic class
*/
class BaseMatrix;

/**
 * For sparse matrix
*/
typedef std::shared_ptr<BaseMatrix> BaseMatrixPtr;

/**
 * Basic class for Sparse and Dense matrices 
*/
class BaseMatrix {
public:
    /**
     * Matrix constructor
     * @param rows
     * @param cols
     */
    BaseMatrix(unsigned int rows, unsigned int cols);

    virtual ~BaseMatrix();

    /**
     * Function for access matrix element
     * @param row
     * @param col
     * @return element of matrix
    */
    double operator()(unsigned int row, unsigned int col) const;

    /**
     * Operator which print matrices
     * @param left matrix
     * @param right
     * @return result
    */
    friend BaseMatrixPtr operator + (const BaseMatrixPtr left, const BaseMatrixPtr right);

    /**
     * Operator which split matrices using down side
     * @param left
     * @param right
     * @return result
    */
    friend BaseMatrixPtr operator % (const BaseMatrixPtr left, const BaseMatrixPtr right);    
    
    /**
     * Operator which split matrices using left side
     * @param left
     * @param right
     * @return result
    */    
    friend BaseMatrixPtr operator & (const BaseMatrixPtr left, const BaseMatrixPtr right);
  
    /**
     * Operator which substract matrices
     * @param left
     * @param right
     * @return result
    */
    friend BaseMatrixPtr operator - (const BaseMatrixPtr left, const BaseMatrixPtr right);
    
    /**
     * Operator which multiply matrix with matrix
     * @param left
     * @param right
     * @return result
    */
    friend BaseMatrixPtr operator * (const BaseMatrixPtr left, const BaseMatrixPtr right);
   
    /**
     * Operator which multiply matrix with number
     * @param number
     * @param right
     * @return result
    */    
    friend BaseMatrixPtr operator * (const double number, const BaseMatrixPtr right);
   
    /**
     * Function which print matrix
    */
    void Print();

    /**
     * Getter of rows
     * @return rows
    */
    unsigned int Rows() const;
    
    /**
     * Getter of columns
     * @return columns
    */
    unsigned int Cols() const;

    /**
     * Getter of determinant
     * @return determinant
    */
    double Determinant();

    /**
     * Calculate determinant
     * @param value
    */
    void Determinant(double);

    /**
     * Function which made low view of matrix(add zero)
     * @return result
     */
    BaseMatrixPtr Lower();

    /**
     * Function which made high view of matrix(add zero)
     * @return result
    */
    BaseMatrixPtr Upper();

    /**
     * Made transpose of matrix
     * @return result
    */
    BaseMatrixPtr Transpose();

    /**
     * Made inverse of matrix
     * @return result
    */
    BaseMatrixPtr Inverse();

    /**
     * Use for finding of inverse matrix
     * @param rows
     * @param cols
     * @return result
    */
    BaseMatrixPtr SubMatrix(unsigned int rows, unsigned int cols);

    /**
     * Static function which we use for filling up the matrix*
     * @param rows
     * @param cols
     * @return result
    */
    static BaseMatrixPtr Input(unsigned int rows, unsigned int cols);

    /**
     * Static function which we use for generation of matrix
     * @param rows
     * @param cols
     * @return result
    */
    static BaseMatrixPtr Generate(unsigned int rows, unsigned int cols);
protected:
    /**
     * Function which have access to element of matrix
     * @param row
     * @param column
     * @return element of matrix
    */
    virtual double At(unsigned int row, unsigned int column) const = 0;

    /**
     * Number of rows
    */
    unsigned int rows;

    /**
     * Number of columns
    */
    unsigned int cols;

    /**
     * Determinant
    */
    double determinant;

    /**
     * Use this one for function Determinant()
    */
    bool isCalculated = false;
};

/**
 * Use this one for sparse martix, data about our sparse matrix we contain in map
*/
typedef std::shared_ptr<BaseMatrix> BaseMatrixPtr;


/**
 * Use this one for dense martix, data about our dense matrix we contain in vector
*/
typedef std::vector<double> RawData;

#endif //MATRIXCALC_MATRIX_H
