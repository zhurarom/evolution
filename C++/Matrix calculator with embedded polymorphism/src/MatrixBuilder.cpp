#include <iostream>
#include "MatrixBuilder.h"

BaseMatrixPtr MatrixBuilder::InternalBuildMatrix(unsigned int rows, unsigned int columns, const RawData& data) {
    unsigned int nonZeroElemCount = 0, sparseMemory, denseMemory;
    double elem = 1.0;
    BaseMatrixPtr newMatrix = nullptr;
    SparseData sparseData;

    for (unsigned int row = 0; row < rows; row++) {
        for (unsigned int column = 0; column < columns; column++) {
            elem = data[row * columns + column];
            if (elem != 0) {
                sparseData[row].emplace(column, elem);
                nonZeroElemCount++;
            }
        }
    }
    sparseMemory = nonZeroElemCount * 3;
    denseMemory = rows * columns;

    if (sparseMemory >= denseMemory) { newMatrix = std::make_shared<DenseMatrix>(rows, columns, data); }
    else { newMatrix = std::make_shared<SparseMatrix>(rows, columns, sparseData); }
    return newMatrix;
}

BaseMatrixPtr MatrixBuilder::BuildMatrix(unsigned int rows, unsigned int columns, const RawData& data) {
    BaseMatrixPtr newMatrix = MatrixBuilder::InternalBuildMatrix(rows, columns, data);
  
    return newMatrix;
}
