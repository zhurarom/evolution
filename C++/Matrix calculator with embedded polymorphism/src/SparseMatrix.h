#ifndef MATRIXCALC_SPARSEMATRIX_H
#define MATRIXCALC_SPARSEMATRIX_H

#include <memory>
#include <vector>
#include <map>
#include "Matrix.h"
#include "MatrixBuilder.h"
#include "Exception.h"

/**
 *  Declaration of class BaseMatrix 
*/
class BaseMatrix;

/**
 * Data about sparse matrix
*/
typedef std::map<unsigned int, std::map<unsigned int, double>> SparseData;

/**
 * Matrix class for sparse matrices, data saved as map (rows) of maps (columns => value), memory efficient
 */

class SparseMatrix : public BaseMatrix {
public:
    /**
     * Constructor
     * @param rows
     * @param cols
     * @param data
     */
    SparseMatrix(unsigned int rows, unsigned int cols, const SparseData& data);
private:
    /**
     * @inherit
    */
    virtual double At(unsigned int row, unsigned int column) const;

    /**
     * Data about sparse matrix
    */
    SparseData data;
};

#endif  //MATRIXCALC_SPARSEMATRIX_H