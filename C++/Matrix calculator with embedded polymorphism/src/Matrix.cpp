#include <iostream>
#include <cmath>
#include <random>
#include "Matrix.h"
#include "MatrixBuilder.h"
#include "Exception.h"
#include "SparseMatrix.h"
#include "DenseMatrix.h"

using namespace std;

BaseMatrix::BaseMatrix(unsigned int rows, unsigned int cols) : rows(rows), cols(cols), determinant(0) {}

double BaseMatrix::operator()(unsigned int row, unsigned int col) const {
    if (row >= rows || col >= cols)
        throw RangeError();
    return At(row, col);
}

void BaseMatrix::Print() {
    for (unsigned int i = 0; i < rows; i++) {
        cout << "[";
        for (unsigned int j = 0; j < cols; j++) {
            cout << " " << At(i, j);
        }
        cout << "]" << endl;
    }
}

unsigned int BaseMatrix::Rows() const { return rows; }

unsigned int BaseMatrix::Cols() const { return cols; }

void BaseMatrix::Determinant(double value) {
    this->determinant = value;
}

BaseMatrixPtr operator&(const BaseMatrixPtr left, const BaseMatrixPtr right) {
    RawData rawResult;
    unsigned int rows = left->Rows(), lcols = left->Cols(), rcols = right->Cols();
    if(rows != right->Rows())
        throw RangeError();
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < lcols; j++) {
            rawResult.push_back(left->At(i, j));
        }
        for (unsigned int j = 0; j < rcols; j++) {
            rawResult.push_back(right->At(i, j));
        }
    }

    auto result = MatrixBuilder::BuildMatrix(rows, lcols+ rcols, rawResult);
    return result;
}


BaseMatrixPtr operator % (const BaseMatrixPtr left, const BaseMatrixPtr right) {
    RawData rawResult;
    unsigned int lrows = left->Rows(), cols = left->Cols(), rrows = right->Rows();
    if (cols != right->Cols())
        throw RangeError();
    for (unsigned int i = 0; i < lrows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(left->At(i, j));
        }
    }
    for (unsigned int i = 0; i < rrows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(right->At(i, j));
        }
    }

    auto result = MatrixBuilder::BuildMatrix(lrows + rrows, cols, rawResult);
    return result;
}



BaseMatrixPtr operator + (const BaseMatrixPtr left, const BaseMatrixPtr right)
{
    RawData rawResult;
    unsigned int rows = left->Rows(), cols = left->Cols();
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(left->At(i,j) + right->At(i, j));
        }
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr operator - (const BaseMatrixPtr left, const BaseMatrixPtr right)
{
    RawData rawResult;
    unsigned int rows = left->Rows(), cols = left->Cols();
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(left->At(i, j) - right->At(i, j));
        }
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr operator * (const BaseMatrixPtr left, const BaseMatrixPtr right)
{
    RawData rawResult;
    unsigned int rows = left->Rows(), cols = left->Cols();
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            double sum = 0;
            for (unsigned int k = 0; k < rows; k++) {
                sum += left->At(i, k) * right->At(k, j);
            }
            rawResult.push_back(sum);
        }
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}


BaseMatrixPtr operator * (const double number, const BaseMatrixPtr right)
{
    RawData rawResult;
    unsigned int rows = right->Rows(), cols = right->Cols();
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(right->At(i, j)* number);
        }
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr BaseMatrix::Lower() {
    RawData rawResult; 
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            if (i < j) {
                rawResult.push_back(0);
            }
            else {
                rawResult.push_back(At(i,j));
            }
        };
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr BaseMatrix::Upper() {
    RawData rawResult;
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            if (i > j) {
                rawResult.push_back(0);
            }
            else {
                rawResult.push_back(At(i, j));
            }
        };
    }

    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr BaseMatrix::Transpose() {
    RawData rawResult;
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(At(j, i));
        };
    }

    auto result = MatrixBuilder::BuildMatrix(cols, rows, rawResult);
    return result;
}


double BaseMatrix::Determinant() {
    if (rows != cols)
        throw InvalidArgument();
    if (!isCalculated) {
        unsigned int r = this->rows, c = this->cols, i, j;

        double det = 1;

        std::vector<RawData> a(rows, RawData(cols));

        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                a[i][j] = (*this)(i, j);
            }
        }

        for (unsigned int i = 0; i < rows; ++i) {
            unsigned int k = i;

            for (unsigned int j = i + 1; j < rows; ++j) {
                if (abs( a[j][i]) > abs( a[k][i]))
                    k = j;
            }

            if (abs( a[k][i]) < __DBL_EPSILON__) {    //double EPS = 1e - 9
                det = 0;
                break;
            }

            swap( a[i],  a[k]);
            if (i != k) {
                det = -det;
            }

            det *=  a[i][i];

            for (unsigned int j = i + 1; j < rows; ++j) {
                 a[i][j] /=  a[i][i];
            }

            for (unsigned int j = 0; j < rows; ++j)
                if (j != i && abs( a[j][i]) > __DBL_EPSILON__) //double EPS = 1e - 9
                    for (unsigned int k = i + 1; k < rows; ++k) {
                         a[j][k] -=  a[i][k] *  a[j][i];
                    }
        }

        determinant = det;
        isCalculated = true;
    }

    return determinant;
}


BaseMatrixPtr BaseMatrix::Generate(unsigned int rows, unsigned int cols) {
    RawData rawResult;
    srand(rand());
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            rawResult.push_back(rand() % 100);
        }
    }
    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);

    return result;
}

BaseMatrixPtr BaseMatrix::Input(unsigned int rows, unsigned int cols)
{
    RawData rawResult;
    double tmp;
    for (unsigned int i = 0; i < rows; i++) {
        for (unsigned int j = 0; j < cols; j++) {
            std::cin >> tmp;

            // terminates function on failure
            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cerr << "ERROR: Please enter valid input!\n" << std::endl;
                continue;
            }

            rawResult.push_back(tmp);
        }
    }
    // trims the excess input from input buffer:
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');


    auto result = MatrixBuilder::BuildMatrix(rows, cols, rawResult);
    return result;
}

BaseMatrixPtr BaseMatrix::Inverse() {
    RawData rawResult;
    unsigned int rows = Rows();;

    double det = Determinant();
    // if the matrix is ​​degenerate,
    // inverse matrix does not exist

    if (det == 0) {
        throw ZeroDivideException();
    }

    // calculation of the transposed matrix
    // algebraic additions

    BaseMatrixPtr tmp = nullptr;
    int z = 0;

    for (unsigned int i = 0; i < rows; i++) {
        z = i % 2 == 0 ? 1 : -1;
        for (unsigned int j = 0; j < rows; j++) {
            tmp = SubMatrix(j, i);
            rawResult.push_back(z * tmp->Determinant() / det);
            z = -z;
        }
    }

    auto result = MatrixBuilder::BuildMatrix(this->Rows(), this->Cols(), rawResult);
    return result;
}

BaseMatrixPtr BaseMatrix::SubMatrix(unsigned int srow, unsigned int scol)
{
    RawData rawResult;
    for (unsigned int i = 0; i < srow; i++) {
        for (unsigned int j = 0; j < scol; j++) {
            rawResult.push_back(At(i,j));
        }
        for (unsigned int j = scol + 1; j < cols; j++) {
            rawResult.push_back(At(i, j));
        }
    }

    for (unsigned int i = srow + 1; i < rows; i++) {
        for (unsigned int j = 0; j < scol; j++) {
            rawResult.push_back(At(i, j));
        }
        for (unsigned int j = scol + 1; j < cols; j++) {
            rawResult.push_back(At(i, j));
        }
    }


    auto result = MatrixBuilder::BuildMatrix(this->Rows()-1, this->Cols() - 1, rawResult);
    return result;
}

BaseMatrix::~BaseMatrix() {}
