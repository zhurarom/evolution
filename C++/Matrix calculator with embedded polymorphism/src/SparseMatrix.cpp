#include <iostream>
#include <cmath>
#include <random>
#include "SparseMatrix.h"

SparseMatrix::SparseMatrix(unsigned int rows, unsigned int cols, const SparseData& dataIn): BaseMatrix(rows, cols), data(dataIn) {}

double SparseMatrix::At(unsigned int row, unsigned int column) const {
    double element;
    try { element = data.at(row).at(column); } catch (std::exception & e) { return 0; }
    return element;
}