#include <cassert>

class BaseAllocator {
public:

    BaseAllocator(int size, void* start)
    {
        _start = start;
        _size = size;
        _used_memory = 0;
        _num_allocations = 0;
    }

    virtual ~BaseAllocator()
    {
        //assert(_num_allocations == 0 && _used_memory == 0);
        _start = nullptr; _size = 0;
    }

    virtual void* allocate(int size) = 0;
    virtual bool deallocate(void* p) = 0;
    void* getStart() const { return _start; }
    int getSize() const { return _size; }
    int getUsedMemory() const { return _used_memory; }
    int getNumAllocations() const { return _num_allocations; }

protected:
    void* _start;
    long unsigned int _size;
    long unsigned int _used_memory;
    long unsigned int _num_allocations;
};

class Allocator : public BaseAllocator
{
public:

    Allocator(int size, void* start);
    ~Allocator();

    void* allocate(int size) override;
    bool deallocate(void* p) override;

private:

    struct AllocationHeader { int size; };
    struct FreeBlock { int size; FreeBlock* next; };
    Allocator(const Allocator&);

    //Prevent copies because it might cause errors 
    Allocator& operator=(const Allocator&);
    FreeBlock* _free_blocks;
};
Allocator* allocator;