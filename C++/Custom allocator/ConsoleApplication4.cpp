#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <cmath>
using namespace std;
#endif /* __PROGTEST__ */
#include "Allocator.h"

Allocator::Allocator(int size, void* start) : BaseAllocator(size, start), _free_blocks((FreeBlock*)start)
{
    assert(size > sizeof(FreeBlock));
    _free_blocks->size = size;
    _free_blocks->next = nullptr;
}

Allocator::~Allocator()
{
    _free_blocks = nullptr;
}

void* Allocator::allocate(int size)
{
    assert(size != 0);
    FreeBlock* prev_free_block = nullptr;
    FreeBlock* free_block = _free_blocks;

    while (free_block != nullptr)
    {
        int total_size = size;

        if (free_block->size < total_size)
        {
            prev_free_block = free_block;
            free_block = free_block->next;
            continue;
        }

        //static_assert(sizeof(AllocationHeader) >= sizeof(FreeBlock), "sizeof(AllocationHeader) < sizeof(FreeBlock)");

        //If allocations in the remaining memory will be impossible 
        if (free_block->size - total_size <= sizeof(AllocationHeader))
        {
            //Increase allocation size instead of creating a new FreeBlock 
            total_size = free_block->size;

            if (prev_free_block != nullptr)
                prev_free_block->next = free_block->next;
            else
                _free_blocks = free_block->next;
        }
        else
        {
            //Else create a new FreeBlock containing remaining memory 
            FreeBlock* next_block = (FreeBlock*)(reinterpret_cast<uintptr_t>(free_block) + total_size);

            next_block->size = free_block->size - total_size;
            next_block->next = free_block->next;

            if (prev_free_block != nullptr)
                prev_free_block->next = next_block;
            else
                _free_blocks = next_block;
        }

        uintptr_t address = (uintptr_t)free_block;
        AllocationHeader* header = (AllocationHeader*)(address - sizeof(AllocationHeader));
        header->size = total_size;
        _used_memory += total_size;
        _num_allocations++;

        //assert(pointer_math::alignForwardAdjustment((void*)aligned_address, alignment) == 0);

        return (void*)address;
    }

    //ASSERT(false && "Couldn't find free block large enough!"); 
    return nullptr;
}

bool Allocator::deallocate(void* p)
{
    if (p == nullptr)
        return false;
    AllocationHeader* header = (AllocationHeader*)(reinterpret_cast<uintptr_t>(p) - sizeof(AllocationHeader));
    uintptr_t block_start = reinterpret_cast<uintptr_t>(p);
    int block_size = header->size;
    uintptr_t block_end = block_start + block_size;
    FreeBlock* prev_free_block = nullptr;
    FreeBlock* free_block = _free_blocks;

    while (free_block != nullptr)
    {
        if ((uintptr_t)free_block >= block_end) break;
        prev_free_block = free_block;
        free_block = free_block->next;
    }

    if (prev_free_block == nullptr)
    {
        prev_free_block = (FreeBlock*)block_start;
        prev_free_block->size = block_size;
        prev_free_block->next = _free_blocks;
        _free_blocks = (FreeBlock *)prev_free_block;
    }
    else if ((uintptr_t)prev_free_block + prev_free_block->size == block_start)
    {
        prev_free_block->size += block_size;
    }
    else
    {
        FreeBlock* temp = (FreeBlock*)block_start;
        temp->size = block_size;
        temp->next = prev_free_block->next;
        prev_free_block->next = temp;
        prev_free_block = temp;
    }

    if (free_block != nullptr && (uintptr_t)free_block == block_end)
    {
        prev_free_block->size += free_block->size;
        prev_free_block->next = free_block->next;
    }

    _num_allocations--;
    _used_memory -= block_size;
    return true;
}

void  HeapInit(void* memPool, int memSize)
{
    allocator = new Allocator(memSize, memPool);
}
void* HeapAlloc(int    size)
{
   return allocator->allocate(size);
}
bool   HeapFree(void* blk)
{    return allocator->deallocate(blk);
}
void HeapDone(int* pendingBlk)
{
    (*pendingBlk) = allocator->getNumAllocations();
    delete allocator;
}

#ifndef __PROGTEST__
int main(void)
{
    uint8_t* p0, * p1, * p2, * p3, * p4;
    int             pendingBlk;
    static uint8_t  memPool[3 * 1048576];

    HeapInit(memPool, 2097152);
    assert((p0 = (uint8_t*)HeapAlloc(512000)) != NULL);
    memset(p0, 0, 512000);
    assert((p1 = (uint8_t*)HeapAlloc(511000)) != NULL);
    memset(p1, 0, 511000);
    assert((p2 = (uint8_t*)HeapAlloc(26000)) != NULL);
    memset(p2, 0, 26000);
    HeapDone(&pendingBlk);
    assert(pendingBlk == 3);


    HeapInit(memPool, 2097152);
    assert((p0 = (uint8_t*)HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert((p1 = (uint8_t*)HeapAlloc(250000)) != NULL);
    memset(p1, 0, 250000);
    assert((p2 = (uint8_t*)HeapAlloc(250000)) != NULL);
    memset(p2, 0, 250000);
    assert((p3 = (uint8_t*)HeapAlloc(250000)) != NULL);
    memset(p3, 0, 250000);
    assert((p4 = (uint8_t*)HeapAlloc(50000)) != NULL);
    memset(p4, 0, 50000);
    assert(HeapFree(p2));
    assert(HeapFree(p4));
    assert(HeapFree(p3));
    assert(HeapFree(p1));
    assert((p1 = (uint8_t*)HeapAlloc(500000)) != NULL);
    memset(p1, 0, 500000);
    assert(HeapFree(p0));
    assert(HeapFree(p1));
    HeapDone(&pendingBlk);
    assert(pendingBlk == 0);


    HeapInit(memPool, 2359296);
    assert((p0 = (uint8_t*)HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert((p1 = (uint8_t*)HeapAlloc(500000)) != NULL);
    memset(p1, 0, 500000);
    assert((p2 = (uint8_t*)HeapAlloc(500000)) != NULL);
    memset(p2, 0, 500000);
    assert((p3 = (uint8_t*)HeapAlloc(500000)) == NULL);
    assert(HeapFree(p2));
    assert((p2 = (uint8_t*)HeapAlloc(300000)) != NULL);
    memset(p2, 0, 300000);
    assert(HeapFree(p0));
    assert(HeapFree(p1));
    HeapDone(&pendingBlk);
    assert(pendingBlk == 1);


    HeapInit(memPool, 2359296);
    assert((p0 = (uint8_t*)HeapAlloc(1000000)) != NULL);
    memset(p0, 0, 1000000);
    assert(!HeapFree(p0 + 1000));
    HeapDone(&pendingBlk);
    assert(pendingBlk == 1);


    return 0;
}
#endif /* __PROGTEST__ */
