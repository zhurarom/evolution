#include <iostream>
#include "Game.h"

int main() {
    shared_ptr<Game> game = make_shared<Game>(Game());
    game->Init();
    game->LoadLevel();
    game->Run();
    return 0;
}