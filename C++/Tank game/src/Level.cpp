
#include "Level.h"
#include <fstream>
#include <iostream>

using namespace std;

Level::Level(const string &name, const string &path) :
        levelName(name), levelPath(path) {
    LevelData.resize(rowSize);
    for (int i = 0; i < rowSize; ++i) {
        LevelData[i].resize(columnSize + 1);
    }
}

Level::~Level() {
    LevelData.clear();
}

void Level::setLevelData() {
    ifstream myfile(levelPath);
    if (myfile.is_open()) {
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j <= columnSize; j++) {
                char sym = myfile.get();
                LevelData[i][j] = sym;
            }
        }
        myfile.close();
    } else cout << "Unable to open file";
}

const vector<vector<char>> &Level::getLevelData() const {
    return LevelData;
}

