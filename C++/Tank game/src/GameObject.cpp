// Include
#include "GameObject.h"
#include "Level.h"

// Class GameObject

GameObject::GameObject(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
                       GameObjectType type) :
        x(x), y(y), width(width), height(height), health(health), invulnerable(invulnerable), physical(physical),
        sym(sym), type(type) {}

GameObject::~GameObject() {

}

void GameObject::Draw() {
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            attron(COLOR_PAIR(GetColor()));
            mvaddch(getY() + j, getX() + i, GetSym());
        }
    }
}

void GameObject::DecHealth(int delta) {
    if (health == 1) {
        health--;
    } else {
        health -= delta;
    }
}