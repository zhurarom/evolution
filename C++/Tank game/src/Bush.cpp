
#include "Bush.h"

Bush::Bush(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
           GameObjectType type) :
        GameObject(x, y, width, height, health, invulnerable, physical, sym, type) {}


char Bush::GetSym() {
    return sym;
}

Colors Bush::GetColor() {
    return COLOR_BUSH;
}
