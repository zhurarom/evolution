
#ifndef TANKS_GAME_H
#define TANKS_GAME_H

// Include
#include <fstream>
#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <ncurses.h>
#include <algorithm>
#include <vector>
#include <map>
#include "stdlib.h"
#include "Constants.h"
#include "GameObject.h"
#include "Level.h"
#include "Wall.h"
#include "Tank.h"
#include "TankPlayer.h"
#include "TankEnemy.h"
#include "Bullet.h"
#include "Base.h"
#include "EnemySpawner.h"
#include "Bush.h"
#include "BonusAtk.h"
#include "Bonus.h"
#include "BonusInv.h"

//==================================================Game================================================================
/**
 * @brief Main class that represents a game itself.
 */
class Game {
public:
    Game();

    /**
     * @brief Initialization of the game.
     */
    void Init();

    /**
     * @brief Load map to vector of objects.
    */
    void LoadLevel();

    /**
     * @brief Infinite loop, all processes run here.
     */
    void Run();

    /**
     * @brief Control of bullets
     */
    void BulletsMovement();

    /**
     * @brief Close window, called at the end of the game.
     */
    void Close();

    /**
     * @brief Refresh all objects on the map except bullets
     */
    void RefreshLevel();

    /**
     * @brief Refresh all bullets on the map.
     */
    void RefreshBullet();

    /**
     * @brief Handle pressed key and do corresponding action.
     * @param[in] keyCode Pressed key
     * @return true if an action was done and false if no action was done (because of collision).
     */
    bool HandlePressedKey(int keyCode);

    /**
     * @brief Ask the name of player
     */
    void NameRead();

    /**
     * @brief Shows the game over screen
     * @param[in] situation (may be lose or win)
     */
    void GameOver(int situation);

    /**
     * @brief Resets player if he die
     */
    void ResetPlayer();

    /**
     * @brief Increase players's score
     */
    void IncreaseScore();

    /**
     * @brief Controls player's movement
     * @param[in] direction Direction where to go
     * @param[in] xDelta Next x position
     * @param[in] yDelta Next y position
     * @return true if moving is possible, false if not
     */
    bool PlayerMovement(Direction direction, int xDelta, int yDelta);

    /**
     * @brief Add an enemy on random spawner
     */
    void AddEnemy();

    /**
     * @brief Create certain type of bonus
     * @param[in] x
     * @param[in] y
     * @param[in] type
     */
    void CreateBonus(int x, int y, GameObjectType type);

    /**
     * @brief Prints player's score
     */
    void PrintScore();

    /**
     * @brief Print title
     * @param[in] title Certain title
     */
    void PrintTitle(string &title);

    /**
     * @brief Process player's score
     */
    void ProcessScore();

    /**
     * @brief Increment amount of killed enemies
     */
    void IncKilledEnemies();

    /**
     * @brief Increment amount of alive enemies
     */
    void IncAliveEnemies();

    /**
     * @brief Decrement amount of live enemies
     */
    void DecAliveEnemies();

    /**
     * @brief Generate a chance to drop some kind of bonus
     * @param game Instance of the game
     * @param objEnemy After which enemy death spawn the bonus
     */
    void BonusChance(Game &game, shared_ptr<TankEnemy> &objEnemy);

    /**
     * @brief Gets random direction to spawn new enemies
     * @return Some random direction
     */
    Direction GetRandomDirection();

    /**
     * @brief Vector with all objects
     */
    vector<shared_ptr<GameObject>> vecObjects;

    /**
     * @brief Vector with all bullets
     */
    vector<shared_ptr<Bullet>> vecBullets;

    /**
     * @brief Vector with all enemy spawners
     */
    vector<shared_ptr<EnemySpawner>> vecSpawners;

    /**
     * @brief Vector with all enemies
     */
    vector<shared_ptr<TankEnemy>> vecEnemies;

private:
    /**
     * @brief Is game still going
     */
    bool gameRun;
    /**
     * @brief Shared pointer to the player
     */
    shared_ptr<TankPlayer> player;

    /**
     * @brief Shared pointer to the current level
     */
    shared_ptr<Level> curLevel;

    /**
     * @brief Player's score
     */
    int score;

    /**
     * @brief Amount of killed enemies
     */
    int killedEnemies;

    /**
     * @brief Amount of alive enemies
     */
    int aliveEnemies;

    /**
     * @brief Player name
     */
    string playerName;
};


#endif //TANKS_GAME_H
