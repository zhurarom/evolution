
#include "BonusInv.h"

BonusInv::BonusInv(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
                   GameObjectType type) : Bonus(x, y, width, height, health, invulnerable, physical, sym, type) {}

char BonusInv::GetSym() {
    return sym;
}

Colors BonusInv::GetColor() {
    return COLOR_BONUS_INV;
}

void BonusInv::GiveBonus(shared_ptr<TankPlayer> &player) {
    player->IncHealth();
}

