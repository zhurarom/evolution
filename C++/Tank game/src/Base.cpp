
#include "Base.h"

Base::Base(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym, GameObjectType type) :
        GameObject(x, y, width, height, health, invulnerable, physical, sym, type){}


char Base::GetSym() {
    return sym;
}

Colors Base::GetColor() {
    return COLOR_BASE;
}

