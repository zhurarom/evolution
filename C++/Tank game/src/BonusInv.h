
#ifndef TANKS_BONUSINV_H
#define TANKS_BONUSINV_H

#include "Bonus.h"

//==================================================Bonus health==============================================================
/**
 * @brief A class to represent an attack bonus.
 */
class BonusInv : public Bonus {
public:
    /**
     * @brief Constructor of health bonus
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     */
    BonusInv(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
             GameObjectType type);

    Colors GetColor() override;

    char GetSym() override;

    /**
     * @brief Give bonus to player
     * @param player Who give a bonus
     */
    void GiveBonus(shared_ptr<TankPlayer> &player) override;

};


#endif //TANKS_BONUSINV_H
