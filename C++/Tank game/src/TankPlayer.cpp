
#include "TankPlayer.h"
#include "Game.h"

TankPlayer::TankPlayer(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
                       GameObjectType type, Direction direction, int lives1) :
        Tank(x, y, width, height, health, invulnerable, physical, sym, type, direction), lives(lives1) {}

char TankPlayer::GetSym() {
    return sym;
}

Colors TankPlayer::GetColor() {
    return COLOR_TANK_PLAYER_SYM;
}

Colors TankPlayer::GetBodyColor() {
    return COLOR_TANK_PLAYER_BODY;
}

void TankPlayer::IncHealth() {
    health++;
}

void TankPlayer::PrintStats() {
    attron(A_BOLD);
    attron(COLOR_PAIR(COLOR_TEXT));
    mvprintw(23, 47, "Health: %d", getHealth());
    attroff(A_BOLD);

    attron(A_BOLD);
    attron(COLOR_PAIR(COLOR_TEXT));
    mvprintw(23, 57, "| Lives: %d", getLives());
    attroff(A_BOLD);
    refresh();

    attron(A_BOLD);
    attron(COLOR_PAIR(COLOR_TEXT));
    mvprintw(23, 67, " | Attack: %d", getTankDamage());
    attroff(A_BOLD);
}
