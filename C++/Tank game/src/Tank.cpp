
#include "Tank.h"
#include "Game.h"

Tank::Tank(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
           GameObjectType type, Direction direction) :
        Moving(x, y, width, height, health, invulnerable, physical, sym, type, direction
        ) {
    cooldown = tankCooldown;
    tankDamage = 1;
}

void Tank::decCooldown() {
    if (cooldown != 0) {
        cooldown--;
    }
}

void Tank::Draw() {
    switch (direction) {
        case Direction_Up: {
            attron(COLOR_PAIR(GetColor())); //Draws a tank in certain position
            mvaddch(getY() - 1, getX(), GetSym());
            attroff(COLOR_PAIR(GetColor()));

            attron(COLOR_PAIR(GetBodyColor()));
            mvaddch(getY(), getX(), GetSym());
            mvaddch(getY(), getX() - 1, GetSym());
            mvaddch(getY(), getX() + 1, GetSym());
            mvaddch(getY() - 1, getX() - 1, GetSym());
            mvaddch(getY() - 1, getX() + 1, GetSym());
            mvaddch(getY() + 1, getX() - 1, GetSym());
            mvaddch(getY() + 1, getX() + 1, GetSym());
            mvaddch(getY() + 1, getX(), GetSym());
            attroff(COLOR_PAIR(GetBodyColor()));


            break;
        }
        case Direction_Down: {
            attron(COLOR_PAIR(GetColor()));
            mvaddch(getY() + 1, getX(), GetSym());
            attroff(COLOR_PAIR(GetColor()));

            attron(COLOR_PAIR(GetBodyColor()));
            mvaddch(getY(), getX(), GetSym());
            mvaddch(getY(), getX() - 1, GetSym());
            mvaddch(getY(), getX() + 1, GetSym());
            mvaddch(getY() + 1, getX() - 1, GetSym());
            mvaddch(getY() + 1, getX() + 1, GetSym());
            mvaddch(getY() - 1, getX() - 1, GetSym());
            mvaddch(getY() - 1, getX() + 1, GetSym());
            mvaddch(getY() - 1, getX(), GetSym());
            attroff(COLOR_PAIR(GetBodyColor()));

            break;
        }
        case Direction_Left: {
            attron(COLOR_PAIR(GetColor()));
            mvaddch(getY(), getX() - 1, GetSym());
            attroff(COLOR_PAIR(GetColor()));

            attron(COLOR_PAIR(GetBodyColor()));
            mvaddch(getY() - 1, getX() - 1, GetSym());
            mvaddch(getY() + 1, getX() - 1, GetSym());
            mvaddch(getY() - 1, getX(), GetSym());
            mvaddch(getY() + 1, getX(), GetSym());
            mvaddch(getY(), getX(), GetSym());
            mvaddch(getY() - 1, getX() + 1, GetSym());
            mvaddch(getY(), getX() + 1, GetSym());
            mvaddch(getY() + 1, getX() + 1, GetSym());
            attroff(COLOR_PAIR(GetBodyColor()));

            break;
        }
        case Direction_Right: {
            attron(COLOR_PAIR(GetColor()));
            mvaddch(getY(), getX() + 1, GetSym());
            attroff(COLOR_PAIR(GetColor()));

            attron(COLOR_PAIR(GetBodyColor()));
            mvaddch(getY() - 1, getX() - 1, GetSym());
            mvaddch(getY() + 1, getX() - 1, GetSym());
            mvaddch(getY() - 1, getX(), GetSym());
            mvaddch(getY() + 1, getX(), GetSym());
            mvaddch(getY(), getX(), GetSym());
            mvaddch(getY() - 1, getX() + 1, GetSym());
            mvaddch(getY(), getX() - 1, GetSym());
            mvaddch(getY() + 1, getX() + 1, GetSym());
            attroff(COLOR_PAIR(GetBodyColor()));
            break;
        }

    }
}

void Tank::Move(Direction mvDirection) {

    if (mvDirection == direction) {
        switch (mvDirection) {
            case Direction_Up: {
                setY(y - 1);
                break;
            }

            case Direction_Down: {
                setY(y + 1);
                break;
            }

            case Direction_Left: {
                setX(x - 1);
                break;
            }

            case Direction_Right: {
                setX(x + 1);
                break;
            }
        }
    } else {
        Turn(mvDirection);
    }

}

void Tank::Turn(Direction trDirection) {
    switch (trDirection) {
        case Direction_Up: {
            setDirection(Direction_Up);
            break;
        }
        case Direction_Down: {
            setDirection(Direction_Down);
            break;
        }

        case Direction_Left: {
            setDirection(Direction_Left);
            break;
        }

        case Direction_Right: {
            setDirection(Direction_Right);
            break;
        }
    }
}

bool Tank::VerticalCollision(shared_ptr<GameObject> &i, int &w, int &h, int &x, int &y) {
    for (int ver = -1; ver <= 1; ver++) {
        //for tanks
        if (i->getType() == GameObjectType_TankPlayer || i->getType() == GameObjectType_TankEnemy) {
            if (i->getX() + w - 1 == x + ver && i->getY() + h - 1 == y) {
                return true;
            }
        }
            //for others
        else {
            if (i->getX() + w == x + ver && i->getY() + h == y) {
                return true;
            }
        }
    }
    return false;
}

bool Tank::HorizontalCollision(shared_ptr<GameObject> &i, int &w, int &h, int &x, int &y) {
    for (int hor = -1; hor <= 1; hor++) {
        //for tanks
        if (i->getType() == GameObjectType_TankPlayer || i->getType() == GameObjectType_TankEnemy) {
            if (i->getX() + w - 1 == x && i->getY() + h - 1 == y + hor) {
                return true;
            }
        }
            //for others
        else {
            if (i->getX() + w == x && i->getY() + h == y + hor) {
                return true;
            }
        }
    }
    return false;
}


shared_ptr<GameObject> Tank::CheckCollision(Game &game, int x, int y) {
    for (auto &i: game.vecObjects) {
        if (i->getPhysical() || i->getType() == GameObjectType_BonusAtk || i->getType() == GameObjectType_BonusInv ||
            i->getType() == GameObjectType_EnemySpawner) {
            for (int w = 0; w < i->getWidth(); w++)
                for (int h = 0; h < i->getHeight(); h++) {
                    //check collision for vertical and horizontal positions
                    if (direction == Direction_Up || direction == Direction_Down) {
                        if (VerticalCollision(i, w, h, x, y)) {
                            return i;
                        } else continue;
                    } else if (direction == Direction_Right || direction == Direction_Left) {
                        if (HorizontalCollision(i, w, h, x, y)) {
                            return i;
                        } else continue;
                    }
                }
        }
    }
    return nullptr;
}

int Tank::getCooldown() const {
    return cooldown;
}

void Tank::Fire(Game &game) {
    shared_ptr<Bullet> bullet = make_shared<Bullet>(
            Bullet(x, y, 1, 1, 1, false, true, symbol_Bullet, GameObjectType_Bullet, direction, tankDamage));
    switch (direction) {
        case Direction_Up: {
            bullet->setDirection(Direction_Up);
            bullet->setY(y - 2);
            break;
        }
        case Direction_Down: {
            bullet->setDirection(Direction_Down);
            bullet->setY(y + 2);
            break;
        }
        case Direction_Right: {
            bullet->setDirection(Direction_Right);
            bullet->setX(x + 2);
            break;
        }
        case Direction_Left: {
            bullet->setDirection(Direction_Left);
            bullet->setX(x - 2);
            break;
        }
    }

    //Check collision on spawn
    shared_ptr<GameObject> bulletCollisionObject = bullet->CheckCollision(game, bullet->getX(), bullet->getY());
    if (bulletCollisionObject) {
        bullet->Interact(game, bulletCollisionObject, bullet);
        bullet->EraseBullet(game, bullet);
    } else {
        game.vecObjects.push_back(bullet);
        game.vecBullets.push_back(bullet);
    }
}
