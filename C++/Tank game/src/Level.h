
#ifndef TANKS_LEVEL_H
#define TANKS_LEVEL_H


// Include
#include "Constants.h"
#include <string>
#include <vector>

using namespace std;

// Level data

//==================================================Level===============================================================
/**
 *  @brief A class to represent a level
 */
class Level {
public:
    /**
     * @brief Constructor of level
     * @param name Level name
     * @param path Level path on the computer
     */
    Level(const string &name, const string &path);

    /**
     * @brief Distructor of level
     */
    ~Level();

    /**
     * @brief Reads data from file
     */
    void setLevelData();

    /**
     * @brief Gets level data
     * @return Vector of chars
     */
    const vector<vector<char>> &getLevelData() const;

private:
    /**
     * @brief Name of the level
     */
    string levelName;
    /**
     * @brief Path of the level
     */
    string levelPath;
    /**
     * @brief Vector of chars
     */
    vector<vector<char>> LevelData;
};

#endif //TANKS_LEVEL_H
