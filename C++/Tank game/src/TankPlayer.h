
#ifndef TANKS_PLAYER_H
#define TANKS_PLAYER_H

#include "Tank.h"

//==================================================Tank player=========================================================
/**
 *  @brief A class to represent player's tank
 */
class TankPlayer : public Tank {
public:
    /**
     * @brief Constructor of moving object
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact with the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     * @param[in] direction Sets direction where to move
     * @param[in] lives Player's lives
     */
    TankPlayer(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
               GameObjectType type, Direction direction, int lives);

    Colors GetColor() override;

    Colors GetBodyColor() override;

    char GetSym() override;

    /**
     * @brief Decrement player's lives
     */
    void DecLives() { lives--; }

    /**
     * @brief Increments player's health
     */
    void IncHealth();

    /**
     * @brief Print player's statistics
     */
    void PrintStats();

    int getLives() { return lives; }

private:
    /**
     * @brief Player's lives
     */
    int lives;
};

#endif //TANKS_PLAYER_H
