
#ifndef TANKS_GAMEOBJECT_H
#define TANKS_GAMEOBJECT_H

#include "Constants.h"
#include <memory>

//forward declaration
class Game;

//==================================================Game object=========================================================
/**
 *  @brief Basic abstract class which contains basic information about object, inherited by other subclasses of objects
 */
class GameObject {
public:
    /**
     * @brief Constructor of object
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     */
    GameObject(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
               GameObjectType type);

//----------Virtual methods---------------------------------------------------------------------------------------------
    /**
     * @brief Virtual destructor
     */
    virtual ~GameObject();

    /**
     * @brief Virtual method GetColor. Gets the color of object
     */
    virtual Colors GetColor() = 0;

    /**
     * @brief Virtual method GetSym. Gets the sym of object
     */
    virtual char GetSym() = 0;

    /**
     * @brief Virtual method draw. Draws the object
     */
    virtual void Draw();

    /**
       * @brief Decrease HP of object
       * @param[in] dmg Received damage
       */
    void DecHealth(int dmg);

//----------Setters/Getters---------------------------------------------------------------------------------------------

    /**
     * @brief Get object's X coordinate
     * @return Return X coordinate
     */
    int getX() const { return x; }

    /**
     * @brief Set object's X coordinate
     * @param[in] new X coordinate
     */
    void setX(int setX) { x = setX; }

    /**
     * @brief Get object's Y coordinate
     * @return Return Y coordinate
     */
    int getY() const { return y; }

    /**
     * @brief Set object's Y coordinate
     * @param[in] new Y coordinate
     */
    void setY(int setY) { y = setY; }

    /**
     * @brief Set object's current hp
     * @param[in] new hp
     */
    void setHealth(int setHealth) { health = setHealth; }

    /**
     * @brief Get object's current hp
     * @return Return health points
     */
    int getHealth() const { return health; }

    /**
     * @brief Get invulnerability of object
     * @return Return object's invulnerability
     */
    bool getInvulnerable() const { return invulnerable; }

    /**
     * @brief Get physicality of object
     * @return Return object's physicality
     */
    bool getPhysical() const { return physical; }

    /**
     * @brief Get width of object
     * @return Return object's width
     */
    int getWidth() const { return width; }

    /**
     * @brief Get height of object
     * @return Return object's height
     */
    int getHeight() const { return height; }

    GameObjectType getType() const { return type; }

protected:
    /** @brief Coordinate X */
    int x;
    /** @brief Coordinate Y */
    int y;
    /** @brief Object width */
    int width;
    /** @brief Object height */
    int height;
    /** @brief Health */
    int health;
    /** @brief Object's invulnerability */
    bool invulnerable;
    /** @brief Object's physicality */
    bool physical;
    /** @brief Object's symbol */
    char sym;
    /** @brief Object's type */
    GameObjectType type;
};

#endif //TANKS_GAMEOBJECT_H
