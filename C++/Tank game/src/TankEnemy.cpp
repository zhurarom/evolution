
#include <iostream>
#include "TankEnemy.h"
#include "Game.h"

TankEnemy::TankEnemy(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
                     GameObjectType type, Direction direction) :
        Tank(x, y, width, height, health, invulnerable, physical, sym, type, direction) {}

char TankEnemy::GetSym() {
    return sym;
}

Colors TankEnemy::GetColor() {
    return COLOR_TANK_ENEMY_SYM;
}

Colors TankEnemy::GetBodyColor() {
    return COLOR_TANK_ENEMY_BODY;
}

void TankEnemy::Analize(Game &game) {
    int xColl = 0, yColl = 0;
    int xDelta = 0, yDelta = 0;
    switch (direction) {
        case Direction_Left:
            xDelta = -1;
            xColl = -1;
            break;
        case Direction_Right:
            xDelta = 1;
            xColl = 1;
            break;
        case Direction_Up:
            yDelta = -1;
            yColl = -1;
            break;
        case Direction_Down:
            yDelta = 1;
            yColl = 1;
            break;
    }

    // Find closest object and distance
    shared_ptr<GameObject> object = nullptr;

    int distance = 0;

    while (object == nullptr) {
        xColl += xDelta;
        yColl += yDelta;
        object = Watch(game, x + xColl, y + yColl);
        distance++;
    }

    //do action
    switch (object->getType()) {
        case GameObjectType_TankPlayer:
        case GameObjectType_Base: {
            Fire(game);
            return;
        }
        case GameObjectType_BrickWall: {
            Fire(game);
            return;
        }
        case GameObjectType_TankEnemy: {
            MoveRandomDirection(game);
            return;
        }
        case GameObjectType_SteelWall:
        case GameObjectType_Bush: {
            MoveRandomDirection(game);
            if (distance == 1) {
            }
            Direction newDir = game.GetRandomDirection();
            CheckMovement(game, newDir);
            return;
        }
        case GameObjectType_BonusInv:
        case GameObjectType_BonusAtk: {
            Fire(game);
            return;
        }
        case GameObjectType_EnemySpawner:
        case GameObjectType_Bullet:
        {
            break;
        }
    }
}

void TankEnemy::MoveRandomDirection(Game &game) {
    Direction randDirection = game.GetRandomDirection();
    CheckMovement(game, randDirection);
}

void TankEnemy::CheckMovement(Game &game, Direction &direction) {
    switch (direction) {
        case Direction_Left: {
            if (!CheckCollision(game, getX() - 2, getY())) {
                Move(Direction_Left);
                break;
            } else if (getDirection() != Direction_Left) {
                Turn(Direction_Left);
                break;
            }
            break;
        }
        case Direction_Right: {
            if (!CheckCollision(game, getX() + 2, getY())) {
                Move(Direction_Right);
                break;
            } else if (getDirection() != Direction_Right) {
                Turn(Direction_Right);
                break;
            }
            break;
        }
        case Direction_Down: {
            if (!CheckCollision(game, getX(), getY() + 2)) {
                Move(Direction_Down);
                break;
            } else if (getDirection() != Direction_Down) {
                Turn(Direction_Down);
                break;
            }
            break;
        }
        case Direction_Up: {
            if (!CheckCollision(game, getX(), getY() - 2)) {
                Move(Direction_Up);
                break;
            } else if (getDirection() != Direction_Up) {
                Turn(Direction_Up);
                break;
            }
        }
    }
}

shared_ptr<GameObject> TankEnemy::Watch(Game &game, int x, int y) {
    for (const auto &i: game.vecObjects) {
        if (i->getPhysical() || i->getType() == GameObjectType_BonusAtk || i->getType() == GameObjectType_BonusInv) {
            for (int w = 0; w < i->getWidth(); w++)
                for (int h = 0; h < i->getHeight(); h++) {
                    //for tanks
                    switch (i->getType()) {
                        case GameObjectType_TankPlayer:
                        case GameObjectType_TankEnemy: {
                            if (i->getX() + w - 1 == x && i->getY() + h - 1 == y) {
                                return i;
                            }
                            break;
                        }
                            //for others
                        default: {
                            if (i->getX() + w == x && i->getY() + h == y) {
                                return i;
                            }
                            break;
                        }
                    }
                }
        }
    }
    return nullptr;
}

