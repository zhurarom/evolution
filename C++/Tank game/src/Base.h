

#ifndef TANKS_BASE_H
#define TANKS_BASE_H

#include "GameObject.h"
#include "Constants.h"
#include <vector>


//==================================================Base==============================================================
/**
 *  @brief A class to represent a base
 */
class Base : public GameObject {
public:
    /**
     * @brief Constructor of base
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     */
    Base(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
         GameObjectType type);

    Colors GetColor() override;

    char GetSym() override;


};

#endif //TANKS_BASE_H
