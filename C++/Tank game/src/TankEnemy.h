
#ifndef TANKS_TANKENEMY_H
#define TANKS_TANKENEMY_H

#include "Tank.h"
#include <random>

//==================================================Tank enemy==============================================================
/**
 *  @brief A class to represent an enemy tank
 */
class TankEnemy : public Tank {
public:
    /**
    * @brief Constructor of moving object
    * @param[in] x Spawn coordinate X
    * @param[in] y Spawn coordinate Y
    * @param[in] width Width of object
    * @param[in] height Height of object
    * @param[in] health Health points on start
    * @param[in] invulnerable Flag of possibility to destroy the object
    * @param[in] physical Flag of possibility to interact with the object
    * @param[in] sym Object's char
    * @param[in] type Object's type
    * @param[in] direction Sets direction where to move
   */
    TankEnemy(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
              GameObjectType type, Direction direction);

    /**
     * @brief Enemy tank's destructor
     */
    ~TankEnemy() override = default;

    Colors GetColor() override;

    Colors GetBodyColor() override;

    char GetSym() override;

    /**
     * @brief Decides what to do
     * @param[in] game Instance of the game
     */
    void Analize(Game &game);

    /**
     * @brief Look straight ahead (viewing angle 1 cell)
     * @param[in] game Instance of the game
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return The first object to hit the road
     */
    shared_ptr <GameObject> Watch(Game &game, int x, int y);

    /**
     * @brief move in a random direction
     * @param[in] game Instance of the game
     */
    void MoveRandomDirection(Game &game);

    /**
     * @brief Check if tank can make move
     * @param game Instance of the game
     * @param direction Where to move
     */
    void CheckMovement(Game &game, Direction &direction);
};


#endif //TANKS_TANKENEMY_H
