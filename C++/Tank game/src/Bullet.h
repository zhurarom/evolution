
#ifndef TANKS_BULLET_H
#define TANKS_BULLET_H

class TankEnemy;

#include "GameObject.h"
#include "Constants.h"
#include "Moving.h"

//==================================================Bullet==============================================================
/**
 * @brief An abstract class to represent a moving object.
 */
class Bullet : public Moving {
public:
    /**
     * @brief Constructor of health bonus
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     * @param[in] direction Sets direction where to move
     * @param[in] damage Sets damage of a bullet
    */
    Bullet(int x, int y, int width, int height, int health, bool invulnerable, bool physical,
           char sym, GameObjectType type, Direction direction, int damage);

    Colors GetColor() override;

    char GetSym() override;

    /**
     * @brief Make bullet move (change coordinates)
     * @param[in] mvDirection Which direction to go
     */
    void Move(Direction mvDirection) override;

    /**
     * @brief Check collision with object next to the bullet
     * @param[in] game Instance of the game
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return Bullet collision object
     */
    shared_ptr<GameObject> CheckCollision(Game &game, int x, int y) override;

    /**
     * @brief Responsible for making damage. If there is, monitoring relation parameters
     * @param[in] game Instance of the game
     * @param[in] object What object do damage
     */
    void DoDamage(Game &game, shared_ptr<GameObject> &object);

    /**
     * @brief Interaction with objects (if bullet meets invulnerable object, it's just break up)
     * @param game Instance of the game
     * @param bulletCollisionObject Object to check
     * @param bullet Sent to find this bullet in vector of objects by shared pointer
     */
    void Interact(Game &game, shared_ptr<GameObject> &bulletCollisionObject, shared_ptr<Bullet> &bullet);

    /**
     * @brief Method erases this bullet from vector of objects and vector of bullets
     * @param game Instance of the game
     * @param bullet Bullet itself
     */
    void EraseBullet(Game &game, shared_ptr<Bullet> &bullet);
//----------Setters/Getters-------------------------------------------------------------------------
    void setDirection(Direction direction_set) { direction = direction_set; }

    Direction getDirection() const;
private:
    /**
     * @brief Bullet damage
     */
    int damage;
};

#endif //TANKS_BULLET_H
