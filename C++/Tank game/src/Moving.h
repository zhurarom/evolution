
#ifndef TANKS_MOVING_H
#define TANKS_MOVING_H

#include "Constants.h"
#include "GameObject.h"

//==================================================Moving==============================================================
/**
 * @brief An abstract class to represent moving object.
 */
class Moving : public GameObject {
public:
    /**
    * @brief Constructor of moving object
    * @param[in] x Spawn coordinate X
    * @param[in] y Spawn coordinate Y
    * @param[in] width Width of object
    * @param[in] height Height of object
    * @param[in] health Health points on start
    * @param[in] invulnerable Flag of possibility to destroy the object
    * @param[in] physical Flag of possibility to interact with the object
    * @param[in] sym Object's char
    * @param[in] type Object's type
    * @param[in] direction Sets direction where to move
   */
    Moving(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
           GameObjectType type,
           Direction direction);

    /**
     * @brief Move the object in a certain direction
     * @param[in] mvDirection In which direction to move
     */
    virtual void Move(Direction mvDirection) = 0;

    /**
     * @brief Check collision with object next to this
     * @param[in] game Instance of the game
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return Collision object
     */
    virtual shared_ptr<GameObject> CheckCollision(Game &game, int x, int y) = 0;

    Direction getDirection() { return direction; }

protected:
    /**
     * @brief Current direction of moving object
     */
    Direction direction;


};

#endif //TANKS_MOVING_H
