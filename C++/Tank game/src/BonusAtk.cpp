
#include "BonusAtk.h"

BonusAtk::BonusAtk(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
                   GameObjectType type) : Bonus(x, y, width, height, health, invulnerable, physical, sym, type) {}

char BonusAtk::GetSym() {
    return sym;
}

Colors BonusAtk::GetColor() {
    return COLOR_BONUS_ATK;
}

void BonusAtk::GiveBonus(shared_ptr<TankPlayer> &player) {
    int prevDamage = player->getTankDamage();
    player->setTankDamage(prevDamage + 1);
}
