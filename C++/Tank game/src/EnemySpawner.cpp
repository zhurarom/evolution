
#include "EnemySpawner.h"
#include "Game.h"


EnemySpawner::EnemySpawner(int x, int y, int width, int height, int health, bool invulnerable, bool physical,
                           char sym, GameObjectType type) :
        GameObject(x, y, width, height, health, invulnerable, physical, sym, type) {}

char EnemySpawner::GetSym() {
    return sym;
}

Colors EnemySpawner::GetColor() {
    return COLOR_ENEMY_SPAWNER;
}

void EnemySpawner::SpawnEnemy(Game &game) {
    game.IncAliveEnemies();
    Direction randomDirection = game.GetRandomDirection();
    shared_ptr<TankEnemy> enemy = make_shared<TankEnemy>(
            TankEnemy(x, y, 3, 3, 3, false, true, symbol_Enemy, GameObjectType_TankEnemy, randomDirection));
    game.vecEnemies.push_back(enemy);
    game.vecObjects.insert(game.vecObjects.begin(), enemy);
    game.RefreshLevel();
}

shared_ptr<GameObject> EnemySpawner::CheckCollision(Game &game) {
    for (const auto &i: game.vecObjects) {
        for (int w = 0; w < i->getWidth(); w++) //object's width
            for (int h = 0; h < i->getHeight(); h++) { //object's height
                for (int j = -1; j <= 1; ++j) //spawner's width
                    for (int k = -1; k <= 1; ++k) { //spawner's height
                        if (i->getType() == GameObjectType_TankPlayer || i->getType() == GameObjectType_TankEnemy) {
                            if (i->getX() + w - 1 == x + j && i->getY() + h - 1 == y + k) {
                                return i;
                            }
                        }
                    }
            }
    }
    return nullptr;
}
