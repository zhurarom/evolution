
#ifndef TANKS_CONSTANTS_H
#define TANKS_CONSTANTS_H

#include <string>
#include "ncurses.h"

//==================================================Constants==============================================================
using namespace std;

enum Colors {
    COLOR_BRICK_WALL = 1,
    COLOR_STEEL_WALL,
    COLOR_BASE,
    COLOR_TANK_PLAYER_BODY,
    COLOR_TANK_PLAYER_SYM,
    COLOR_TANK_ENEMY_BODY,
    COLOR_TANK_ENEMY_SYM,
    COLOR_BULLET,
    COLOR_ENEMY_SPAWNER,
    COLOR_BONUS_ATK,
    COLOR_BONUS_INV,
    COLOR_DEFAULT,
    COLOR_TEXT,
    COLOR_TEXT_MENU,
    COLOR_BUSH

};

enum GameObjectType {
    GameObjectType_BrickWall,
    GameObjectType_SteelWall,
    GameObjectType_Base,
    GameObjectType_TankPlayer,
    GameObjectType_TankEnemy,
    GameObjectType_Bullet,
    GameObjectType_EnemySpawner,
    GameObjectType_BonusAtk,
    GameObjectType_BonusInv,
    GameObjectType_Bush

};

enum Direction {
    Direction_Left,
    Direction_Up,
    Direction_Right,
    Direction_Down
};

const char symbol_SteelWall = '@';
const char symbol_BrickWall = '#';
const char symbol_Player = 'U';
const char symbol_Enemy = 'X';
const char symbol_Base = 'B';
const char symbol_Spawner = 'S';
const char symbol_Bush = '%';
const char symbol_BonusAtk = 'A';
const char symbol_BonusInv = 'H';
const char symbol_Bullet = '*';

static const int tankCooldown = 1300000;
static const int ticksBullet = 70000;
static const int ticksEnemySpawn = 3000000;
static const int ticksEnemyAnalize = 1000000;
static const int columnSize = 80;
static const int rowSize = 24;
static const int maxEnemies = 5;
static const int xStart = 49;
static const int yStart = 20;
static const int playerHealth = 3;
static const int winVariable = 1;
static const int loseVariable = 2;
static const int winAmount = 10;
static const string path = "./src/level.txt";

#endif //TANKS_CONSTANTS_H
