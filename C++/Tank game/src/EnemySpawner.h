
#ifndef TANKS_ENEMYSPAWNER_H
#define TANKS_ENEMYSPAWNER_H

#include "GameObject.h"

//==================================================Enemy spawner==============================================================
/**
 * @brief A class to represent an enemy spawner.
 */
class EnemySpawner : public GameObject {
public:
    /**
     * @brief Constructor of the enemy spawner
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     */
    EnemySpawner(int x, int y, int width, int height, int health, bool invulnerable, bool physical,
                 char sym, GameObjectType type);

    Colors GetColor() override;

    char GetSym() override;

    /**
     * @brief Check if something is staying on spawner position
     * @param[in] game Instance of the game
     * @return Collision object
     */
    shared_ptr<GameObject> CheckCollision(Game & game);

    /**
     * @brief Spawn an enemy from this spawner
     * @param[in] game Instance of the game
     */
    void SpawnEnemy(Game& game);

};


#endif //TANKS_ENEMYSPAWNER_H
