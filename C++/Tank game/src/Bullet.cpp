
#include "Bullet.h"
#include "Game.h"

Bullet::Bullet(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
               GameObjectType type, Direction direction, int damage) :
        Moving(x, y, width, height, health, invulnerable, physical, sym, type, direction), damage(damage) {}

char Bullet::GetSym() {
    return sym;
}

Colors Bullet::GetColor() {
    return COLOR_BULLET;
}

void Bullet::EraseBullet(Game &game, shared_ptr<Bullet> &bullet) {
    for (auto it = game.vecObjects.begin(); it != game.vecObjects.end(); it++) {
        if (bullet == *it) {
            game.vecObjects.erase(it);
            break;
        }
    }

    for (auto it = game.vecBullets.begin(); it != game.vecBullets.end(); it++) {
        if (bullet == *it) {
            game.vecBullets.erase(it);
            break;
        }
    }
}

void Bullet::Interact(Game &game, shared_ptr<GameObject> &bulletCollisionObject, shared_ptr<Bullet> &bullet) {
    if (!bulletCollisionObject->getInvulnerable()) {
        (*this).DoDamage(game, bulletCollisionObject);
    }
    EraseBullet(game, bullet);
}

void Bullet::DoDamage(Game &game, shared_ptr<GameObject> &object) {
    object->DecHealth(damage);
    if (object->getHealth() <= 0) {
        if (auto objEnemy = dynamic_pointer_cast<TankEnemy>(object)) {
            auto it = find(game.vecEnemies.begin(), game.vecEnemies.end(), objEnemy);
            if (it != game.vecEnemies.end()) {
                game.IncreaseScore();
                game.IncKilledEnemies();
                game.DecAliveEnemies();
                game.BonusChance(game, objEnemy); //chance to drop a bonus after death
                game.vecEnemies.erase(it);
            }
        } else if (auto objPlayer = dynamic_pointer_cast<TankPlayer>(object)) {
            game.ResetPlayer();
            objPlayer->DecLives();
            if (objPlayer->getLives() == 0) {
                game.GameOver(loseVariable);
            }
            return;
        } else if (auto objBase = dynamic_pointer_cast<Base>(object)) {
            game.GameOver(loseVariable);
        }
        game.vecObjects.erase(find(game.vecObjects.begin(), game.vecObjects.end(), object));
    }
}

void Bullet::Move(Direction mvDirection) {
    switch (mvDirection) {
        case Direction_Up: {
            setY(y - 1);
            break;
        }
        case Direction_Down: {
            setY(y + 1);
            break;
        }
        case Direction_Right: {
            setX(x + 1);
            break;
        }
        case Direction_Left: {
            setX(x - 1);
            break;
        }
        default: {
            setY(y - 2);
            break;
        }
    }
}

shared_ptr<GameObject> Bullet::CheckCollision(Game &game, int x, int y) {
    for (const auto &i: game.vecObjects) {
        if (i->getPhysical() || i->getType() == GameObjectType_BonusAtk || i->getType() == GameObjectType_BonusInv) {
            for (int w = 0; w < i->getWidth(); w++)
                for (int h = 0; h < i->getHeight(); h++) {
                    //for tanks
                    if (i->getType() == GameObjectType_TankPlayer || i->getType() == GameObjectType_TankEnemy) {
                        if (i->getX() + w - 1 == x && i->getY() + h - 1 == y) {
                            return i;
                        }
                    }
                        //for others
                    else {
                        if (i->getX() + w == x && i->getY() + h == y) {
                            return i;
                        }
                    }
                }
        }
    }
    return nullptr;
}

Direction Bullet::getDirection() const {
    return direction;
}
