
#ifndef TANKS_BONUS_H
#define TANKS_BONUS_H


#include "GameObject.h"
#include "TankPlayer.h"

//==================================================Bonus==============================================================
/**
 * @brief An abstract class to represent bonus.
 */
class Bonus : public GameObject {
public:
    /**
     * @brief Constructor of Bonus
     * @param[in] x Spawn coordinate X
     * @param[in] y Spawn coordinate Y
     * @param[in] width Width of object
     * @param[in] height Height of object
     * @param[in] health Health points on start
     * @param[in] invulnerable Flag of possibility to destroy the object
     * @param[in] physical Flag of possibility to interact the object
     * @param[in] sym Object's char
     * @param[in] type Object's type
     */
    Bonus(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
          GameObjectType type);

    virtual void GiveBonus(shared_ptr<TankPlayer> &player) = 0;

};


#endif //TANKS_BONUS_H
