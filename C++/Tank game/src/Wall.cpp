
#include "Wall.h"

Wall::Wall(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
           GameObjectType type) :
        GameObject(x, y, width, height, health, invulnerable, physical, sym, type) {}


char Wall::GetSym() {
    return sym;
}

Colors Wall::GetColor() {
    if (sym == '@') {
        return COLOR_STEEL_WALL;
    } else if (sym == '#') {
        return COLOR_BRICK_WALL;
    }
    return COLOR_DEFAULT;
}
