
#include "Moving.h"

Moving::Moving(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
               GameObjectType type,
               Direction direction) :
        GameObject(x, y, width, height, health, invulnerable, physical, sym, type),
        direction(direction) {}
