

#include "Game.h"

Game::Game() {
    gameRun = true;
    curLevel = make_shared<Level>(Level("1", path));
    score = 0;
    killedEnemies = 0;
    aliveEnemies = 0;
    playerName = "player";
}

void Game::Init() {
    initscr();
    cbreak();
    noecho();
    clear();
    refresh();
    curs_set(0);
    start_color();
    keypad(stdscr, true);
    nodelay(stdscr, true);
    cbreak();
    srand(time(nullptr));

    init_pair(COLOR_BRICK_WALL, COLOR_YELLOW, COLOR_YELLOW);
    init_pair(COLOR_STEEL_WALL, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(COLOR_BASE, COLOR_BLUE, COLOR_BLUE);
    init_pair(COLOR_TANK_PLAYER_SYM, COLOR_BLUE, COLOR_BLUE);
    init_pair(COLOR_TANK_PLAYER_BODY, COLOR_CYAN, COLOR_CYAN);
    init_pair(COLOR_TANK_ENEMY_BODY, COLOR_RED, COLOR_RED);
    init_pair(COLOR_TANK_ENEMY_SYM, COLOR_BLUE, COLOR_BLUE);
    init_pair(COLOR_BULLET, COLOR_WHITE, COLOR_BLACK);
    init_pair(COLOR_BUSH, COLOR_GREEN, COLOR_GREEN);
    init_pair(COLOR_ENEMY_SPAWNER, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_DEFAULT, COLOR_BLACK, COLOR_BLACK);
    init_pair(COLOR_BONUS_INV, COLOR_GREEN, COLOR_BLACK);
    init_pair(COLOR_BONUS_ATK, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_TEXT, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(COLOR_TEXT_MENU, COLOR_WHITE, COLOR_BLACK);

    if (!has_colors()) {
        endwin();
        printf("ERROR: Terminal does not support color.\n");
        exit(1);
    }

    NameRead();

    player = make_shared<TankPlayer>(
            TankPlayer(49, 20, 3, 3, 3, false, true, symbol_Player, GameObjectType_TankPlayer, Direction_Up, 3));
    vecObjects.push_back(player);

    curLevel->setLevelData();

}

void Game::BonusChance(Game &game, shared_ptr<TankEnemy> &objEnemy) {
    int bonusChance = rand() % 2; //chance to appear
    if (bonusChance == 1) {
        int bonusType = rand() % 2; //chance to generate some type
        GameObjectType type;
        if (bonusType == 0) {
            type = GameObjectType_BonusAtk;
        } else { type = GameObjectType_BonusInv; }
        game.CreateBonus(objEnemy->getX(), objEnemy->getY(), type);
    }
}

void Game::LoadLevel() {
    for (int i = 0; i < rowSize; i++) {
        for (int j = 0; j < columnSize; j++) {
            switch (curLevel->getLevelData()[i][j]) {
                case symbol_SteelWall: {
                    vecObjects.push_back(
                            make_shared<Wall>(
                                    Wall(j, i, 1, 1, 1, true, true, symbol_SteelWall, GameObjectType_SteelWall)));
                    break;
                }
                case symbol_BrickWall : {
                    vecObjects.push_back(
                            make_shared<Wall>(
                                    Wall(j, i, 1, 1, 1, false, true, symbol_BrickWall, GameObjectType_BrickWall)));
                    break;
                }
                case symbol_Bush : {
                    vecObjects.push_back(
                            make_shared<Bush>(Bush(j, i, 1, 1, 1, false, false, symbol_Bush, GameObjectType_Bush)));
                    break;
                }
                case symbol_Base: {
                    vecObjects.push_back(
                            make_shared<Base>(Base(j, i, 8, 2, 5, false, true, symbol_Base, GameObjectType_Base)));
                    break;
                }
                case symbol_Spawner: {
                    shared_ptr<EnemySpawner> spawner = make_shared<EnemySpawner>(
                            EnemySpawner(j, i, 1, 1, 1, false, false, symbol_Spawner, GameObjectType_EnemySpawner));
                    vecSpawners.push_back(spawner);
                    break;
                }
            }
        }
    }
}

void Game::PrintTitle(string &title) {
    attron(COLOR_PAIR(COLOR_TEXT_MENU));
    mvprintw(rowSize / 2 - 4, (columnSize - title.length()) / 2, title.c_str());
}


void Game::GameOver(int situation) {
    clear();
    ProcessScore();
    int key = 0;
    string titleGameOver = "Game over!", titleWon = "You won!", titleLost = "You lose";
    attron(A_BOLD);
    attron(COLOR_PAIR(COLOR_TEXT_MENU));
    mvprintw(rowSize / 2 - 5, (columnSize - titleGameOver.length()) / 2, titleGameOver.c_str());
    attroff(A_BOLD);
    if (situation == 1) {
        PrintTitle(titleWon);
    } else if (situation == 2) {
        PrintTitle(titleLost);
    }
    while (true) {
        if ((key = getch()) > 0 && (key == 'q' || key == 'Q')) {
            gameRun = false;
            break;
        }
    }
}

void Game::RefreshBullet() {
    for (const auto &object : vecBullets) {
        object->Draw();
    }
    RefreshLevel();
    refresh();
}

void Game::RefreshLevel() {
    clear();
    for (const auto &object : vecObjects) { //Draw every object on map
        object->Draw();
    }
    PrintScore();
    player->PrintStats();
    refresh();
}

void Game::PrintScore() {
    attron(A_BOLD);
    attron(COLOR_PAIR(COLOR_TEXT));
    mvprintw(0, 1, "Score: %d", score);
    attroff(A_BOLD);
    refresh();
}

void Game::ProcessScore() {
    fstream sc("./src/leaderboards.txt");
    if (!sc.is_open()) {
        attron(COLOR_PAIR(COLOR_TEXT_MENU));
        mvprintw(rowSize / 2, columnSize / 2 - 6, "Failed to load leaderboards.");
    } else {
        attron(A_BOLD);
        attron(COLOR_PAIR(COLOR_TEXT_MENU));
        string strLb = "Leaderboards";
        mvprintw(rowSize / 2 - 2, (columnSize - strLb.length()) / 2, strLb.c_str());
        attroff(A_BOLD);
        multimap<string, int> scores;
        string buffer;
        int scoreBuffer = 0, padding = 0;
        attron(COLOR_PAIR(COLOR_TEXT_MENU));
        while (getline(sc, buffer)) { // Load previous leaders.
            stringstream ss(buffer);
            string nameBuffer;
            ss >> nameBuffer;
            ss >> scoreBuffer;
            nameBuffer.pop_back(); // Delete ':' from the name.
            scores.insert({nameBuffer, scoreBuffer});
        }
        if (playerName.empty()) {
            playerName = "player";
        }
        scores.insert({playerName, score});
        sc.close();
        ofstream out("./src/leaderboards.txt", ofstream::out | ofstream::trunc);
        vector<pair<string, int>> pairs; // Sorted map by score.
        for (auto &score: scores) pairs.push_back(score);
        sort(pairs.begin(), pairs.end(),
             [](pair<string, int> &a, pair<string, int> &b) { return a.second > b.second; });
        int counter = 0;
        for (const auto &i: pairs) { // Write new leaderboard.
            if (counter > 8) {
                break;
            }
            counter++;
            stringstream ss;
            ss << i.second;
            string strScore = ss.str();
            mvprintw(rowSize / 2 - padding--, (columnSize - i.first.length() - strScore.length() - 1) / 2, "%s: %d",
                     i.first.c_str(), i.second);
            out << i.first << ": " << i.second << endl;
        }
        out.close();
    }
}

void Game::NameRead() {
    clear();
    string name;
    bool written = false;
    while (!written) {
        int key;
        attron(A_BOLD);
        attron(COLOR_PAIR(COLOR_TEXT_MENU));
        mvprintw(rowSize / 2 - 2, columnSize / 2 - 10, "Welcome to the TANK game!");
        attroff(A_BOLD);
        mvprintw(rowSize / 2, columnSize / 2 - 10, "Enter your name: %s", name.c_str());
        if ((key = getch()) > 0) { // Enter key
            switch (key) {
                case '\n': {
                    written = true;
                    break;
                }
                case KEY_BACKSPACE: {
                    if (!name.empty()) {
                        name.pop_back();
                        clear();
                    }
                    break;
                }
                case 'q': {
                    written = true;
                    gameRun = false;
                    Close();
                    break;
                }
                default: {
                    name += key;
                }
            }
        }
        refresh();
    }
    playerName = name;
}


void Game::AddEnemy() {
    int spawnerNum = rand() % vecSpawners.size(); //choose random spawner to spawn an enemy
    if (!vecSpawners[spawnerNum]->CheckCollision(*this)) {
        if (aliveEnemies < maxEnemies) {
            vecSpawners[spawnerNum]->SpawnEnemy(*this);
        }
    }
}


void Game::Run() {
    RefreshLevel();
    int pressed_key = 0, bulletTicks = ticksBullet, enemyAnalizeTicks = ticksEnemyAnalize, enemySpawnTicks = ticksEnemySpawn;
    AddEnemy(); //add first enemy before cool down
    while (gameRun) {
        if (killedEnemies == winAmount) {
            GameOver(winVariable);
        }
        if (enemySpawnTicks-- == 0) {
            enemySpawnTicks = ticksEnemySpawn;
            AddEnemy(); //spawns an enemy
        }
        if (enemyAnalizeTicks-- == 0) {
            enemyAnalizeTicks = ticksEnemyAnalize;
            for (auto &i:vecEnemies) {
                i->Analize(*this);
                RefreshLevel();
            }
        }
        if (bulletTicks-- == 0) {
            bulletTicks = ticksBullet;
            BulletsMovement();
        }
        player->decCooldown();
        if ((pressed_key = getch()) > 0) {
            if (HandlePressedKey(pressed_key)) { //Enter key
                RefreshLevel();
            }
        }
    }
    Close();
}

bool Game::PlayerMovement(Direction direction, int xDelta, int yDelta) {

    shared_ptr<GameObject> collisionObject = player->CheckCollision(*this, player->getX() + xDelta,
                                                                    player->getY() + yDelta);
    if (!collisionObject) {
        player->Move(direction);
        return true;
    } else {
        if (player->getDirection() != direction) { //Turn if has another direction
            player->Turn(direction);
            return true;
        }

        auto objAtk = dynamic_pointer_cast<BonusAtk>(collisionObject);
        auto objInv = dynamic_pointer_cast<BonusInv>(collisionObject);

        if (objAtk != nullptr) { //pick up a bonus
            objAtk->GiveBonus(player);
            vecObjects.erase(find(vecObjects.begin(), vecObjects.end(), collisionObject));
            player->Move(direction);
            return true;
        }

        if (objInv != nullptr) {
            objInv->GiveBonus(player);
            vecObjects.erase(find(vecObjects.begin(), vecObjects.end(), collisionObject));
            player->Move(direction);
            return true;
        }
    }
    return false;
}

bool Game::HandlePressedKey(int keyCode) {
    switch (keyCode) {
        case KEY_LEFT: {
            return PlayerMovement(Direction_Left, -2, 0);
        }
        case KEY_RIGHT: {
            return PlayerMovement(Direction_Right, 2, 0);
        }
        case KEY_DOWN: {
            return PlayerMovement(Direction_Down, 0, 2);
        }
        case KEY_UP: {
            return PlayerMovement(Direction_Up, 0, -2);
        }
        case ' ': {
            if (player->getCooldown() == 0) {
                player->resetCooldown();
                player->Fire(*this);
            }
            return true;
        }
        case 'q': { //quit
            gameRun = false;
            return true;
        }
        case 'r': { //instant loose
            GameOver(loseVariable);
            return true;
        }
        default: {
            return false;
        }
    }
}

void Game::BulletsMovement() {
    for (auto &i : vecBullets) {
        int x_collision = i->getX(), y_collision = i->getY();
        switch (i->getDirection()) {
            case Direction_Up : {
                y_collision--;
                break;
            }
            case Direction_Down: {
                y_collision++;
                break;
            }
            case Direction_Left: {
                x_collision--;
                break;
            }
            case Direction_Right: {
                x_collision++;
                break;
            }
        }
        shared_ptr<GameObject> bulletCollisionObject = i->CheckCollision(*this, x_collision, y_collision);
        if (!bulletCollisionObject) {
            i->Move(i->getDirection());
            RefreshLevel();
        } else {
            i->Interact(*this, bulletCollisionObject, i);
            RefreshLevel();
            break;
        }
    }
}


Direction Game::GetRandomDirection() {
    auto randomDirection = (Direction) (rand() % 4);
    return randomDirection;
}

void Game::IncreaseScore() {
    score += 500;
}

void Game::CreateBonus(int x, int y, GameObjectType type) {
    if (type == GameObjectType_BonusAtk) {
        shared_ptr<BonusAtk> bonusAtk = make_shared<BonusAtk>(
                BonusAtk(x, y, 1, 1, 1, false, false, symbol_BonusAtk, GameObjectType_BonusAtk));
        vecObjects.insert(vecObjects.begin(), bonusAtk);

    } else if (type == GameObjectType_BonusInv) {
        shared_ptr<BonusInv> bonusInv = make_shared<BonusInv>(
                BonusInv(x, y, 1, 1, 1, false, false, symbol_BonusInv, GameObjectType_BonusInv));
        vecObjects.insert(vecObjects.begin(), bonusInv);
    }
    RefreshLevel();
}

void Game::ResetPlayer() {
    player->setX(xStart);
    player->setY(yStart);
    player->setDirection(Direction_Up);
    player->setHealth(playerHealth);
    RefreshLevel();
}

void Game::Close() {
    endwin();
}

void Game::IncKilledEnemies() {
    killedEnemies++;
}

void Game::DecAliveEnemies() {
    aliveEnemies--;
}

void Game::IncAliveEnemies() {
    aliveEnemies++;
}

