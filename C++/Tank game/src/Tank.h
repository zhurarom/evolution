
#ifndef TANKS_TANK_H
#define TANKS_TANK_H

#include "GameObject.h"
#include "Constants.h"
#include "Bullet.h"
#include "Moving.h"

//==================================================Tank==============================================================
/**
 *  @brief An abstract class to represent a tank
 */
class Tank : public Moving {
public:
    /**
    * @brief Constructor of moving object
    * @param[in] x Spawn coordinate X
    * @param[in] y Spawn coordinate Y
    * @param[in] width Width of object
    * @param[in] height Height of object
    * @param[in] health Health points on start
    * @param[in] invulnerable Flag of possibility to destroy the object
    * @param[in] physical Flag of possibility to interact with the object
    * @param[in] sym Object's char
    * @param[in] type Object's type
    * @param[in] direction Sets direction where to move
   */
    Tank(int x, int y, int width, int height, int health, bool invulnerable, bool physical, char sym,
         GameObjectType type,
         Direction direction);

    void Draw() override;

    void Move(Direction mvDirection) override;

    /**
     * @brief Virtual method that gets color of tank's hull (may be player or enemy)
     * @return Tank hull color
     */

    /**
    *@brief Creates a bullet
    * @param[in] game Instance of the game
    */
    void Fire(Game &game);

    virtual Colors GetBodyColor() = 0;

    /**
     * @brief Turn the tank
     * @param[in] trDirection In which direction to turn
     */
    void Turn(Direction trDirection);

    /**
     * @brief Check collision with another object
     * @param[in] game Instance of the game
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return Collision object
     */
    shared_ptr<GameObject> CheckCollision(Game &game, int x, int y) override;

    /**
     * @brief Check object 3 cells above or below tank for collisions
     * @param[in] i Which object to check
     * @param[in] w Object width at this iteration
     * @param[in] h Object height at this iteration
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return True if if there is collision, false if there isn't
     */
    bool VerticalCollision(shared_ptr<GameObject> &i, int &w, int &h, int &x, int &y);

    /**
     * @brief Check object 3 cells left or right of tank for collisions
     * @param[in] i Which object to check
     * @param[in] w Object width at this iteration
     * @param[in] h Object height at this iteration
     * @param[in] x Coordinate X
     * @param[in] y Coordinate Y
     * @return True if if there is collision, false if there isn't
     */
    bool HorizontalCollision(shared_ptr<GameObject> &i, int &w, int &h, int &x, int &y);

    /**
     * @brief Decrements cool down of tank
     */
    void decCooldown();

    /**
     * @brief Resets cool down
     */
    void resetCooldown() { cooldown = tankCooldown; }

//----------Setters/Getters-------------------------------------------------------------------------
    void setDirection(Direction set_direction) { direction = set_direction; }

    void setTankDamage(const int &damage) { tankDamage = damage; }

    int getTankDamage() { return tankDamage; }

    int getCooldown() const;
//--------------------------------------------------------------------------------------------------
protected:
    /**
     * @brief Tank's cool down
     */
    int cooldown;
    /**
     * @brief Tank's damage
     */
    int tankDamage;
};


#endif //TANKS_TANK_H
