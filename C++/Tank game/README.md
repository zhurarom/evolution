# Tank Game (2019)

This game is modeled after Battle city (Namco, 1985).
It's a multi-directional shooter game where the player, controlling a tank, must destroy enemy tanks in  level, which enter the playfield from the top of the screen. The enemy tanks attempt to destroy the human tank, as well as the player's base.  Level is completed when the player destroys all 10 enemy Tanks, but the game ends if the player loses all available lives or the player's base is destroyed. When the game is over, the score is recorded in the file in ascending order.
 Map includes brick walls that can be destroyed by having either the player's Tank or an enemy Tank shoot at them and steel walls that can't be destroyed. There is also a certain chance that when killing an enemy, a bonus will appear that will make the player invulnerable for the time being. 
 As well it implies the same game together on one computer.
