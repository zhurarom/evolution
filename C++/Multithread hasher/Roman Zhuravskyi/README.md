Signature
You need to write a command line program in C++ to generate the signature of a given file. The signature
is generated as follows: the source file is divided into equal (fixed) length blocks. When the source file size
is not divisible by block size, the last fragment can be shorter or complemented with zeroes to full block
size. A hash value is calculated for each block and then added to the output signature file.

Interface: Command line containing the following data:
• Input file path
• Output file path
• Block size (default size is 1 MB)

Mandatory Requirements:
• The program execution speed should be optimized as much as possible for the multiprocessing
environment
• Exception-based error handling must be implemented
• Smart pointers must be used for resource management


The application must be built according to the principles of object-oriented programming. Avoid
using functional programming or metaprogramming methods for this task
Assumptions:
• The input file size can be much bigger than the size of accessible physical memory (> 4 GB)
• You can use Boost
• Any hash function can be used (MD5, CRC, etc.)

Note: Please submit the whole project, not just the source code.
