import numpy as np
import curses

"""Printing of a final result"""


def make_print(screen, visited, path):
    current_row = 0
    print_center(screen, "S - Start", current_row)
    current_row += 1
    print_center(screen, "F - Finish", current_row)
    current_row += 1
    print_center(screen, "Total nodes: '{}'".format(len(visited)), current_row)
    current_row += 1
    print_center(screen, "Length of path: '{}'".format(len(path) - 1), current_row)


"""Printing of a final result in a center of screen"""


def print_center(screen, text, current_row):
    # screen.clear()
    curses.init_pair(12, curses.COLOR_BLACK, curses.COLOR_WHITE)
    h, w = screen.getmaxyx()
    x = w // 2 - len(text) // 2
    y = h // 2 + current_row

    screen.attron(curses.color_pair(12))
    screen.addstr(y, x, text)
    screen.attroff(curses.color_pair(12))

    screen.refresh()


"""Change our values to 1 - wall, 0 - free space, S - start, F - finish"""


def change_values(tmp_maze, start, finish):
    tmp_maze[tmp_maze == 'X'] = 1
    tmp_maze[tmp_maze == ' '] = 0
    tmp_maze[start[1]][start[0]] = 'S'
    tmp_maze[finish[1]][finish[0]] = 'F'
    return tmp_maze


def show(maze):
    for line in maze:
        print(*line)
    print()


"""Transform our list to numpy array"""


def to_matrix(lines):
    matrix = [list(line) for line in lines]
    tmp_maze = np.asarray(matrix)
    tmp_maze = tmp_maze[..., :-1]
    return tmp_maze


"""Take "number" cooridates of start and finish"""


def take_first_coordinates(lines, f_start, f_finish):
    col_start, row_start = int(f_start[1][:-1]), int(f_start[2])
    col_finish, row_finish = int(f_finish[1][:-1]), int(f_finish[2])

    start = col_start, row_start
    finish = col_finish, row_finish

    del lines[-2:]

    return start, finish


"""For spliting of start and finish coordinates """


def split(lines):
    f_start = lines[-2].split()
    f_finish = lines[-1].split()
    return f_start, f_finish


"""Starting of preprocessing. After this function we will have maze, start and finish coordinates"""


def start_preprocessing(f):
    lines = f.readlines()
    f_start, f_finish = split(lines)
    start, finish = take_first_coordinates(lines, f_start, f_finish)
    tmp_maze = to_matrix(lines)
    maze = change_values(tmp_maze, start, finish)
    return maze, start, finish
