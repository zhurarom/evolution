from preprocessing import start_preprocessing
from collections import deque
from to_graph import mazetograph
from animation import make_animation
from preprocessing import make_print
import time

"""DFS algorithm"""


def find_path_dfs(maze, start, finish, screen):
    stack = deque([([start], start)])
    visited = set()
    step_inverted = {'L': (-1, 0), 'D': (0, 1), 'R': (1, 0), 'U': (0, -1)}
    '''Transforming of maze into graph'''
    graph = mazetograph(maze, step_inverted)
    while stack:
        make_animation(maze, screen)

        path, current = stack.pop()

        if current == finish:
            return path, visited

        if current in visited:
            continue
        visited.add(current)

        if maze[current[1]][current[0]] != "S":
            maze[current[1]][current[0]] = "*"

        for neighbour in graph[current]:
            stack.append((path + [neighbour], neighbour))


'''
    This function we call from start.py. Here we start preprocessing and finding a solve of a maze 
'''


def start_dfs(screen, file):
    maze, start, finish = start_preprocessing(file)
    path, visited = find_path_dfs(maze, start, finish, screen)

    '''Use "P" for final path'''
    for col, row in path:
        make_animation(maze, screen)
        if maze[row][col] != "S":
            maze[row][col] = "P"

    time.sleep(1)

    make_print(screen, visited, path)
