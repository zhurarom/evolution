;Header and description
(define (domain happy)
    (:requirements :strips)

    (:predicates
        (place ?where)
        (connect_land ?from ?to)
        (connect_water ?from ?to)
        (have ?item)
        (is ?which)
    )

    ;CRAFTING OF SHIP ======================================================================
    (:action postavit_clun
        :parameters ()
        :precondition (and (have drevo))
        :effect (and (have clun)
                     (not (have drevo)) )
    )

    (:action postavit_fregat
        :parameters ()
        :precondition (and (have drevo) 
                           (have clun) 
                           (have zlate_zrnko))
        :effect (and (have fregat)
                     (not (have drevo))
                     (not (have clun))
                     (not (have zlate_zrnko)) )
    )

    (:action postavit_karavelu
        :parameters ()
        :precondition (and (have drevo) 
                           (have clun) 
                           (have zlate_mince))
        :effect (and (have karavela)
                     (not (have drevo))
                     (not (have clun))
                     (not (have zlate_mince)) )
    )

    ;POHYB ================================================================================
    (:action pohyb_zeme
        :parameters (?from ?to)
        :precondition (and (place ?from) 
                      (connect_land ?from ?to) )
        :effect (and (place ?to)
                     (not (place ?from)) )
    )

    (:action pohyb_voda_clunem
        :parameters (?from ?to)
        :precondition (and (place ?from) 
                           (connect_water ?from ?to)
                           (have clun) )
        :effect (and (place ?to)
                     (not (place ?from)) )
    )

    (:action pohyb_voda_fregatem
        :parameters (?from ?to)
        :precondition (and (place ?from) 
                           (connect_water ?from ?to) 
                           (have fregat) )
        :effect (and (place ?to)
                     (not (place ?from)) )
    )

    (:action pohyb_voda_karavelou
        :parameters (?from ?to)
        :precondition (and (place ?from) 
                           (connect_water ?from ?to) 
                           (have karavela) )
        :effect (and (place ?to)
                     (not (place ?from)) )
    )

    ;Alkohol ========================================================================
    (:action nametit_se
        :parameters ()
        :precondition (and (have alkohol) 
                           (not (is nameteny_stav))
                           (not (is opily_stav)) )
        :effect (and (is nameteny_stav)
                     (not (have alkohol)) )
    )
    
    (:action opit_se
        :parameters ()
        :precondition (and (have alkohol) 
                           (is nameteny_stav) )
        :effect (and (is opily_stav)
                     (not (is nameteny_stav))
                     (not (have alkohol)) )
    )
    
    (:action stat_se_zavislym
        :parameters ()
        :precondition (and (have alkohol) 
                           (is opily_stav) )
        :effect (and (is zavisly_stav)
                     (not (have alkohol)) )
    )
    

    ;Les ============================================================================
    (:action les_kacet_drevo
        :parameters ()
        :precondition (and (place les) )
        :effect (and (have drevo) )
    )
    
    (:action les_zbirat_kvetiny
        :parameters ()
        :precondition (and (place les) )
        :effect (and (have kvetiny) )
    )

    (:action les_porvat_s_medvedem
        :parameters ()
        :precondition (and (place les))
        :effect (and (have medvedi_kuze) 
                     (is slava)
                     (is zoceleni))
    )
    
    (:action les_setkani_s_dedeckem 
        :parameters ()
        :precondition (and (place les) (have alkohol))
        :effect (and (have mapa)
                     (have pochybne_znamosti)
                     (not (have alkohol)))
    )
    

    ;Reka ===========================================================================
    ; (:action reka_ukrast_clun
    ;     :parameters ()
    ;     :precondition (and (place reka) )
    ;     :effect (and (have clun) 
    ;                  (is zaznam_v_rejstriku))
    ; )

    (:action reka_ryzovat_zlato
        :parameters ()
        :precondition (and (place reka) )
        :effect (and (have zlate_zrnko))
    )

    (:action reka_doprat_ledovou_koupel
        :parameters ()
        :precondition (and (place reka) )
        :effect (and (not (is opily_stav)) )
    )

    ;Pristav ========================================================================
    (:action pristav_prace
        :parameters ()
        :precondition (and (place pristav))
        :effect (and (have zlate_zrnko) )
    )

    (:action pristav_obchodovani_kokos
        :parameters ()
        :precondition (and (place pristav)
                           (have kokos) )
        :effect (and (have zlate_mince) 
                     (not (have kokos)) )
    )

    (:action pristav_obchodovani_medvedi_kuze
        :parameters ()
        :precondition (and (place pristav)
                           (have medvedi_kuze) )
        :effect (and (have zlate_mince) 
                     (not (have medvedi_kuze)) )
    )

    (:action pristav_seznameni_s_paseraky
        :parameters ()
        :precondition (and  (place pristav)
                            (have zlata_cihle)
                            (have pochybne_znamosti) )
        :effect (and (is seznameni_s_paseraky) )
    )


    ;Hospoda ========================================================================
    (:action hospoda_kup_alkohol
        :parameters ()
        :precondition (and  (place hospoda)
                            (have zlate_zrnko) )
        :effect (and (have alkohol) 
                     (not (have zlate_zrnko)) )
    )

    (:action hospoda_zaplatit_runde
        :parameters ()
        :precondition (and  (place hospoda)
                            (have zlate_mince) )
        :effect (and (have dobre_znamosti) 
                     (not (have zlate_mince)) )
    )

    (:action hospoda_zacelit_v_bitce
        :parameters ()
        :precondition (and  (place hospoda)
                            (is nameteny_stav) )
        :effect (and (is zoceleni) )
    )


    ;Mesto ==========================================================================
    (:action mesto_stradani_mince
        :parameters ()
        :precondition (and  (place mesto)
                            (have zlate_zrnko) )
        :effect (and (have zlate_mince) 
                     (have dobre_znamosti) 
                     (not (have zlate_zrnko)) )
    )

    (:action mesto_investovani_cihle
        :parameters ()
        :precondition (and  (place mesto)
                            (have zlate_mince) )
        :effect (and (have zlata_cihle) 
                     (have dobre_znamosti) 
                     (not (have zlate_mince)) )
    )

    (:action mesto_pustit_do_zlodejny
        :parameters ()
        :precondition (and (place mesto))
        :effect (and (have zlate_mince) 
                     (is zaznam_v_rejstriku) )
    )

    (:action mesto_koupit_odstupenky
        :parameters ()
        :precondition (and  (place mesto)
                            (is zaznam_v_rejstriku)
                            (have zlate_zrnko) )
        :effect (and (not (have zlate_zrnko))
                     (not (is zaznam_v_rejstriku)))
    )

    (:action mesto_venovat_se_verejne_prace
        :parameters ()
        :precondition (and (place mesto)
                           (is zaznam_v_rejstriku))
        :effect (and (is nameteny_stav)
                     (not (is zaznam_v_rejstriku)))
    )


    ;Akademie =======================================================================
    (:action akademie_stat_kapitanem
        :parameters ()
        :precondition (and  (place akademie)
                            (have zlate_mince)
                            (not (is zaznam_v_rejstriku)))
        :effect (and (is kapitan)
                     (is slava)
                     (not (have zlate_mince)))
    )


    ;More ===========================================================================
    (:action more_narazit_na_piraty
        :parameters ()
        :precondition (and (place more)
                           (not (is zoceleni)) )
        :effect (and (is zoceleni)
                     (not (have zlate_zrnko))
                     (not (have zlate_mince))
                     (not (have zlata_cihle))
                     (not (have fregat))
                     (not (have karavela)) )
    )

    (:action more_porazit_piraty
        :parameters ()
        :precondition (and (place more)
                           (is zoceleni)
                           (have karavela) )
        :effect (and (have zlate_mince)
			         (have zlata_cihle)
			         (have zlate_zrnko)
	                 (have clun)
			         (have fregat)
			         (is slava)
			         (have porazene_piraty))
    )

    (:action more_pridat_k_piratum
        :parameters ()
        :precondition (and (place more)
                           (have pochybne_znamosti) )
        :effect (and (is nameteny_stav) )
    )

    (:action more_dostat_perlu
        :parameters ()
        :precondition (and (place more) )
        :effect (and (have perla) )
    )

    (:action more_doprat_ledovou_koupel
        :parameters ()
        :precondition (and (place more) )
        :effect (and (not (is opily_stav)) )
    )

    ;Majak ==========================================================================
    (:action majak_vzit_divku
        :parameters ()
        :precondition (and (place majak)
                           (is slava))
        :effect (and (have divka)
                     (not (is opily_stav)) )
    )

    ;Ostrov =========================================================================
    (:action ostrov_najit_kokaine
        :parameters ()
        :precondition (and (place ostrov)
                           (have mapa))
        :effect (and (have kokaine) )
    )
    
    (:action ostrov_kacet_drevo_ostrov
        :parameters ()
        :precondition (and (place ostrov))
        :effect (and (have drevo) )
    )

    (:action ostrov_sbir_kokosu_ostrov
        :parameters ()
        :precondition (and (place ostrov))
        :effect (and (have kokos) )
    )


    ;Special for happy ==============================================================
    (:action vyrobit_prsten
        :parameters ()
        :precondition (and (have zlata_cihle) (have perla))
        :effect (and (have prsten) 
                     (not (have zlata_cihle))
                     (not (have perla)) )
    )


    ;Goal for happy =================================================================
    (:action stastny_svatba
        :parameters ()
        :precondition (and (place ostrov)
                           (have divka) 
                           (have prsten)
                           (have kvetiny) 
                           (have dobre_znamosti)
                           (not (is zaznam_v_rejstriku))
                           (not (is opily_stav))
                           (not (is zavisly_stav)))
        :effect (and (is stastny) )
    )

    (:action stastny_admiral
        :parameters ()
        :precondition (and (place akademie)
                           (is kapitan)
                           (have porazene_piraty)
                           (not (is opily_stav))
                           (not (is nameteny_stav)))
        :effect (and (is stastny) )
    )

    (:action stastny_narkoman
        :parameters ()
        :precondition (and (is seznameni_s_paseraky)
                           (have fregat)
                           (have kokaine)
                           (is zavisly_stav))
        :effect (and (is stastny) )
    )
)