from variables import *


# Class, which represent walls
class Obstacle(object):
    def __init__(self, x, y, width, height):
        self.x, self.y = x, y
        self.width, self.height = width, height

        # We need this for collision detection
        self.subsurface = pg.Surface((self.width, self.height))

    # Draw a rectangle where we have an object
    def draw(self):
        pg.draw.rect(SC, (255, 100, 100), (self.x, self.y, self.width, self.height))
