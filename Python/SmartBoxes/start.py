from dna import *
import sys
from obstacle import *
from variables import *


# Quick way to show text on the screen
def ShowText(string, x, y, size=40, color=(255, 255, 255)):
    funcFont = pg.font.SysFont(None, size)
    SC.blit(funcFont.render(string, True, color), (x, y))


# Represent our box
class SmartBox(object):
    def __init__(self, dna=None):
        self.alive = True
        self.crashed = False
        self.won = False

        # Selection for gene pool, only for the fastest boxes
        self.wonTime = 0

        # Make dna(2d vector), it`s a brain of our box
        if dna:
            self.gene = DNA(dna)
        else:
            self.gene = DNA()

        self.x = 10
        self.y = SH // 2

        # For box
        self.size = 3
        self.acc = pg.math.Vector2()
        self.acc.xy = 0, 0
        self.vel = pg.math.Vector2()
        self.vel.xy = 0, 0
        self.velLimit = 50  # Speed of boxes

        self.burstColor = pg.Color("orange")
        self.burstSize = 60
        self.fitness = 0

        # We need this to check for collisions
        self.subsurface = pg.Surface((self.size, self.size))
        self.subsurface.fill((50, 215, 240))
        self.subsurface.set_alpha(320)

    def check_for_overlap(self, item):
        if self.subsurface.get_rect(topleft=(self.x, self.y)).colliderect(
                item.subsurface.get_rect(topleft=(item.x, item.y))):
            self.crashed = True

    def collision_with_wall(self):
        if self.x + self.size > SW or self.x < 0 or self.y < 0 or self.y + self.size > SH:
            self.crashed = True

    # Check for collision with window or a bariere
    def CheckCollision(self, arr):
        global aliveBoxCount

        # Check for walls
        self.collision_with_wall()

        # Check for overlap between self and item
        for item in arr:
            self.check_for_overlap(item)

        # Dead
        if self.crashed:
            self.alive = False
            aliveBoxCount -= 1

    # If our box is closer, it will get better fitness
    def culc_fitness(self):
        # Calculate distance
        dist = math.sqrt(math.pow(self.x - finish.x, 2) + math.pow(self.y - finish.y, 2))
        # Here we remapping our distance from 0 to 1. The best fitness is 1, we found our goal
        self.fitness = 1 + (dist - 0) * (0 - 1) / (SW - 0)


    def what_if_alive(self):
        self.acc = self.gene.array[frameCount]
        if self.subsurface.get_rect(topleft=(self.x, self.y)).colliderect(winRect) and not self.won:
            self.won = True
            self.wonTime = frameCount
        if self.won:
            self.x, self.y = finish.x, finish.y
            self.vel.xy = 0, 0
            self.acc.xy = 0, 0
            self.alive = False

    # For printing of a object we need to update it every frame
    def make_update(self):
        if self.crashed:
            # make red, if our box is dead
            self.subsurface.fill((128, 0, 0))
        if self.alive:
            self.what_if_alive()

        # Adding acc to speed + changing of position
        self.vel += self.acc
        if self.vel.x > self.velLimit and self.acc.x > 0:
            self.vel.x = self.velLimit
        if self.vel.x < -self.velLimit and self.acc.x < 0:
            self.vel.x = -self.velLimit
        if self.vel.y > self.velLimit and self.acc.y > 0:
            self.vel.y = self.velLimit
        if self.vel.y < -self.velLimit and self.acc.y < 0:
            self.vel.y = -self.velLimit
        self.x += self.vel.x
        self.y += self.vel.y

    # Direction for drawing of a tail
    def direction_for_draw(self):
        if math.fabs(self.vel.x) > math.fabs(self.vel.y):
            if self.vel.x > 0:
                pg.draw.rect(SC, self.burstColor, (self.x - 5, self.y + 3, self.burstSize, 3))
            else:
                pg.draw.rect(SC, self.burstColor, (self.x + 10, self.y + 3, self.burstSize, 3))
        else:
            if self.vel.y > 0:
                pg.draw.rect(SC, self.burstColor, (self.x + 3, self.y - 5, 3, self.burstSize))
            else:
                pg.draw.rect(SC, self.burstColor, (self.x + 3, self.y + 10, 3, self.burstSize))

    def change_color_for_frame(self):
        if frameCount % 5 == 0:
            self.burstColor = pg.Color("red")
            self.burstSize = 5
        else:
            self.burstColor = pg.Color("purple")
            self.burstSize = 10

        # Direction for drawing of a tail
        self.direction_for_draw()

    # If box is alive, change a color
    def draw(self):
        if self.alive:
            self.change_color_for_frame()
        SC.blit(self.subsurface, (self.x, self.y))


# Set up the goal point
finish = pg.math.Vector2()
finish.xy = SW - 50, SH // 2
winSurface = pg.Surface((80, 80))
winRect = winSurface.get_rect(topleft=(finish.x - 40, finish.y - 40))

# Start configuration
boxes = []
for i in range(boxCount):
    boxes.append(SmartBox())


def create_a_child_using_crossover():
    for i, box in enumerate(boxes):
        randomIndex = random.randint(0, len(genePool) - 1)
        parentA = genePool[randomIndex].gene
        randomIndex = random.randint(0, len(genePool) - 1)
        parentB = genePool[randomIndex].gene
        child = parentA.CrossOver(parentB)
        boxes[i] = SmartBox(child.array)


def make_new_generation():
    global generationCount
    boxes.clear()
    generationCount = 0
    for i in range(boxCount):
        boxes.append(SmartBox())


# Represent a process of a game
def FinishGeneration():
    global generationCount, frameCount, levelCount, lowestTime
    global finished, avgFitness, moveLimit, walls, successCount
    global levelColor, avgFitnessD, lowestTimeD, successCountD, aliveBoxCount

    tempLowestTime = lowestTime
    tempAvgFitness = avgFitness
    tempSuccessCount = successCount
    genePool.clear()
    maxFit = 0
    lowestTime = moveLimit
    lowestIndex = 0
    successCount = 0
    avgFitnessSum = 0
    maxFitIndex = 0

    for box in boxes:
        box.culc_fitness()
        avgFitnessSum += box.fitness
        if box.fitness >= 1.0:
            successCount += 1
        if box.fitness > maxFit:
            maxFit = box.fitness
            maxFitIndex = boxes.index(box)

    successCountD = successCount - tempSuccessCount
    avgFitness = avgFitnessSum / len(boxes)
    avgFitnessD = avgFitness - tempAvgFitness

    for i, box in enumerate(boxes):
        if box.won:
            if box.wonTime < lowestTime:
                lowestTime = box.wonTime
                lowestIndex = i
    lowestTimeD = lowestTime - tempLowestTime

    # Decide what to do with fitness
    for i, box in enumerate(boxes):
        n = int((box.fitness ** 2) * 100)

        if i == maxFitIndex:
            print(box.fitness)
            if successCount < 2:
                # Make squared to make sure, that the best one is better in the gene pool
                n = int((box.fitness ** 2) * 150)

        if i == lowestIndex and successCount > 1:
            # If this box is the first one, it will get more space in the gene pool
            n = int((box.fitness ** 2) * 500)

        for j in range(n):
            genePool.append(boxes[i])

    # IF we have more, than 50% of successful boxes, finish the game or change the level
    if successCount >= len(boxes) // 2:
        levelCount += 1
        # make the new one generation
        make_new_generation()
    else:
        # If we don`t have enough successful boxes, create a child box using a crossover
        create_a_child_using_crossover()
        generationCount += 1

    # Update info
    frameCount = 0
    aliveBoxCount = boxCount
    finished = False


if levelCount == 1:
    walls = [
        Obstacle(100, 100, 20, 320),
        Obstacle(950, 100, 20, 320),
        Obstacle(550, 200, 20, 100),
        Obstacle(550, 520, 20, 200),
        Obstacle(250, 250, 20, 500)
    ]

""" Here you can add more levels"""


def print_the_last_generation():
    # Change a color because of success
    if successCountD > 0:
        ShowText("+" + str(successCountD), 1200, 610, 25, pg.Color("green"))
    else:
        ShowText("-" + str(-successCountD), 1200, 610, 25, pg.Color("red"))

    ShowText("Avg. Fitness:            " + str(round(avgFitness, 3)), 900, 630, 25)

    if avgFitnessD > 0:
        ShowText("+" + str(round(avgFitnessD, 3)), 1200, 630, 25, pg.Color("green"))
    else:
        ShowText("-" + str(round(-avgFitnessD, 3)), 1200, 630, 25, pg.Color("red"))

    ShowText("Record Time :           " + str(lowestTime), 900, 650, 25)
    if lowestTimeD > 0:
        ShowText("+" + str(lowestTimeD), 1200, 650, 25, pg.Color("red"))
    else:
        ShowText("-" + str(-lowestTimeD), 1200, 650, 25, pg.Color("green"))


def main_info():
    # Draw main info about a game
    ShowText("Possible moves: " + str(frameCount) + " / " + str(moveLimit), 900, 30)
    ShowText("Generation: " + str(generationCount), 1020, 80)
    ShowText("Alive Boxes: " + str(aliveBoxCount), 1020, 110, 30)

    ShowText("Last Generation:", 900, 550, 45)
    ShowText("Total Boxes:             " + str(len(boxes)), 900, 590, 25)
    ShowText("Successful Boxes:   " + str(successCount), 900, 610, 25)


# A window of a game
while True:
    # Speed
    clock.tick(FPS)

    for event in pg.event.get():
        # Exit of program
        if event.type == pg.QUIT:
            pg.quit()
            sys.exit()

    # Fill background
    SC.fill((0, 0, 0))

    # Draw walls
    for wall in walls:
        wall.draw()

    # Draw box, check for collision, update
    for box in boxes:
        box.draw()
        if box.alive:
            box.CheckCollision(walls)
            box.make_update()

    # Draw main info
    main_info()
    print_the_last_generation()

    if levelCount == 2:
        ShowText("THE END", SW / 2, SH / 2, 120, (255, 255, 255))

    # Draw final point
    pg.draw.circle(SC, pg.Color("blue"), (int(finish.x), int(finish.y)), 20)

    pg.display.update()  # Update the display of the screen every frame.

    # Finishing of generation
    if (frameCount >= moveLimit - 1 and levelCount < 3) or aliveBoxCount <= 0:
        frameCount = moveLimit - 1
        finished = True
    else:
        frameCount += 1

    if levelCount == 2:
        print("THE END")
        pg.time.wait(1000)
        pg.quit()
        sys.exit()

    # Is it the end of generation?
    if finished:
        FinishGeneration()
