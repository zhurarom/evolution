# Smart Boxes(2020)

OSNOVA
1. Zadání úlohy.
	Implementation of genetic algorithm in a game „NoWayBack“.
	The main idea of a game is to teach boxes how to get to the blue point through all obstacles.
	This task was created to show practical usage of gentic algorithm in a game.

2. Název použité metody/ algoritmu.
	Genetic algorithm

3. Stručný rozbor úlohy (podstatné informace k řešení).
	Each generation of boxes has 1000 participantes (at the beginning each boxe has random 2d 	vector of movement, but then because of genetic algo, we coordinate movements). Each box 	is controlled by DNA (each box has own brain – genetic algorithm). With iterations the best 	boxes get more „place“ in gene pool. Then we selecte new boxes from gene pool and 	continue this action until the level is complete(we will get at least 50% of generation in a 	blue point).

4. Reference - odkaz na literaturu/ zdroj, ze kterého čerpáte.
	The main inspiration was gotten from youtube chanel:
	 https://www.youtube.com/watch?v=9zfeTw-uFCw&list=PLRqwX-V7Uu6bJM3VgzjNV5YxVxUwzALHV
