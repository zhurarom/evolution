from TCP_server import *


if __name__ == "__main__":
    server = TCP_server()
    host = ('127.0.0.2', 9090)
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    print("Running at ", host[0], host[1])

    socket.bind(host)
    socket.listen(1)

    threads = []

    try:
        server.connection(socket, threads)
    except OSError:     # Exception which is connected to OS error
        server.connection(('127.0.0.3', 8888), threads)
