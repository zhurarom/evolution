import threading
from Client import *


class myThread(threading.Thread):
    def __init__(self, connection, client_address):
        threading.Thread.__init__(self)
        self.connection = connection
        self.client_address = client_address

    def run(self):
        handler = Client(self.connection)
        handler.choose_action()


class TCP_server:
    def connection(self, socket, threads):
        while True:
            connection, address = socket.accept()
            connection.settimeout(1)
            thread = myThread(connection, address)
            threads += [thread]
            thread.start()
            print("New Thread started.")
