import socket
import re
import threading


class LogicError(Exception):
    pass


class SyntaxError(Exception):
    pass


class MessageFound(Exception):
    pass


KEY_SERVER = 54621
KEY_CLIENT = 45328
BUFFER_SIZE = 1024

SERVER_COMMANDS = {
    'MOVE': bytes('102 MOVE\a\b', 'ascii'),
    'TURN_LEFT': bytes('103 TURN LEFT\a\b', 'ascii'),
    'TURN_RIGHT': bytes('104 TURN RIGHT\a\b', 'ascii'),
    'GET_MESSAGE': bytes('105 GET MESSAGE\a\b', 'ascii'),
    'LOGOUT': bytes('106 LOGOUT\a\b', 'ascii'),
    'OK': bytes('200 OK\a\b', 'ascii'),
    'LOGIN_FAILED': bytes('300 LOGIN FAILED\a\b', 'ascii'),
    'SYNTAX_ERROR': bytes('301 SYNTAX ERROR\a\b', 'ascii'),
    'LOGIC_ERROR': bytes('302 LOGIC ERROR\a\b', 'ascii')
}

stages = ['CLIENT_USERNAME', 'CLIENT_OK', 'LOCATION', 'CORNER', 'CLIENT_MESSAGE']
stage_len = {'CLIENT_USERNAME': 12, 'CLIENT_OK': 12, 'LOCATION': 12, 'CORNER': 12, 'CLIENT_MESSAGE': 100}
DIRECTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT']


def find_a_hash(username):
    username = username[:-2]
    ascii_sum = 0
    for i in range(0, len(username), 1):
        ascii_sum += ord(username[i])
    hash = (ascii_sum * 1000) % 65536
    return hash


class Client:

    def __init__(self, connection):
        self.hash = 0
        self.action = 0
        self.direction_of_robot = 0

        self.connection = connection
        self.visited = []
        self.actual_stage = 0
        self.message = ''

        self.battery = False
        self.x = None
        self.y = None

        self.desired_x = -2
        self.desired_y = -2

    def move_with_separator(self):
        sep = self.message.find('\a\b')
        if sep != -1:
            message = self.message[:sep + 2]
            self.message = self.message[sep + 2:]
            return message
        else:
            return ''

    def read_to_buffer(self):
        if '\a\b' not in self.message:
            while True:
                buffer = self.connection.recv(BUFFER_SIZE).decode('ascii')
                # client not responding
                if not buffer:
                    break
                self.message += buffer

                # we should load min one message
                if '\a\b' in buffer or '\a\b' in self.message:
                    break

                # check maximum len of message
                if len(buffer) >= stage_len[stages[self.action]] and '\a\b' not in buffer:
                    raise SyntaxError

    def check_for_full_power(self, message):
        if message == "FULL POWER\a\b":
            self.battery = False
            self.connection.settimeout(1)
            return True
        else:
            return False

    def check_for_recharging(self, message):
        if message == 'RECHARGING\a\b':
            self.battery = True
            self.connection.settimeout(5)
            self.read_to_buffer()
            return True
        return False

    """
     It will test if it recieved 'FULL POWER\a\b' message or 'RECHARGING\a\b'   
     """

    def test_recharge(self, message):
        # Check for Full Power
        if self.battery:
            if self.check_for_full_power(message):
                return True
            else:
                raise LogicError

        # Check for Recharging
        if self.check_for_recharging(message):
            message = self.move_with_separator()
            return self.test_recharge(message)

        return False

    def get_message(self):
        self.read_to_buffer()
        # Here we take message from our buffer
        message = self.move_with_separator()
        if self.test_recharge(message):
            self.read_to_buffer()
            message = self.move_with_separator()

        print("MESSAGE: ", message)
        return message

    """
        Here we check, that our message is received like '<int> <int> OK\a\b'
    """

    def check_ok(self, message):
        if message[:2] != 'OK' or message[-2:] != '\a\b':
            raise SyntaxError

        """
            ^ - Matches the beginning of a line if the multiline flag (m) is enabled. This matches a position,  \
                not a character
            '\d' - any digit character (0-9)
                + - match 1 or more the preceding token
            $ - the end of the string 

            We need this to be sure, that we have <int>
        """
        pattern = re.compile('^-?\d+ -?\d+$')
        if pattern.match(message[3:-2]) is None:
            raise SyntaxError

    def horizontal_move(self, pos_first_x, pos_first_y, pos_second_x, pos_second_y):
        # horizontal move
        if pos_first_y == pos_second_y:
            if pos_first_x > pos_second_x:
                self.direction_of_robot = 3  # LEFT
            if pos_first_x < pos_second_x:
                self.direction_of_robot = 1  # RIGHT

    def vertical_move(self, pos_first_x, pos_first_y, pos_second_x, pos_second_y):
        # vertical move
        if pos_first_x == pos_second_x:
            if pos_first_y > pos_second_y:
                self.direction_of_robot = 2  # DOWN
            if pos_first_y < pos_second_y:
                self.direction_of_robot = 0  # UP

    """
        Calculates which direction is the robot facing
        0 -> 'UP';
        1 -> 'RIGHT';
        2 -> 'DOWN';
        3 -> 'LEFT'
    """

    def direction(self, pos_of_robot_1, pos_of_robot_2):
        pos_first_x = pos_of_robot_1[0]
        pos_first_y = pos_of_robot_1[1]

        pos_second_x = pos_of_robot_2[0]
        pos_second_y = pos_of_robot_2[1]

        self.horizontal_move(pos_first_x, pos_first_y, pos_second_x, pos_second_y)
        self.vertical_move(pos_first_x, pos_first_y, pos_second_x, pos_second_y)

    """
        Move our robot x2 ahead for activation
    """

    def move_x2_activation(self):
        position_first = ''
        position_second = ''

        # SERVER_MOVE FIRST and check for OK
        for init_counter in range(2):
            self.connection.send(SERVER_COMMANDS['MOVE'])
            if init_counter == 0:
                position_first = self.get_message()
                self.check_ok(position_first)
            else:
                position_second = self.get_message()
                self.check_ok(position_second)

        return position_first, position_second

    """
        Here is start of any moves. Firstly, we should move our robot x2 ahead for activation
    """

    def start_location(self):

        position_first, position_second = self.move_x2_activation()

        # Here we take coordinates for moving
        # 0 -> 'UP'; 1 -> 'RIGHT'; 2 -> 'DOWN'; 3 -> 'LEFT'
        pos_of_robot_1 = [int(x) for x in position_first[3:-2].split(' ')]
        print("pos_of_robot_1: ", pos_of_robot_1)
        pos_of_robot_2 = [int(x) for x in position_second[3:-2].split(' ')]
        print("pos_of_robot_2: ", pos_of_robot_2)

        # Which kind of move we have: horizontal / vertical
        self.direction(pos_of_robot_1, pos_of_robot_2)

        # Remember positions of a robot
        self.x = pos_of_robot_2[0]
        self.y = pos_of_robot_2[1]

        print("Robot direction: ", DIRECTIONS[self.direction_of_robot])

    """
    Rotates robot to goal direction
    """

    def rotate(self, direction):
        while True:
            if DIRECTIONS[self.direction_of_robot] != direction:
                self.connection.sendall(SERVER_COMMANDS['TURN_RIGHT'])
                message = self.get_message()
                self.check_ok(message)
                self.direction_of_robot = (self.direction_of_robot + 1) % 4
            else:
                break

    """
        Did we found a message?
    """

    def found_message(self, message):
        if message[:-2] != '':
            self.message = message[:-2]
            return True
        return False

    """
        Sends robot ahead if the robot is in middle square array, also checks for secret_message
    """

    def pick_up_message(self, message):
        if [self.x, self.y] not in self.visited and -2 <= self.x <= 2 and -2 <= self.y <= 2:
            # SERVER_PICK_UP
            self.connection.sendall(SERVER_COMMANDS['GET_MESSAGE'])
            message = self.get_message()

            if self.found_message(message):
                raise MessageFound

            self.visited.append([self.x, self.y])

    def make_move(self):
        # SERVER MOVE
        self.connection.sendall(SERVER_COMMANDS['MOVE'])
        message = self.get_message()
        self.check_ok(message)

        position_of_robot = [int(x) for x in message[3:-2].split(' ')]
        self.x = position_of_robot[0]
        self.y = position_of_robot[1]

        # SERVER_PICK_UP
        self.pick_up_message(self.message)

        print("CURRENT ", self.x, self.y)

    """
    Make navigation to [-2, -2] -> left bottom corner of a middle square array
    """

    ###################################### move to goal ##############################################################

    def move_to_goal(self):
        print("Robot location: ", self.x, ",", self.y)
        print("Goal location: ", self.desired_x, ",", self.desired_y)

        if self.y != self.desired_y:
            if self.y < self.desired_y:
                self.rotate('UP')
            else:
                self.rotate('DOWN')
            while self.y != self.desired_y:
                self.make_move()

        if self.x != self.desired_x:
            if self.x < self.desired_x:
                self.rotate('RIGHT')
            else:
                self.rotate('LEFT')
            while self.x != self.desired_x:
                self.make_move()

        return

    """
        Client confirmation in authentication 
    """

    def client_confirmation(self, response, to_check):
        # CLIENT_CONFIRMATION
        print("CLIENT_CONFIRMATION")
        if response[:-2] == str(to_check):
            # SERVER_OK
            print("SERVER_OK")
            self.connection.sendall(SERVER_COMMANDS['OK'])
            return True
        else:
            # SERVER_LOGIN_FAILED
            print("SERVER_LOGIN_FAILED")
            self.connection.sendall(SERVER_COMMANDS['LOGIN_FAILED'])
            return False

    """
        Server confirmation in authentication 
    """

    def server_confirmation(self, to_send):
        # SERVER_CONFIRM
        print("SERVER_CONFIRM")
        self.connection.sendall((str(to_send) + '\a\b').encode('ascii'))

    """
        Function for make_authentication, the first part: key_server
    """

    def key_server(self, username):
        if len(username) > stage_len[stages[self.actual_stage]] and username[-2:] == '\a\b':
            raise SyntaxError

        self.hash = find_a_hash(username)
        to_send = (self.hash + KEY_SERVER) % 65536
        return to_send

    """
        Function for make_authentication, the second part: key_client
    """

    def key_client(self, username):
        response = self.get_message()
        if len(username) > stage_len[stages[self.actual_stage]] and username[-2:] == '\a\b':
            raise SyntaxError

        if not response[:-2].isdigit() or int(response[:-2]) > 99999:
            raise SyntaxError

        to_check = (self.hash + KEY_CLIENT) % 65536

        return response, to_check

    """
    Method used for client authentication
    """

    def make_authentication(self):
        username = self.get_message()
        to_send = self.key_server(username)

        self.server_confirmation(to_send)

        response, to_check = self.key_client(username)
        client_confirmation = self.client_confirmation(response, to_check)

        if client_confirmation:
            return True
        return False

    #####################################################################################################

    """
        Here we will check all middle array indexes
    """

    def get_secret_message(self):
        SNAKE_DIRECTIONS = ['RIGHT', 'UP', 'LEFT', 'DOWN']
        COORDINATES_X = [2, -2, 1, -1]
        COORDINATES_Y = [2, -1, 1, 0]

        helper_x = 0
        for _ in range(2):
            for counter, i in enumerate(SNAKE_DIRECTIONS):
                if counter % 2 == 0:
                    self.rotate(i)
                    while self.x != COORDINATES_X[counter - helper_x]:
                        self.make_move()
                    helper_x += 1
                else:
                    self.rotate(i)
                    while self.y != COORDINATES_Y[counter - helper_x]:
                        self.make_move()

        self.rotate('RIGHT')
        while self.x != 0:
            self.make_move()

    """
        The main loop, which is responsible for CLIENT-SERVER connection
    """

    def choose_action(self):
        try:
            while True:
                if self.action == 0:
                    if not self.make_authentication():
                        break
                    print("make_authentication")
                    self.action += 1

                if self.action == 1:
                    self.start_location()
                    print("locate")
                    self.action += 1

                if self.action == 2:
                    self.move_to_goal()
                    print("desired")
                    self.action += 1

                if self.action == 3:
                    self.get_secret_message()
                    print("GET_MESSAGE")

        except socket.timeout:
            print("timeout")

        except LogicError:
            self.connection.sendall(SERVER_COMMANDS['LOGIC_ERROR'])
            print("logic error")

        except SyntaxError:
            self.connection.sendall(SERVER_COMMANDS['SYNTAX_ERROR'])
            print("syntax error")

        except MessageFound:
            self.connection.sendall(SERVER_COMMANDS['LOGOUT'])
            print("MESSAGE FOUND")

        finally:
            self.connection.close()
            print("Server closed.")
            return

from TCP_server import *


if __name__ == "__main__":
    server = TCP_server()
    host = ('127.0.0.2', 9090)
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    print("Running at ", host[0], host[1])

    socket.bind(host)
    socket.listen(1)

    threads = []

    try:
        server.connection(socket, threads)
    except OSError:     # Exception which is connected to OS error
        server.connection(('127.0.0.3', 8888), threads)

import threading
from Client import *


class myThread(threading.Thread):
    def __init__(self, connection, client_address):
        threading.Thread.__init__(self)
        self.connection = connection
        self.client_address = client_address

    def run(self):
        handler = Client(self.connection)
        handler.choose_action()


class TCP_server:
    def connection(self, socket, threads):
        while True:
            connection, address = socket.accept()
            connection.settimeout(1)
            thread = myThread(connection, address)
            threads += [thread]
            thread.start()
            print("New Thread started.")
