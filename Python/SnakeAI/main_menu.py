
from Classic_snake.classic_snake import *
from Genetic_snake.genetic_snake import *

# Game Initialization
pygame.init()

# Game Resolution
screen_width = 880
screen_height = 880
screen = pygame.display.set_mode((screen_width, screen_height))

# # Load background
# background = pygame.image.load('./images.jpeg')
# background = pygame.transform.scale(background, (500, screen_height))
#


# Text Renderer
def text_format(message, textSize, textColor):
    pygame.font.init()
    newFont = pygame.font.SysFont('Comic Sans MS', textSize)
    newText = newFont.render(message, 0, textColor)

    return newText


# Colors
white = (255, 255, 255)
black = (0, 0, 0)
gray = (50, 50, 50)
red = (255, 0, 0)
green = (56, 205, 12)
blue = (0, 0, 255)
yellow = (255, 255, 0)

# Game Framerate
clock = pygame.time.Clock()
FPS = 30

# display_surface = pygame.display.set_mode((880, 880))


# Main Menu of SnakeAI
def main_menu():
    menu = True
    selected = "Classic snake"

    while menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    selected = "Classic snake"
                elif event.key == pygame.K_DOWN and selected != "Genetic snake":
                    selected = "Genetic snake"
                elif event.key == pygame.K_DOWN:
                    selected = "quit"

                if event.key == pygame.K_RETURN:
                    if selected == "Classic snake":
                        print("Classic snake")
                        classic_snake()
                    if selected == "Genetic snake":
                        print("Genetic snake")
                        genetic_snake()
                    if selected == "quit":
                        pygame.quit()
                        quit()

        # Main Menu UI
        screen.fill(green)
        # display_surface.blit(background, (0, 0))

        title = text_format("SNAKE AI", 90, red)

        if selected == "Classic snake":
            classic_snake_flag = text_format("Classic snake", 75, white)
        else:
            classic_snake_flag = text_format("Classic snake", 75, black)

        if selected == "Genetic snake":
            genetic_snake_flag = text_format("Genetic snake", 75, white)
        else:
            genetic_snake_flag = text_format("Genetic snake", 75, black)

        if selected == "quit":
            text_quit = text_format("QUIT", 75, white)
        else:
            text_quit = text_format("QUIT", 75, black)

        title_rect = title.get_rect()
        classic_snake_rect = classic_snake_flag.get_rect()
        genetic_snake_rect = genetic_snake_flag.get_rect()
        quit_rect = text_quit.get_rect()

        # Main Menu Text
        screen.blit(title, (screen_width / 2 - (title_rect[2] / 2), 80))
        screen.blit(classic_snake_flag, (screen_width / 2 - (classic_snake_rect[2] / 2), 300))
        screen.blit(genetic_snake_flag, (screen_width / 2 - (genetic_snake_rect[2] / 2), 360))
        screen.blit(text_quit, (screen_width / 2 - (quit_rect[2] / 2), 420))
        pygame.display.update()
        clock.tick(FPS)
        pygame.display.set_caption("SNAKE AI")


if __name__ == '__main__':
    # Initialize the Game
    main_menu()
    pygame.quit()
    quit()
