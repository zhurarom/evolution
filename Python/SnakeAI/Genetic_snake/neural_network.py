import numpy as np

# Configurations of neural network
N_INPUT_NEURONS = 7
N_LAYER_1_NEURONS = 8
N_LAYER_2_NEURONS = 16
N_OUTPUT_NEURONS = 3  # ahead, left or right

WEIGHTS_ROW_SIZE = N_INPUT_NEURONS * N_LAYER_1_NEURONS + \
                   N_LAYER_1_NEURONS * N_LAYER_2_NEURONS + \
                   N_LAYER_2_NEURONS * N_OUTPUT_NEURONS

# shapes of matrices
w_input_lay_1_shape = (N_LAYER_1_NEURONS, N_INPUT_NEURONS)
w_lay_1_lay_2_shape = (N_LAYER_2_NEURONS, N_LAYER_1_NEURONS)
w_lay_2_output_shape = (N_OUTPUT_NEURONS, N_LAYER_2_NEURONS)


# Initialization of random weights
def init_random_weights(shape):
    return np.random.choice(np.arange(-1, 1, step=0.01), size=shape, replace=True)


# Class which represent a neural network
class NeuralNetwork:
    def __init__(self, weights=None):
        if weights is None:
            # Make random init of weights for each layer(we have 3 hidden layers)
            self.w_input_lay1 = init_random_weights(w_input_lay_1_shape)
            self.w_lay1_lay2 = init_random_weights(w_lay_1_lay_2_shape)
            self.w_lay2_output = init_random_weights(w_lay_2_output_shape)
        else:
            self.input_weights(weights)

    # weights are stacked in columns for crossover and mutation, we need to stack them back into the shape of
    # matrices of weights between the layers
    def input_weights(self, weights):
        self.w_input_lay1 = np.reshape(weights[: N_INPUT_NEURONS * N_LAYER_1_NEURONS], newshape=w_input_lay_1_shape)
        self.w_lay1_lay2 = np.reshape(weights[
                                      N_INPUT_NEURONS * N_LAYER_1_NEURONS: N_INPUT_NEURONS * N_LAYER_1_NEURONS + N_LAYER_1_NEURONS * N_LAYER_2_NEURONS],
                                      newshape=w_lay_1_lay_2_shape)
        self.w_lay2_output = np.reshape(
            weights[N_INPUT_NEURONS * N_LAYER_1_NEURONS + N_LAYER_1_NEURONS * N_LAYER_2_NEURONS:],
            newshape=w_lay_2_output_shape)

    def output_weights(self):
        return np.vstack((np.reshape(self.w_input_lay1, newshape=(N_INPUT_NEURONS * N_LAYER_1_NEURONS, 1)),
                          np.reshape(self.w_lay1_lay2, newshape=(N_LAYER_1_NEURONS * N_LAYER_2_NEURONS, 1)),
                          np.reshape(self.w_lay2_output, newshape=(N_LAYER_2_NEURONS * N_OUTPUT_NEURONS, 1)))).T

    # get the predicted outputs for the given input
    def forward_propagation(self, input):
        Z1 = np.matmul(self.w_input_lay1, input.T)
        A1 = np.tanh(Z1)
        Z2 = np.matmul(self.w_lay1_lay2, A1)
        A2 = np.tanh(Z2)
        Z3 = np.matmul(self.w_lay2_output, A2)
        A3 = self.softmax(Z3)
        return A3

    # Make prediction of a snake direction: -1 - Left
    #                                        0 - Ahead
    #                                        1 - Right
    def predict_snake_direction(self, input):
        out = self.forward_propagation(input.reshape(-1, 7))
        return np.argmax(out) - 1

    # Activation function
    def softmax(self, z):
        s = np.exp(z.T) / np.sum(np.exp(z.T), axis=1).reshape(-1, 1)
        return s
