import numpy as np
import random

from Genetic_snake.game_simulation import SnakeGame
from Genetic_snake.neural_network import WEIGHTS_ROW_SIZE, NeuralNetwork

# Configuration of genetic algorithm
POPULATION_SIZE = 120
N_PARENTS = 14
MUTATION_PROB = 0.08
ITERATIONS = 450

best_score = 0
best_nn = None


# Calculate single fitness
def single_nn_fitness(nn):
    return SnakeGame(nn).play_game()


# Calculate fitness for a population
def fitness(population):
    pop_fit_tuple = []
    for individual in population:
        fitness = single_nn_fitness(individual)
        pop_fit_tuple.append((individual, fitness))
    return pop_fit_tuple


# Make selection
def selection(pop_fit_tuple, old_best_score):
    pop_fit_tuple = sorted(pop_fit_tuple, key=lambda x: x[1], reverse=True)
    best_nn = None
    best_score = -100
    if pop_fit_tuple[0][1] > old_best_score:
        best_score = pop_fit_tuple[0][1]
        best_nn = pop_fit_tuple[0][0]
    return [nn for nn, fit in pop_fit_tuple[:N_PARENTS]], best_score, best_nn


# We always have the best fit agent in the population
def crossover(parents, best_nn):
    if best_nn is not None:
        offsprings = [best_nn]
    else:
        offsprings = []

    for _ in range(POPULATION_SIZE - 1):
        # select two random parents
        p1_idx = random.randint(0, N_PARENTS - 1)
        p2_idx = random.randint(0, N_PARENTS - 1)

        p1 = parents[p1_idx].output_weights()
        p2 = parents[p2_idx].output_weights()

        # perform 1-point crossover
        fold_point_1 = random.randint(3, int(WEIGHTS_ROW_SIZE / 2))

        offspring = np.vstack((p1[:fold_point_1], p2[fold_point_1:]))

        # Mutate offspring and create the network again
        offsprings.append(NeuralNetwork(weights=mutation(offspring)[0]))

    return offsprings


# Make mutation
def mutation(offspring):
    for i in range(WEIGHTS_ROW_SIZE):
        if random.random() < MUTATION_PROB:
            offspring[0, i] = random.uniform(-1.0, 1.0)
    return offspring
