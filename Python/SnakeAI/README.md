# SnakeAI(2020)

OSNOVA
1. Zadání semestrální práce.
	Make implementation of game snake in two modes. The first one is a classic variant of a
	snake and the second one is a variant with neural network accompanied by genetic algorithm for
	adjustments of weights.
2. Stručný rozbor, analýza problému/ zadání.
	For realization of the second mode we should understand how to teach our snake „to be
	smarter“, so we need to understand in which way we should coordinate weights of neural network.
	To solve this task, we use fitness function (system of punishment and promotion) and genetic
	algorithm.
3. Výběr metody.
	For realization of the second part of a task, I chose neural network and genetic algorithm for
	adjustments of weights. In the first generation we have random weights, but in the next generations
	genetic algorithm make weights better, because of fitness function, selection, mutation and
	crossover. Also in each generation we have the best fit agent in the population.
4. Popis aplikace metody na daný problém.
	Configuration for neural network:
	• 7 input neurons,
	• 8 neurons for the first hidden layer
	• 16 neurons for the secon hidden layer
	• 3 neurons for the output layer(direction of movements: -1 – left, 0 – ahead, 1 – right)
	Configuration for genetic algorithm:
	• size of population is 120
	• number of parents is 14
	• mutation probability is 0.08
	• count of all generations/iterations is 450.
	System of punishment and promotion (fitness function):
	• -50 – for crashing yourself
	• +10 – for eaten food
	• +1 – move toward the food
	• -1.5 – move NOT toward the food
5. Implementace.
	For realization of task, I chose two main libraries: pygame(visualization) and
	numpy(especially for NN and math). I created two different modes, which is not intersected and
	main_menu for allowing to choose one of the modes. To start a program, you should run a
	command: python main_menu.py
6. Reference - odkaz na literaturu/ zdroj, ze kterého čerpáte.
	https://pythonawesome.com/train-a-neural-network-to-play-snake-using-a-genetic-algorithm/
