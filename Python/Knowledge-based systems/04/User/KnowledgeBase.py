from typing import List
from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import MapProxy, GameObjectProxy, GameUncertaintyProxy
from ExpertSystem.Business.UserFramework import IKnowledgeBase
from ExpertSystem.Structure.RuleBase import Fact
from OrodaelTurrim.Structure.Enums import TerrainType, AttributeType, EffectType, GameRole, GameObjectType
from OrodaelTurrim.Structure.Position import OffsetPosition, CubicPosition, AxialPosition


class KnowledgeBase(IKnowledgeBase):
    """
    Class for defining known facts based on Proxy information. You can transform here any information from
    proxy to better format of Facts. Important is method `create_knowledge_base()`. Return value of this method
    will be passed to `Inference.interfere`. It is recommended to use Fact class but you can use another type.

    |
    |
    | Class provides attributes:

    - **map_proxy [MapProxy]** - Proxy for access to map information
    - **game_object_proxy [GameObjectProxy]** - Proxy for access to all game object information
    - **uncertainty_proxy [UncertaintyProxy]** - Proxy for access to all uncertainty information in game
    - **player [PlayerTag]** - class that serve as instance of user player for identification in proxy methods

    """
    map_proxy: MapProxy
    game_object_proxy: GameObjectProxy
    game_uncertainty_proxy: GameUncertaintyProxy
    player: PlayerTag


    def __init__(self, map_proxy: MapProxy, game_object_proxy: GameObjectProxy,
                 game_uncertainty_proxy: GameUncertaintyProxy, player: PlayerTag):
        """
        You can add some code to __init__ function, but don't change the signature. You cannot initialize
        KnowledgeBase class manually so, it is make no sense to change signature.
        """
        super().__init__(map_proxy, game_object_proxy, game_uncertainty_proxy, player)


    def create_knowledge_base(self) -> List[Fact]:
        """
        Method for create user knowledge base. You can also have other class methods, but entry point must be this
        function. Don't change the signature of the method, you can change return value, but it is not recommended.

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!  TODO: Write implementation of your knowledge base definition HERE   !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """

        facts = []

        # Add bool fact
        if not self.map_proxy.player_have_base(self.player):
            facts.append(Fact('player_dont_have_base'))

        if self.map_proxy.player_have_base(self.player):
            facts.append(Fact('player_have_base'))

        #For fuzzy logic
        facts.append(Fact("amount_of_defenders", lambda amount: self.amount_of_defenders(amount)))
        facts.append(Fact("health", lambda how: self.health(how)))

        #For uncertainty
        facts.append(Fact("knights_required", lambda: self.knights_required()))

        # Add fact with data holder
        # We can use there eval function same as data function
        # because if first_free_tile return None, bool value of None is False, otherwise bool value is True
        # You can use different functions for eval and data
        
        facts.append(Fact('is_not_occupied', lambda x, y: not self.map_proxy.is_position_occupied(OffsetPosition(int(x), int(y)))))

        
        facts.append(Fact('visible_free_tile', eval_function=self.visible_free_tile, data=self.visible_free_tile))
        facts.append(Fact("money", lambda: self.game_object_proxy.get_resources(self.player)))

        user_resources = self.game_object_proxy.get_resources(self.player)

        facts.append(Fact("money_EQUAL", lambda x: user_resources == int(x)))
        facts.append(Fact("money_GREATER_EQUAL", lambda x: user_resources >= int(x)))
        facts.append(Fact("money_LESS", lambda x: user_resources < int(x)))
        return facts

    def visible_free_tile(self):
        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()

        #
        next_wave_direction = self.next_wave_direction()
        #

        for min in tiles:
            occupied = self.map_proxy.is_position_occupied(min)
            if not occupied and min not in border_tiles:
                break

        for position in tiles:
            occupied = self.map_proxy.is_position_occupied(position)
            position_sum = position.q**4 + position.r**4
            min_sum = min.q**4 + min.r**4

            if not occupied and position not in border_tiles:
                if position_sum < min_sum:
                    min = position
                elif position_sum == min_sum and self.direction(position.q, position.r) != next_wave_direction:
                    min = position
                elif position_sum == min_sum and self.map_proxy.get_terrain_type(position) == TerrainType.from_string("MOUNTAIN"):
                    min = position

        return min

    def possible_spawn_tiles(self):
        """ Get list of possible tiles, where enemy spawn a unit """
        spawn_info = self.game_uncertainty_proxy.spawn_information()

        next_round = spawn_info[0]

        possible_tiles = set()
        for unit in next_round:
            possible_tiles.update([x.position for x in unit.positions])

        return possible_tiles

    def direction(self, q, r):
        if q >= 0 and r >= 0:
            return "rb" 
        elif q >= 0 and r < 0:
            return "rt" 
        elif q < 0 and r >= 0:
            return "lb" 
        else:
            return "lt" 

    def next_wave_direction(self):
        spawn_info = self.game_uncertainty_proxy.spawn_information()
        next_round = spawn_info[0]

        q = 0
        r = 0

        for unit in next_round:
            for pos in unit.positions:
                q += pos.position.q
                r += pos.position.r

        direction = self.direction(q,r)

        return direction

    def knights_required(self):
        knights = 0
        druids = 0
        tiles = self.map_proxy.get_player_visible_tiles()
        for tile in tiles:
            if self.game_object_proxy.get_object_type(tile) == GameObjectType.KNIGHT:
                knights += 1
            if self.game_object_proxy.get_object_type(tile) == GameObjectType.DRUID:
                druids += 1

        if ( druids + 1 ) / ( knights + 1 ) >= 0.75:
            return True
        return False

    def amount_of_defenders(self, amount):
        units = 0
        tiles = self.map_proxy.get_player_visible_tiles()
        for tile in tiles:
            if self.game_object_proxy.get_role(tile) == GameRole.DEFENDER:
                units += 1

        if amount == "few":
            return self.fuzzificate(0, 0, 4, 6, float(units))
        elif amount == "medium":
            return self.fuzzificate(4, 6, 11, 13, float(units))
        elif amount == "a_lot":
            return self.fuzzificate(12, 13, 25, 25, float(units))
        else:
            return 0.0

    def health(self, how) -> float:
        base_hp = self.game_object_proxy.get_current_hit_points(OffsetPosition(0, 0))

        if how == "bad":
            return self.fuzzificate(0, 0, 190, 220, float(base_hp))
        elif how == "ok":
            return self.fuzzificate(190, 220, 290, 320, float(base_hp))
        elif how == "awesome":
            return self.fuzzificate(290, 320, 500, 500, float(base_hp))
        else:
            return 0.0


    def fuzzificate(self, a, b, c, d, x) -> float:
        if x < a:
            return 0.0
        elif a <= x and x <= b:
            if x:
                return (x - a) / (b - a)
            else:
                return 1.0
        elif b < x and x <= c:
            return 1.0
        elif c < x and x <= d:
            if x:
                return (d - x) / (d - c)
            else:
                return 1.0
        else: # d < x:
            return 0.0