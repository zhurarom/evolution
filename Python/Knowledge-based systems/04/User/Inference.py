from typing import List

from ExpertSystem.Business.UserFramework import IInference, ActionBaseCaller
from ExpertSystem.Structure.Enums import Operator
from ExpertSystem.Structure.Enums import LogicalOperator
from ExpertSystem.Structure.RuleBase import Rule, Fact, ExpressionNode, Expression

import random;


class Inference(IInference):
    """
    | User definition of the inference. You can define here you inference method (forward or backward).
      You can have here as many functions as you want, but you must implement interfere with same signature

    |
    | `def interfere(self, knowledge_base: List[Fact], rules: List[Rule], action_base: ActionBase):`
    |

    | Method `interfere` will be called each turn or manually with `Inference` button.
    | Class have no class parameters, you can use only inference parameters

    """
    knowledge_base: List[Fact]
    action_base: ActionBaseCaller


    def infere(self, knowledge_base: List[Fact], rules: List[Rule], action_base: ActionBaseCaller) -> None:
        """
        User defined inference

        :param knowledge_base: - list of Fact classes defined in  KnowledgeBase.create_knowledge_base()
        :param rules:  - list of rules trees defined in rules file.
        :param action_base: - instance of ActionBaseCaller for executing conclusions

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!    TODO: Write implementation of your inference mechanism definition HERE    !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        self.knowledge_base = knowledge_base
        self.action_base = action_base


        for rule in rules:
            condition = self.rule_evaluation(rule.condition)
            self.conclusion_evaluation(rule.conclusion, condition, knowledge_base)

    def rule_evaluation(self, root_node: ExpressionNode) -> float:

        if root_node.operator == LogicalOperator.AND:
            return min( self.rule_evaluation(root_node.left), self.rule_evaluation(root_node.right) )

        elif root_node.operator == LogicalOperator.OR:
            return max( self.rule_evaluation(root_node.left), self.rule_evaluation(root_node.right) )

        elif isinstance(root_node.value, Expression):
            try:
                return float(bool(self.knowledge_base[self.knowledge_base.index(root_node.value.name)](*root_node.value.args)))
            except ValueError:
                return 0.0
        else:
            return float(bool(root_node.value))


    def conclusion_evaluation(self, root_node: ExpressionNode, condition_value: float, knowledge_base: List[Fact]):

        if isinstance(root_node.value, Expression):
            if root_node.value.comparator == Operator.ASSIGN:
                if root_node.value.name == 'next_turn_is':
                    if root_node.value.value == 'easy':
                        value = self.defuzzificate(0, 0, 7, 10, condition_value)
                    elif root_node.value.value == 'medium':
                        value = self.defuzzificate(9, 11, 19, 21, condition_value)
                    elif root_node.value.value == 'hard':
                        value = self.defuzzificate(18, 20, 30, 30, condition_value)
                    else:
                        return

                    knowledge_base.append(Fact(root_node.value.name,
                        lambda name: self.next_turn_is(name, value)))

            elif self.action_base.has_method(root_node.value):
                if condition_value > 0.5:
                    self.action_base.call(root_node.value)


    def next_turn_is(self, name, value) -> float:

        if  name == 'easy':
            return self.fuzzificate(0, 0, 4, 6, value)
        elif name == 'medium':
            return self.fuzzificate(4, 6, 11, 13, value)
        elif name == 'hard':
            return self.fuzzificate(12, 13, 25, 25, value)
        else:
            return float(0)


    def fuzzificate(self, a, b, c, d, x) -> float:
        if x < a:
            return 0.0
        elif a <= x and x <= b:
            if x:
                return (x - a) / (b - a)
            else:
                return 1.0
        elif b < x and x <= c:
            return 1.0
        elif c < x and x <= d:
            if x:
                return (d - x) / (d - c)
            else:
                return 1.0
        else: 
            return 0.0


    def defuzzificate(self, a, b, c, d, x) -> float:
        if x == 0:
            return a

        summ_weighted = 0

        for n in range(a, d):
            summ_weighted += n * x
        summ = 0
        for n in range(a, d):
            summ += x

        return summ_weighted / summ