# Knowledge-based systems

Students will get acquainted with the so-called knowledge-based systems, which are systems that use artificial intelligence techniques in solving problems that require human decision-making, learning and drawing conclusions and actions. The course introduces students to the philosophy and architecture of knowledge systems to support decision making and planning. The course presupposes knowledge of set theory, basics of probability theory, artificial neural networks and evolutionary algorithms.
