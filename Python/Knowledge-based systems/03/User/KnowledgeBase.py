from typing import List
from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import MapProxy, GameObjectProxy, GameUncertaintyProxy
from ExpertSystem.Business.UserFramework import IKnowledgeBase
from ExpertSystem.Structure.RuleBase import Fact
from OrodaelTurrim.Structure.Enums import TerrainType, AttributeType, EffectType, GameRole
from OrodaelTurrim.Structure.Position import OffsetPosition, CubicPosition, AxialPosition

from OrodaelTurrim.Structure.Enums import GameObjectType


class KnowledgeBase(IKnowledgeBase):
    """
    Class for defining known facts based on Proxy information. You can transform here any information from
    proxy to better format of Facts. Important is method `create_knowledge_base()`. Return value of this method
    will be passed to `Inference.interfere`. It is recommended to use Fact class but you can use another type.

    |
    |
    | Class provides attributes:

    - **map_proxy [MapProxy]** - Proxy for access to map information
    - **game_object_proxy [GameObjectProxy]** - Proxy for access to all game object information
    - **uncertainty_proxy [UncertaintyProxy]** - Proxy for access to all uncertainty information in game
    - **player [PlayerTag]** - class that serve as instance of user player for identification in proxy methods

    """
    map_proxy: MapProxy
    game_object_proxy: GameObjectProxy
    game_uncertainty_proxy: GameUncertaintyProxy
    player: PlayerTag


    def __init__(self, map_proxy: MapProxy, game_object_proxy: GameObjectProxy,
                 game_uncertainty_proxy: GameUncertaintyProxy, player: PlayerTag):
        """
        You can add some code to __init__ function, but don't change the signature. You cannot initialize
        KnowledgeBase class manually so, it is make no sense to change signature.
        """
        super().__init__(map_proxy, game_object_proxy, game_uncertainty_proxy, player)


    def create_knowledge_base(self) -> List[Fact]:
        """
        Method for create user knowledge base. You can also have other class methods, but entry point must be this
        function. Don't change the signature of the method, you can change return value, but it is not recommended.

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!  TODO: Write implementation of your knowledge base definition HERE   !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """

        facts = []

        # Add bool fact
        if not self.map_proxy.player_have_base(self.player):
            facts.append(Fact('player_dont_have_base'))

        if self.map_proxy.player_have_base(self.player):
            facts.append(Fact('player_have_base'))

        druid = 0

        tiles = self.map_proxy.get_player_visible_tiles()
        for tile in tiles:
            if self.game_object_proxy.get_object_type(tile) == GameObjectType.DRUID:
                druid += 1

        if druid < 7:
            facts.append(Fact('not_enough_druids'))   
        
        if druid == 6:
            facts.append(Fact('enough_druids'))   



        facts.append(Fact("knights_requiered", lambda: self.knights_requiered() ))

        # Add fact with data holder
        # We can use there eval function same as data function
        # because if first_free_tile return None, bool value of None is False, otherwise bool value is True
        # You can use different functions for eval and data
        facts.append(Fact('free_tile', eval_function=self.first_free_tile, data=self.first_free_tile))

        facts.append(Fact('is_not_occupied', lambda x, y: not self.map_proxy.is_position_occupied(OffsetPosition(int(x), int(y)))))

        facts.append(Fact('visible_free_tile', eval_function=self.visible_free_tile, data=self.visible_free_tile))
        
        facts.append(Fact("position_is_visible", lambda x, y: OffsetPosition(int(x), int(y)) in self.map_proxy.get_player_visible_tiles()))
        
        facts.append(Fact("position_is_accessible", lambda x, y: not self.map_proxy.is_position_occupied(OffsetPosition(int(x), int(y)))))
                
        facts.append(Fact("money", lambda x: int(x) <= self.game_object_proxy.get_resources(self.player)))

        user_resources = self.game_object_proxy.get_resources(self.player)
        facts.append(Fact("moneyEQUAL", lambda x: user_resources == int(x)))
        facts.append(Fact("moneyMORE_EQUAL", lambda x: user_resources >= int(x)))
        facts.append(Fact("moneyLESS", lambda x: user_resources < int(x)))


        # target_position = OffsetPosition(0, 0)

        # Add fact with data holder
        # We can use there eval function same as data function
        # because if first_free_tile return None, bool value of None is False, otherwise bool value is True
        # You can use different functions for eval and data
        #facts.append(Fact('free_tile', eval_function=self.first_free_tile, data=self.first_free_tile))        
        
        # facts.append(Fact('free_tile', eval_function = self.first_free_tile, data = self.first_free_tile))

        # facts.append(Fact('visible_free_tile', eval_function = self.visible_free_tile, data = self.visible_free_tile))

        # facts.append(Fact('position_from_base', lambda: self.game_object_proxy.get_visible_tiles(self.map_proxy.get_bases_positions())))


        # Add numerical fact
        #facts.append(Fact("money", lambda: self.game_object_proxy.get_resources(self.player)))
        # facts.append(Fact("money", lambda x: int(x) <= self.game_object_proxy.get_resources(self.player)))

        # facts.append(Fact('is_not_occupied', lambda x, y: not self.map_proxy.is_position_occupied(OffsetPosition(int(x), int(y)))))

        return facts

    def first_free_tile(self, terrain_type: str):
        """ Find random tile with given terrain type """
        tiles = self.map_proxy.get_inner_tiles()
        border_tiles = self.map_proxy.get_border_tiles()

        for position in tiles:
            terrain = self.map_proxy.get_terrain_type(position) == TerrainType.from_string(terrain_type)
            if terrain and position not in border_tiles:
                return position
        return None

    # def first_free_tile(self):
    #     """ Find random tile without given terrain type """
    #     tiles = self.map_proxy.get_inner_tiles()
    #     border_tiles = self.map_proxy.get_border_tiles()

    #     for position in tiles:
    #         if position not in border_tiles:
    #             return position
    #     return None

    def visible_free_tile(self):
        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()

        #
        next_wave_direction = self.next_wave_direction()
        #

        for min in tiles:
            occupied = self.map_proxy.is_position_occupied(min)
            if not occupied and min not in border_tiles:
                break

        for position in tiles:
            occupied = self.map_proxy.is_position_occupied(position)
            position_sum = position.q**4 + position.r**4
            min_sum = min.q**4 + min.r**4

            if not occupied and position not in border_tiles:
                if position_sum < min_sum:
                    min = position
                elif position_sum == min_sum and self.direction(position.q, position.r) != next_wave_direction:
                    min = position
                elif position_sum == min_sum and self.map_proxy.get_terrain_type(position) == TerrainType.from_string("MOUNTAIN"):
                    min = position

        return min

    def possible_spawn_tiles(self):
        """ Get list of possible tiles, where enemy spawn a unit """
        spawn_info = self.game_uncertainty_proxy.spawn_information()

        next_round = spawn_info[0]

        possible_tiles = set()
        for unit in next_round:
            possible_tiles.update([x.position for x in unit.positions])

        return possible_tiles

    def direction(self, q, r):
        if q >= 0 and r >= 0:
            return "rb" # right bottom
        elif q >= 0 and r < 0:
            return "rt" # right top
        elif q < 0 and r >= 0:
            return "lb" # left bottom
        else:
            return "lt" # left top
 
    def next_wave_direction(self):
        spawn_info = self.game_uncertainty_proxy.spawn_information()
        next_round = spawn_info[0]
        q = 0
        r = 0
        for unit in next_round:
            for pos in unit.positions:
                q += pos.position.q
                r += pos.position.r

        direction = self.direction(q,r)

        return direction

    def knights_requiered(self):
        knights = 0
        druids = 0
        tiles = self.map_proxy.get_player_visible_tiles()
        for tile in tiles:
            if self.game_object_proxy.get_object_type(tile) == GameObjectType.KNIGHT:
                knights += 1
            if self.game_object_proxy.get_object_type(tile) == GameObjectType.DRUID:
                druids += 1

        if ( druids + 1 ) / ( knights + 1 ) >= 0.65:
            return True
        return False
