# Database "Shop of limited edition (LE)"


 ## Description
 After birth, everyone first wants to buy a pair of elegant basketball shoes that can highlight their uniqueness. Everyone knows that the limited edition basketball shoes * from Shop limited edition (LE) can handle this task. Something about products that can be understood by the term "small accessories"? These sneakers are not sized, they automatically adapt to the size of the feet. Also, using the application (each for a different operating system, module installation speed, application design), which you can download from Google Market after purchasing a pair of sneakers, you can easily redraw them (create your own unique design) or customize LEDs (color, power supply, animation) that are built into the sole. The plants (number of employees and module they produce, address, opening hours, size of warehouse, how many profits) of our company are environmentally friendly and are located in China and all deliveries are made from there. Our company is well aware that each user is unique, so we went ahead and launched sneakers with built-in modules. Currently, there are only 3 modules (module properties can be customized in the application, installation speed, materials used in production (expensive cheap)): running shoes with built-in dryer, sneakers with built-in teleport and running shoes that allow you to walk on water. For each pair of sneakers, it is necessary to purchase a separate charger (wired or cordless, power, packaging). Using modules in everyday life. For example, if you like to travel but don't have the money, then you have the option of ordering sneakers with a built-in teleport. Or if you are an entrepreneur who likes to run, but at the same time you do not like wearing detachable shoes, you can order running shoes with a built-in dryer. Now a little about the workplace (in which language website). When registering, each customer must enter their last name, first name, e-mail address and telephone number. The buyer makes the purchase, determines the number of pairs and modifications. Delivery is made within 3 days in any place in the world using our couriers (name, surname, telephone number, e-mail). Each of our couriers has its own mode of transport (plane, car, motorcycle). After receiving the goods and documentation (it stores a unique order number that will allow you to access the application) You need to download the application and enter the order number, then the functions according to your order will be displayed. It plans to expand its offer in the future, so the database must make it easy to introduce a new type of product. I would also like to draw attention to a flexible system of discounts (percentage for which client). Every 3 pairs of sneakers you get half the price. And if you accumulate our sneakers in bulk, from 10,000 pairs, you will also get nice discounts.


 ## Conceptual schema
![alt text](https://gitlab.fit.cvut.cz/zhurarom/evolution/blob/master/SQL%20database/concept_1.png)

![alt text](https://gitlab.fit.cvut.cz/zhurarom/evolution/blob/master/SQL%20database/concept_2.png)

![alt text](https://gitlab.fit.cvut.cz/zhurarom/evolution/blob/master/SQL%20database/concept_3.png)


 ## Relational schema
 ![alt text](https://gitlab.fit.cvut.cz/zhurarom/evolution/blob/master/SQL%20database/rel.png)
 

Authored by **Roman Zhuravskyi**.

