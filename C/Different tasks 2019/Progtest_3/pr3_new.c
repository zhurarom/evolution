#ifndef PROGTEST
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* PROGTEST */

int clipLine ( double
rx1,
double
ry1,
double
rx2,
double
ry2,
double
* ax,
double
* ay,
double
* bx,
double
* by )
{

double x1, x2, y1, y2, kax, kay, kbx, kby, f=0, fx1=0, fx2=0, fy2=0,
fy1=0;
kax = *ax;
kay = *ay;
kbx = *bx;
kby = *by;

printf ("ANA LOX^2 : %f %f %f %f\n", *ax, *ay, *bx, *by);

x1 = (*ax - kax)*(ry1 - kay)/(kby-kay)*1.0 + kax;
x2 = (kbx - kax)*(ry2 - kay)/(kby-kay)*1.0 + kax;
y1 = (kby - kay)*(rx1 - kax)/(kbx-kax)*1.0 + kay;
y2 = (kby - kay)*(rx2 - kax)/(kbx-kax)*1.0 + kay;

if ( fmax(kax, kbx)>=x1 && fmin(kax, kbx)<=x1 && fmax(rx1, rx2)>=x1 &&
fmin(rx1, rx2)<=x1 )
{ if (f==0)
{fx1=x1; fy1=ry1;}
else {fx2=x1; fy2=ry1;}
f++;}

if ( fmax(kax, kbx)>=x2 && fmin(kax, kbx)<=x2 && fmax(rx1, rx2)>=x2 &&
fmin(rx1, rx2)<=x2 )
{ if (f==0)
{fx1=x2; fy1=ry2;}
else {fx2=x2; fy2=ry2;}
f++;}

if ( fmax(kay, kby)>=y1 && fmin(kay, kby)<=y1 && fmax(ry1, ry2)>=y1 &&
fmin(ry1, ry2)<=y1 )
{ if (f==0)
{fy1=y1; fx1=rx1;}
else {fy2=y1; fx2=rx1;}
f++;}

if ( fmax(kay, kby)>=y2 && fmin(kay, kby)<=y2 && fmax(ry1, ry2)>=y2
&& fmin(ry1, ry2)<=y2 )
{ if (f==0)
{fy1=y1; fx1=rx1;}
else {fy2=y1; fx2=rx1;}
f++;}

if ((fx1==fx2 && fy1==fy2) || f!=2) return 0;

printf ("ANA LOX : %f %f %f %f\n", fx1, fy1, fx2, fy2);

*ax = fx1;
*ay = fy1;
*bx = fx2;
*by = fy2;

printf ("ANA LOX^2 : %f %f %f %f\n", *ax, *ay, *bx, *by);

return 1;
}

#ifndef PROGTEST
int almostEqual ( double
x,
double
y )
{
/* todo */
}

int main ( void )
{
double x1, y1, x2, y2;

x1 = 60;
y1 = 40;
x2 = 70;
y2 = 50;

clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 );

// assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
// && almostEqual ( x1, 60 )
// && almostEqual ( y1, 40 )
// && almostEqual ( x2, 70 )
// && almostEqual ( y2, 50 ) );

// x1 = 0;
// y1 = 50;
// x2 = 20;
// y2 = 30;
// assert ( clipLine ( 90, 100, 10, 20, &x1, &y1, &x2, &y2 )
// && almostEqual ( x1, 10 )
// && almostEqual ( y1, 40 )
// && almostEqual ( x2, 20 )
// && almostEqual ( y2, 30 ) );

// x1 = 0;
// y1 = 30;
// x2 = 120;
// y2 = 150;
// assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
// && almostEqual ( x1, 10 )
// && almostEqual ( y1, 40 )
// && almostEqual ( x2, 70 )
// && almostEqual ( y2, 100 ) );

// x1 = -10;
// y1 = -10;
// x2 = -20;
// y2 = -20;
// assert ( ! clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 ) );

// x1 = 0;
// y1 = 30;
// x2 = 20;
// y2 = 10;
// assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
// && almostEqual ( x1, 10 )
// && almostEqual ( y1, 20 )
// && almostEqual ( x2, 10 )
// && almostEqual ( y2, 20 ) );

// x1 = 0;
// y1 = 0.3553;
// x2 = 10.45;
// y2 = 0;
// assert ( clipLine ( 0.95, 0.323, 1, 1, &x1, &y1, &x2, &y2 )
// && almostEqual ( x1, 0.95 )
// && almostEqual ( y1, 0.323 )
// && almostEqual ( x2, 0.95 )
// && almostEqual ( y2, 0.323 ) );
return 0;
}
#endif /* PROGTEST */

