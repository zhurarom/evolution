#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#define RECT_NO_OVERLAP 0
#define RECT_OVERLAP    1
#define RECT_A_IN_B     2
#define RECT_B_IN_A     3
#define RECT_ERROR    (-1)
#endif /* __PROGTEST__ */

#include <math.h>
#include <stdbool.h>


//Only one point is inside
bool isInside(          int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2) {

    if((ax1 <= bx1 && bx1 <= ax2 && ay1 <= by2 && by2 <= ay2) ||
      (ax1 <= bx2 && bx2 <= ax2 && ay1 <= by2 && by2 <= ay2) ||
      (ax1 <= bx1 && bx1 <= ax2 && ay1 <= by1 && by1 <= ay2) ||
      (ax1 <= bx2 && bx2 <= ax2 && ay1 <= by1 && by1 <= ay2)) {
        //  printf("1");
          return true;
    } 

    if((bx1 <= ax1 && ax1 <= bx2 && by1 <= ay2 && ay2 <= by2) ||
      (bx1 <= ax2 && ax2 <= bx2 && by1 <= ay2 && ay2 <= by2) ||
      (bx1 <= ax1 && ax1 <= bx2 && by1 <= ay1 && ay1 <= by2) ||
      (bx1 <= ax2 && ax2 <= bx2 && by1 <= ay1 && ay1 <= by2) ) {
          //printf("2");
          
          return true;
    }

    if( (ax1 <= bx1 && bx1 <= ax2 && by2 >= ay2 && by2 <= ay1) ||
        (ax1 <= bx1 && bx1 <= ax2 && by1 >= ay2 && by1 <= ay1) ||
        (ax1 <= bx2 && bx2 <= ax2 && by2 >= ay2 && by2 <= ay1) ||
        (ax1 <= bx2 && bx2 <= ax2 && by1 >= ay2 && by1 <= ay1) ) {
         // printf("3");
            
          return true;
    }

    if( (bx1 <= ax1 && ax1 <= bx2 && ay2 >= by2 && ay2 <= by1) ||
        (bx1 <= ax1 && ax1 <= bx2 && ay1 >= by2 && ay1 <= by1) ||
        (bx1 <= ax2 && ax2 <= bx2 && ay2 >= by2 && ay2 <= by1) ||
        (bx1 <= ax2 && ax2 <= bx2 && ay1 >= by2 && ay1 <= by1) ) {
         // printf("4");
            
          return true;
    }

    return false;
}

bool isOverlap(int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2)   {
    

    //IF we have the same coordinates
    // if( (ax1 == bx1 && ay1 == by1 && ax2 == bx2 && ay2 == by2) ||
    //     (ax1 == bx1 && ay2 == by1 && ax2 == bx2 && ay2 == by1) ||
    //     (ax1 == bx2 && ay2 == by2 && ax2 == bx1 && ay1 == by1) ) {
    //     return true;
    // }

    printf("%d ----- %d ------- %d--------%d-------\n", ax1, ay1, ax2, ay2);
    printf("%d ----- %d ------- %d--------%d-------\n", bx1, by1, bx2, by2);


    if(ax2 == bx1 && ay1 == by1 && ax1 == bx2 && ay2 == by2) {
        return true;
    }

    return false;

}


bool rect_B_in_A(int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2) {

    if(ax1 <= bx1 && bx1 <= ax2 && ay1 <= by1 && by1 <= ay2 &&
       ax1 <= bx2 && bx2 <= ax2 && ay1 <= by2 && by2 <= ay2 ) {
           return true;
    }

    if( ax1 <= bx1 && bx1 <= ax2 && ay2 <= by1 && by1 <= ay1 &&
        ax1 <= bx2 && bx2 <= ax2 && ay2 <= by2 && by2 <= ay1) {
        return true;
    }

    return false;
}

bool isNoOverlap(int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2) {
    if(ax2 == bx1 && (ay2 <= by1 || by1 <= ay2) && ax2 <= bx2 && ay2 <= by2) {
        return true;
    }
}


//All points are inside
bool rect_A_in_B(int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2) {


    if(bx1 <= ax1 && ax1 <= bx2 && by1 <= ay1 && ay1 <= by2 &&
       bx1 <= ax2 && ax2 <= bx2 && by1 <= ay2 && ay2 <= by2 ) {
           return true;
    }

    if( bx1 <= ax1 && ax1 <= bx2 && by2 <= ay1 && ay1 <= by1 &&
        bx1 <= ax2 && ax2 <= bx2 && by2 <= ay2 && ay2 <= by1) {
        return true;
    }

    return false;
}


int rectanglePosition ( int ax1, int ay1,
                        int ax2, int ay2,
                        int bx1, int by1,
                        int bx2, int by2 )
{

    int a1 = sqrt(pow(ax1 - ax1, 2) + pow(ay2 - ay1, 2));
    int b1 = sqrt(pow(ax2 - ax1, 2) + pow(ay1 - ay1, 2));

    int a2 = sqrt(pow(bx1 - bx1, 2) + pow(by2 - by1, 2));
    int b2 = sqrt(pow(bx2 - bx1, 2) + pow(by1 - by1, 2));

    //printf("%d ----- %d\n", a1, b1);

    //FOR RECT_A_IN_B

    if(isOverlap(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)) {
        printf("HERE");
        return RECT_OVERLAP;
    }

    if( rect_A_in_B(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) ) {
        printf("rect_A_in_B\n");
        return RECT_A_IN_B;
    }

    if(rect_B_in_A(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)) {
        printf("rect_B_in_A\n");    
        return RECT_B_IN_A;
    } 
    
    if(isInside(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)) {
        printf("isInside\n");        
        return RECT_OVERLAP;
    } 
    
    if(isNoOverlap(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)) {
        return RECT_NO_OVERLAP;
    }

    if(!isInside(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) || !isOverlap(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2)) {
        return RECT_NO_OVERLAP;
    }

    if( a1 <= 0 || b1 <= 0 ||
        a2 <= 0 || b2 <= 0) {
        printf("7");
        return RECT_ERROR;
    } 

    printf("The end\n");
    
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{

//    rectanglePosition ( 0, 0, 50, 20,
//                                  -100, 100, 100, 90  );

  assert ( rectanglePosition ( 0, 0, 50, 20,
                               10, 5, 75, 40 ) == RECT_OVERLAP );
  assert ( rectanglePosition ( 0, 20, 50, 0,
                               75, 40, 10, 5 ) == RECT_OVERLAP );
  assert ( rectanglePosition ( 0, 0, 50, 20,
                                -100, 100, 100, 90 ) == RECT_NO_OVERLAP );
  assert ( rectanglePosition ( 0, 0, 50, 20,
                                 50, -100, 100, 100 ) == RECT_NO_OVERLAP );
  assert ( rectanglePosition ( 0, 0, 10, 10,
                               2, 8, 4, 6 ) == RECT_B_IN_A );
  assert ( rectanglePosition ( 2, 6, 3, 7,
                              1, 5, 4, 8 ) == RECT_A_IN_B );
//  assert ( rectanglePosition ( 1, 6, 3, 7,
//                                1, 5, 4, 8 ) == RECT_OVERLAP );
  assert ( rectanglePosition ( 0, 0, 1, 1,
                               1, 0, 0, 1 ) == RECT_OVERLAP );
  assert ( rectanglePosition ( 0, 0, 50, 20,
                               50, -100, 100, -100 ) == RECT_ERROR );
  return 0;
}

#endif /* __PROGTEST__ */