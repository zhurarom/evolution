#ifndef __PROGTEST__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* __PROGTEST__ */


#include <stdbool.h>

#define ZAJEBIS 1
#define CHUJEVO 0

typedef struct Point{
  double x;
  double y;
} Point;

bool isPointOnSide(Point r1, Point r2, Point inter) {
  return fabs((inter.x - r1.x) / (r2.x - r1.x) - (inter.y - r1.y) / (r2.y - r1.y)) <= 1e-6;
}

bool isOn(Point r1, Point r2, Point inter) {
  return (inter.x == r1.x && inter.y >= r1.y && inter.y <= r2.y) || 
          (inter.y == r1.y && inter.y >= r1.x && inter.y <= r2.x);
}

bool crossed(Point r1, Point r2, Point a, Point b) {
  return fmin(a.x, b.x) <= fmax(r1.x, r2.x) && fmax(a.x, b.x) >= fmin(r1.x, r2.x) &&
         fmin(a.y, b.y) <= fmax(r1.y, r2.y) && fmax(a.y, b.y) >= fmin(r1.y, r2.y);
}

bool insideOfRect(Point r1, Point r2, Point inter ) {
  return fmin(r1.x, r2.x) <= inter.x && inter.x <= fmax(r1.x, r2.x) &&
         fmin(r1.y, r2.y) <= inter.y && inter.y <= fmax(r1.y, r2.y);
} 

/**
 * Representation of line AB == a1x + b1y = c1;
 * Representation of line CD == a2x + b2y = c2;
*/
bool intersectionOfTwoLines(Point r1, Point r2, Point a, Point b, Point *intersection) {
  
  //Create a line from A(rx1, ry1) and B(rx2, ry2)
  double a1 = r2.y - r1.y;  
  double b1 = r1.x - r2.x;
  double c1 = a1 * r1.x + b1 * r1.y;

  //Create a line from C(*ax, *ay) and D(*bx, *by)
  double a2 = b.y - a.y;
  double b2 = a.x - b.x;
  double c2 = a2 * (a.x) + b2 * (a.y);  

  double det = a1 * b2 - a2 * b1;
  
  if(det == 0) {
    //printf("Lines are parallel\n");
    intersection->x = -1;
    intersection->y = -1;
    return CHUJEVO;
  } else {
    intersection->x = (b2 * c1 - b1 * c2) / det;
    intersection->y = (a1 * c2 - a2 * c1) / det;
    return ZAJEBIS;
  }

}



int                clipLine                                ( double            rx1,
                                                             double            ry1,
                                                             double            rx2,
                                                             double            ry2,
                                                             double          * ax,
                                                             double          * ay,
                                                             double          * bx,
                                                             double          * by )
{
  Point r1 = {rx1, ry1};
  Point r2 = {rx2, ry1};    
  Point r3 = {rx1, ry2};
  Point r4 = {rx2, ry2};

  Point a = {*ax, *ay};
  Point b = {*bx, *by};

  Point inter = {0, 0};
  Point first_struct = {-100, -100};
  Point second_struct = {-100, -100};

  int counter = 0;
  int first_counter = 0;


  if( crossed(r1, r4, a, b)) {
  //printf("Is crossed\n");

    // if(isPointOnSide(r1, r2, a)|| isPointOnSide(r1, r3, a) || 
    //   isPointOnSide(r2, r3, a) || isPointOnSide(r1, r4, a) )  {
    //   *ax = *bx;
    //   *ay = *by;
    //   printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    //   printf("SECOND : %lf ----- %lf\n", *bx, *by);
    //   return ZAJEBIS;
    // } else if(isPointOnSide(r1, r2, b) || isPointOnSide(r1, r3, b)  ||
    //           isPointOnSide(r2, r3, b) || isPointOnSide(r1, r4, b)  ) {
    //   *bx = *ax;
    //   *by = *ay;
    //   printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    //   printf("SECOND : %lf ----- %lf\n", *bx, *by);
    //   return ZAJEBIS;
    // }


    if(isOn(r1, r4, a)) {
      *bx = *ax;
      *by = *ay;
      //printf("FIRST : %lf ----- %lf\n", *ax, *ay);
      //printf("SECOND : %lf ----- %lf\n", *bx, *by);
      return ZAJEBIS;
    }

    if(isOn(r1, r4, b)) {
      *ax = *bx;
      *ay = *by;
      //printf("FIRST : %lf ----- %lf\n", *ax, *ay);
      //printf("SECOND : %lf ----- %lf\n", *bx, *by);
      return ZAJEBIS;
    }

    //If my line is inside -> print my line
    if(insideOfRect(r1, r4, a) && insideOfRect(r1, r4, b) ) {
        printf("INSIDE FIRST : %lf ----- %lf\n", *ax, *ay);
        printf("INSIDE SECOND : %lf ----- %lf\n", *bx, *by);
        return ZAJEBIS;
    }

    if(insideOfRect(r1, r4, a) && !insideOfRect(r1, r4, b)) {
      first_struct.x = a.x;
      first_struct.y = a.y;
      first_counter++;
      counter++;
    } else if(!insideOfRect(r1, r4, a) && insideOfRect(r1, r4, b)) {
      first_struct.x = b.x;
      first_struct.y = b.y;
      first_counter++;
      counter++;
    }


    //printf("1\n");
    if(intersectionOfTwoLines(r1, r2, a, b, &inter) ) {
      if(insideOfRect(r1, r4, inter)) {
         counter++;
         first_counter++;
         if(counter == 1) {
           first_struct.x = inter.x;
           first_struct.y = inter.y;
         } else {
           second_struct.x = inter.x;
           second_struct.y = inter.y;
         }
         //printf("1A : %lf ------ %lf\n", first_struct.x, first_struct.y);
         //printf("1B : %lf ------ %lf\n", first_struct.x, first_struct.y);
      }
    }

    //printf("2\n");
    if(intersectionOfTwoLines(r3, r4, a, b, &inter)) {
       if( insideOfRect(r1, r4, inter)) {
         counter++;
         first_counter++;
         if(counter >= 2 && first_counter != 3) {
           second_struct.x = inter.x;
           second_struct.y = inter.y;
         } else if(counter == 1 && first_counter != 3) {
           first_struct.x = inter.x;
           first_struct.y = inter.y;
         } 
         //printf("2A : %lf ------ %lf\n", first_struct.x, first_struct.y);
         //printf("2B : %lf ------ %lf\n", first_struct.x, first_struct.y);
         //is_a = 1;
      }
    }


    //printf("3\n");
    if( intersectionOfTwoLines(r2, r4, a, b, &inter) ) {
      if( insideOfRect(r1, r4, inter)) {
        counter++;
        first_counter++; 
        if(counter >= 2 && first_counter != 4) {
           second_struct.x = inter.x;
           second_struct.y = inter.y;
        } else if(counter == 1 && first_counter != 4) {
           first_struct.x = inter.x;
           first_struct.y = inter.y;
        }

         //printf("3A : %lf ------ %lf\n", first_struct.x, first_struct.y);
         //printf("3B : %lf ------ %lf\n", first_struct.x, first_struct.y);
      }
    }


  //printf("4\n");
  if(counter == 1 || counter == 0) {
    if( intersectionOfTwoLines(r1, r3, a, b, &inter) ) {
      if( insideOfRect(r1, r4, inter)) {
        counter++;
        first_counter++;
        if(counter >= 2 && first_counter != 5) {
           second_struct.x = inter.x;
           second_struct.y = inter.y;
        } else if(counter == 1 && first_counter != 5) {
           first_struct.x = inter.x;
           first_struct.y = inter.y;
        }

         //printf("4A : %lf ------ %lf\n", first_struct.x, first_struct.y);
         //printf("4B : %lf ------ %lf\n", first_struct.x, first_struct.y);
      }
    }
  }


  if(counter == 0) {
    // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    // printf("SECOND : %lf ----- %lf\n", *bx, *by);
     return CHUJEVO;
  }


  if(counter == 1 && (second_struct.x == -100.0) && (second_struct.y == -100.0)) {
    second_struct.x = first_struct.x;
    second_struct.y = first_struct.y;
  } else if(counter == 1 && (first_struct.x == -100.0) && (first_struct.y == -100.0)) {
    first_struct.x = second_struct.x;
    first_struct.y = second_struct.y;
  }

    if(first_struct.x <= second_struct.x) {
      *ax = first_struct.x;
      *ay = first_struct.y;
      *bx = second_struct.x;
      *by = second_struct.y;
      // printf("A : %lf -------- %lf\n", *ax, *ay);
      // printf("B : %lf -------- %lf\n", *bx, *by);
    } else {
      *ax = second_struct.x;
      *ay = second_struct.y;
      *bx = first_struct.x;
      *by = first_struct.y;
      // printf("A : %lf -------- %lf\n", *ax, *ay);
      // printf("B : %lf -------- %lf\n", *bx, *by);
    }

    printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    printf("SECOND : %lf ----- %lf\n", *bx, *by);


    return ZAJEBIS;
  } 

  //printf("IsSSSS NOT crossed\n");
  return CHUJEVO;

}


#ifndef __PROGTEST__
int                almostEqual                             ( double            x,
                                                             double            y )
{
  return fabs(x - y) <= 1e-6;
}

int                main                                    ( void )
{
  double x1, y1, x2, y2;

//   x1 = 60;
// y1 = 40;
// x2 = 70;
// y2 = 50;
// assert (clipLine(10, 20, 90, 100, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 60)
// && almostEqual(y1, 40)
// && almostEqual(x2, 70)
// && almostEqual(y2, 50));

x1 = 0;
y1 = 50;
x2 = 20;
y2 = 30;
assert (clipLine(90, 100, 10, 20, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 10)
&& almostEqual(y1, 40)
&& almostEqual(x2, 20)
&& almostEqual(y2, 30));

x1 = 0;
y1 = 30;
x2 = 120;
y2 = 150;
assert (clipLine(10, 20, 90, 100, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 10)
&& almostEqual(y1, 40)
&& almostEqual(x2, 70)
&& almostEqual(y2, 100));

x1 = -10;
y1 = -10;
x2 = -20;
y2 = -20;
assert (!clipLine(10, 20, 90, 100, &x1, &y1, &x2, &y2));

x1 = 0;
y1 = 30;
x2 = 20;
y2 = 10;
assert (clipLine(10, 20, 90, 100, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 10)
&& almostEqual(y1, 20)
&& almostEqual(x2, 10)
&& almostEqual(y2, 20));

x1 = 0;
y1 = 0.3553;
x2 = 10.45;
y2 = 0;
assert (clipLine(0.95, 0.323, 1, 1, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 0.95)
&& almostEqual(y1, 0.323)
&& almostEqual(x2, 0.95)
&& almostEqual(y2, 0.323));
x1 = -1072;
y1 = -1746;
x2 = -545.5;
y2 = -328.5;
assert (clipLine(-604, -486, -370, 144, &x1, &y1, &x2, &y2)
&& almostEqual(x1, -604)
&& almostEqual(y1, -486)
&& almostEqual(x2, -545.5)
&& almostEqual(y2, -328.5));

x1 = -450;
y1 = -386.25;
x2 = -904;
y2 = -978;
assert (clipLine(-450, -452, -223, -189, &x1, &y1, &x2, &y2)
&& almostEqual(x1, -450)
&& almostEqual(y1, -386.25)
&& almostEqual(x2, -450)
&& almostEqual(y2, -386.25));

x1 = 11;
y1 = 1316.75;
x2 = 11;
y2 = 1012.25;

assert (clipLine(11, 1469, -830, 860, &x1, &y1, &x2, &y2)
&& almostEqual(x1, x1)
&& almostEqual(y1, y1)
&& almostEqual(x2, x2)
&& almostEqual(y2, y2));



x1 = -149;
y1 = -6;
x2 = 691;
y2 = 925.5;
assert (clipLine(691, 822, 1111, 1236, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 691)
&& almostEqual(y1, 925.5)
&& almostEqual(x2, 691)
&& almostEqual(y2, 925.5));



x1 = -2412;
y1 = -2721;
x2 = -654;
y2 = -885;
assert (clipLine(-654, 33, 225, -885, &x1, &y1, &x2, &y2)
&& almostEqual(x1, -654)
&& almostEqual(y1, -885)
&& almostEqual(x2, -654)
&& almostEqual(y2, -885));

x1 = 0;
y1 = 0.8448;
x2 = 3.685;
y2 = 0;
assert (clipLine(0.335, 0.768, 1, 1, &x1, &y1, &x2, &y2)
&& almostEqual(x1,0.335)
&& almostEqual(y1,0.768)
&& almostEqual(x2,0.335)
&& almostEqual(y2,0.768));

/*
x1 = 755;
y1 = 774;
x2 = -1143;
y2 = 774;
assert (clipLine(755, 774, 1704, 1492, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 755)
&& almostEqual(y1, 774)
&& almostEqual(x2, 755)
&& almostEqual(y2, 774));

x1 = 0;
y1 = 3;
x2 = 10;
y2 = 3;
assert (clipLine(3, 5, 1, 1, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 1)
&& almostEqual(y1, 3)
&& almostEqual(x2, 3)
&& almostEqual(y2, 3));

x1 = 0;
y1 = 1;
x2 = 10;
y2 = 1;
assert (clipLine(3, 5, 1, 1, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 1)
&& almostEqual(y1, 1)
&& almostEqual(x2, 3)
&& almostEqual(y2, 1));

x1 = 1;
y1 = 1;
x2 = 1;
y2 = 5;
assert (clipLine(3, 5, 1, 1, &x1, &y1, &x2, &y2)
&& almostEqual(x1, x1)
&& almostEqual(y1, y1)
&& almostEqual(x2, x2)
&& almostEqual(y2, y2));


x1 = -8;
y1 = -3;
x2 = 8;
y2 = 3;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&&
almostEqual(x1, 0)
&& almostEqual(y1, 0)
&& almostEqual(x2, 4)
&& almostEqual(y2, 1.5));

x1 = -2;
y1 = -3;
x2 = 4;
y2 = 6;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 0)
&& almostEqual(y1, 0)
&& almostEqual(x2, 2)
&& almostEqual(y2, 3));

x1 = -2;
y1 = 6;
x2 = 4;
y2 = -3;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 0)
&& almostEqual(y1, 3)
&& almostEqual(x2, 2)
&& almostEqual(y2, 0));

x1 = 0;
y1 = -3;
x2 = 6;
y2 = 6;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 2)
&& almostEqual(y1, 0)
&& almostEqual(x2, 4)
&& almostEqual(y2, 3));

x1 = -8;
y1 = 6;
x2 = 8;
y2 = 0;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 0)
&& almostEqual(y1, 3)
&& almostEqual(x2, 4)
&& almostEqual(y2, 1.5));



//NOPE
// x1 = 0;
// y1 = 0.6083;
// x2 = 3.047;
// y2 = 0;
// assert (clipLine ( 0.277, 0.553, 1, 1, &x1, &y1, &x2, &y2)
// && almostEqual(x1,0.277)
// && almostEqual(y1,0.553)
// && almostEqual(x2,0.277)
// && almostEqual(y2,0.553));


//NOPE
// x1 = -4;
// y1 = 0;
// x2 = 12;
// y2 = 6;
// assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 0)
// && almostEqual(y1, 1.5)
// && almostEqual(x2, 4)
// && almostEqual(y2, 3));

//NOPE
// x1 = -4;
// y1 = 3;
// x2 = 12;
// y2 = -3;
// assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 0)
// && almostEqual(y1, 1.5)
// && almostEqual(x2, 4)
// && almostEqual(y2, 0));


//NOPE
x1 = 0;
y1 = 6;
x2 = 6;
y2 = -3;
assert (clipLine(0, 0, 4, 3, &x1, &y1, &x2, &y2)
&& almostEqual(x1, 2)
&& almostEqual(y1, 3)
&& almostEqual(x2, 4)
&& almostEqual(y2, 0));

//NOPE
// x1 = 0;
// y1 = 1.0692;
// x2 = 5.423;
// y2 = 0;
// assert (clipLine ( 0.493, 0.972, 1, 1, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 0.493)
// && almostEqual(y1, 0.972)
// && almostEqual(x2, 0.493)
// && almostEqual(y2, 0.972));






//!
// x1 = 2500;
// y1 = -96;
// x2 = 1138;
// y2 = 290;
// assert (!clipLine(457, -289, 1138, -96, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 2500)
// && almostEqual(y1, -96)
// && almostEqual(x2, 1138)
// && almostEqual(y2, 290));

// x1 = -1115;
// y1 = 145;
// x2 = -1627;
// y2 = 145;
// assert (!clipLine ( -603, 145, -91, 690, &x1, &y1, &x2, &y2)
// && almostEqual(x1, -1115)
// && almostEqual(y1, 145)
// && almostEqual(x2, -1627)
// && almostEqual(y2, 145));

// x1 = 1096;
// y1 = 261;
// x2 = 1594;
// y2 = -139;
// assert (!clipLine(1096, -139, 847, -339, &x1, &y1, &x2, &y2)
// && almostEqual(x1, 1096)
// && almostEqual(y1, 261)
// && almostEqual(x2, 1594)
// && almostEqual(y2, -139));

              */
  return 0;
} 
#endif /* __PROGTEST__ */