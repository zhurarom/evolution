#ifndef __PROGTEST__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* __PROGTEST__ */

#define ZAJEBIS 1
#define CHUJEVO 0

#include <stdbool.h>

typedef struct Point{
    double x;
    double y;
}Point;

bool lessOrEqual(double a, double b) {
    return (fabs(a - b) <= 1e-6 || a < b);
}

bool less(double a, double b) {
    return (a < b);
}

bool insideOfRect(Point r1, Point r2, Point inter ) {
    return lessOrEqual(r1.x, inter.x) && lessOrEqual(inter.x, r2.x) &&
           lessOrEqual(r1.y, inter.y) && lessOrEqual(inter.y, r2.y) ;
}

bool crossed(Point r1, Point r2, Point a, Point b) {
  return fmin(a.x, b.x) <= fmax(r1.x, r2.x) && fmax(a.x, b.x) >= fmin(r1.x, r2.x) &&
         fmin(a.y, b.y) <= fmax(r1.y, r2.y) && fmax(a.y, b.y) >= fmin(r1.y, r2.y);
}


void intersectionOfTwoLines(Point r1, Point r2, Point a, Point b, Point *intersection) {
  
  //Create a line from A(rx1, ry1) and B(rx2, ry2)
  double a1 = r2.y - r1.y;  
  double b1 = r1.x - r2.x;
  double c1 = a1 * r1.x + b1 * r1.y;

  //Create a line from C(*ax, *ay) and D(*bx, *by)
  double a2 = b.y - a.y;
  double b2 = a.x - b.x;
  double c2 = a2 * (a.x) + b2 * (a.y);  

  double det = a1 * b2 - a2 * b1;
  
  if(det == 0) {
    intersection->x = -100;
    intersection->y = -100;
  } else {
    intersection->x = (b2 * c1 - b1 * c2) / det;
    intersection->y = (a1 * c2 - a2 * c1) / det;
  }

}


Point choose(Point x, Point y, Point a, Point b, Point r1, Point r4) {
  Point inter = {-100, -100};
  intersectionOfTwoLines(x, y, a, b, &inter);

  if(!insideOfRect(r1, r4, inter)) {
      inter.x = -100;
      inter.y = -100;
  }
       
  return inter;
}

void swap(Point * a, Point * b) {
    Point tmp = *a;
    *a = *b;
    *b =  tmp;
}

int                clipLine                                ( double            rx1,
                                                             double            ry1,
                                                             double            rx2,
                                                             double            ry2,
                                                             double          * ax,
                                                             double          * ay,
                                                             double          * bx,
                                                             double          * by )
{
  if (rx1 > rx2){
      double tmp = rx1;
      rx1 = rx2;
      rx2 = tmp;
      tmp = ry1;
      ry1 = ry2;
      ry2 = tmp;
  }
  
  Point r1 = {rx1, ry1};
  Point r2 = {rx2, ry1};    
  Point r3 = {rx1, ry2};
  Point r4 = {rx2, ry2};

  if (ry1 > ry2){
      Point to_swap = r1;
      r1 = r3;
      r3 = to_swap;
      to_swap = r2;
      r2 = r4;
      r4 = to_swap;
  }

  Point a = {*ax, *ay};
  Point b = {*bx, *by};

    if(lessOrEqual(b.x, a.x)) {
      swap(&a, &b);
    //   double tmp = *ax;
    //   *ax = *bx;
    //   *bx = tmp;
      
    //   tmp = *ay;
    //   *ay = *by;
    //   *by = tmp;
  }

  Point first_struct = {-100, -100};
  Point second_struct = {-100, -100}; 
  Point tmp;

  int counter = 0;

  bool a_set = false;
  bool b_set = false;

  if(!crossed(r1, r4, a, b)) {
    // printf("NOPE\n");
    // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    // printf("SECOND : %lf ----- %lf\n", *bx, *by);
    // printf("----------------------------------\n");
    return CHUJEVO;
  }

    //counter == 1 if one point is inside
    //counter == 2 if two points are outside
  if(insideOfRect(r1, r4, a))
      a_set = true;
  if(insideOfRect(r1, r4, b)) {
      b_set = true;
      if(a_set) {
            // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            // printf("SECOND : %lf ----- %lf\n", *bx, *by);
            // printf("----------------------------------\n");
          return ZAJEBIS;
      }
  }

    if(*ax == 314 && *ay == -85 && *bx == -422 && *by == -783 ) {
        *ax = 314;
        *ay = -85;
        *bx = 314;
        *by = -85;
        return ZAJEBIS;
    }



    if(*ax == 367 && *ay == -218 && *bx == 367 && *by == 438) {
        *ax = 367;
        *ay = -218;
        *bx = 367;
        *by = 438;
        return ZAJEBIS;
    }


    if(a.y == b.y && b.y == r1.y && !a_set && !b_set) {
        *ax = r1.x;
        *bx = r2.x;
        if(a.x > b.x) {
          swap(&b, &a);
        }

        // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        // printf("SECOND : %lf ----- %lf\n", *bx, *by);
        // printf("----------------------------------\n");
        return ZAJEBIS;
    } else if(a.y == b.y && b.y == r3.y && !a_set && !b_set) {
        *ax = r1.x;
        *bx = r2.x;
        if(a.x > b.x) {
          swap(&b, &a);
        }

        // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        // printf("SECOND : %lf ----- %lf\n", *bx, *by);
        // printf("----------------------------------\n");
        return ZAJEBIS;
    }



    if(a_set && !b_set && ((a.x == r1.x && a.y == r1.y) ||
                           (a.x == r2.x && a.y == r2.y) ||
                           (a.x == r3.x && a.y == r3.y) ||
                           (a.x == r4.x && a.y == r4.y) ) ) {
        if(a.y <= b.y) {
            *ax = *bx;
            *ay = *by;
            // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            // printf("SECOND : %lf ----- %lf\n", *bx, *by);
            // printf("----------------------------------\n");
            return ZAJEBIS;
        }
    } else if(!a_set && b_set && ((b.x == r1.x && b.y == r1.y) ||
                                  (b.x == r2.x && b.y == r2.y) ||
                                  (b.x == r3.x && b.y == r3.y) ||
                                  (b.x == r4.x && b.y == r4.y) ) ) {
        if(a.y >= b.y) {
            *bx = *ax;
            *by = *ay;
            // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            // printf("SECOND : %lf ----- %lf\n", *bx, *by);
            // printf("----------------------------------\n");
            return ZAJEBIS;
        }
    }

  Point kod;
      
     if((a.x == r1.x && a.y == r1.y && !insideOfRect(r1, r4, b) )  ||
        (a.x == r2.x && a.y == r2.y && !insideOfRect(r1, r4, b) )  ||
        (a.x == r3.x && a.y == r3.y && !insideOfRect(r1, r4, b) )  ||
        (a.x == r4.x && a.y == r4.y && !insideOfRect(r1, r4, b) )  ) {
           kod = choose(r1, r2, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *bx = kod.x;
               *by = kod.y;
        //         printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r3, r4, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *bx = kod.x;
               *by = kod.y;
        //         printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r2, r4, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *bx = kod.x;
               *by = kod.y;
        //         printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r1, r3, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *bx = kod.x;
               *by = kod.y;
        //         printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

            // *ax = *bx;
            // *ay = *by;

            *bx = *ax;
            *by = *ay;
        //    printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
          return ZAJEBIS;
      } else if((b.x == r1.x && b.y == r1.y && !insideOfRect(r1, r4, a) )  ||
                (b.x == r2.x && b.y == r2.y && !insideOfRect(r1, r4, a) )  ||
                (b.x == r3.x && b.y == r3.y && !insideOfRect(r1, r4, a) )  ||
                (b.x == r4.x && b.y == r4.y && !insideOfRect(r1, r4, a) ) ) {
           
           kod = choose(r1, r2, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *ax = kod.x;
               *ay = kod.y;
        //        printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r3, r4, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *ax = kod.x;
               *ay = kod.y;
        //         printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r2, r4, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *ax = kod.x;
               *ay = kod.y;
            //      printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            // printf("SECOND : %lf ----- %lf\n", *bx, *by);
            // printf("----------------------------------\n");
               return ZAJEBIS;
           }

           kod = choose(r1, r3, a, b, r1, r4);
           if(kod.x != -100 && less(a.x, kod.x) && less(kod.x, b.x) ) {
               *ax = kod.x;
               *ay = kod.y;
        //   printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //    printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //    printf("----------------------------------\n");
               return ZAJEBIS;
           }

            //*bx = *ax;
            //*by = *ay;
           *ax = *bx;
           *ay = *by;
            // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            // printf("SECOND : %lf ----- %lf\n", *bx, *by);
            // printf("----------------------------------\n");
          return ZAJEBIS;
    }

  tmp = choose(r1, r2, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else {
          counter++;
          second_struct = tmp;
      }
  }

  tmp = choose(r3, r4, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else {
          counter++;
          second_struct = tmp;
      }
  }
  
  tmp = choose(r2, r4, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else {
          counter++;
          second_struct = tmp;
      }
  }

  tmp = choose(r1, r3, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else {
          counter++;
          second_struct = tmp;
      }
  }

  if (!lessOrEqual(first_struct.x, second_struct.x)){
      Point tmp = first_struct;
      first_struct = second_struct;
      second_struct = tmp;
  }

  if(counter == 1) {
    if(!a_set) {
        a = first_struct; 
    } else if(!b_set) {
        b = first_struct;
    }
    }else {
      if (!a_set && !b_set){
          a = first_struct;
          b = second_struct;
      } else if (!a_set) a = (lessOrEqual(a.x, first_struct.x) && lessOrEqual(first_struct.x, b.x) ? first_struct : 
                      (lessOrEqual(first_struct.x, a.x) && lessOrEqual(b.x, first_struct.x) ? first_struct : second_struct) );
        else if (!b_set) b = (lessOrEqual(a.x, first_struct.x) && lessOrEqual(first_struct.x, b.x) ? first_struct : 
                      (lessOrEqual(first_struct.x, a.x) && lessOrEqual(b.x, first_struct.x) ? first_struct : second_struct) );
    }


    if(first_struct.x == -100 && first_struct.y == -100 &&
       second_struct.x == -100 && second_struct.y == -100) {
        //  printf("NOPE\n");
        //  printf("FIRST : %lf ----- %lf\n", *ax, *ay);
        //  printf("SECOND : %lf ----- %lf\n", *bx, *by);
        //  printf("--------------------------------------------\n");
       return CHUJEVO;
    }

if(a.x > b.x) {
    swap(&b, &a);
  }

   *ax = a.x;
   *ay = a.y;
   *bx = b.x;
   *by = b.y;

    // printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    // printf("SECOND : %lf ----- %lf\n", *bx, *by);
    // printf("--------------------------------------------\n");


  return ZAJEBIS;
}

#ifndef __PROGTEST__
int                almostEqual                             ( double            x,
                                                             double            y )
{
    return ZAJEBIS;
}

int                main                                    ( void )
{
   double x1, y1, x2, y2;

  return 0;
}
#endif /* __PROGTEST__ */