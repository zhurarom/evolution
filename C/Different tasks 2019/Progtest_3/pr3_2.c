#ifndef __PROGTEST__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* __PROGTEST__ */


#include <stdbool.h>

#define ZAJEBIS 1
#define CHUJEVO 0

typedef struct Point{
  double x;
  double y;
} Point;

bool crossed(Point r1, Point r2, Point a, Point b) {
  return fmin(a.x, b.x) <= fmax(r1.x, r2.x) && fmax(a.x, b.x) >= fmin(r1.x, r2.x) &&
         fmin(a.y, b.y) <= fmax(r1.y, r2.y) && fmax(a.y, b.y) >= fmin(r1.y, r2.y);
}

bool insideOfRect(Point r1, Point r2, Point inter ) {
  return fmin(r1.x, r2.x) <= inter.x && inter.x <= fmax(r1.x, r2.x) &&
         fmin(r1.y, r2.y) <= inter.y && inter.y <= fmax(r1.y, r2.y);
} 

/**
 * Representation of line AB == a1x + b1y = c1;
 * Representation of line CD == a2x + b2y = c2;
*/
bool intersectionOfTwoLines(Point r1, Point r2, Point a, Point b, Point * intersection) {

  //double inter_x, inter_y;

  //Create a line from A(rx1, ry1) and B(rx2, ry2)
  double a1 = r2.y - r1.y;  
  double b1 = r1.x - r2.x;
  double c1 = a1 * r1.x + b1 * r1.y;

  //Create a line from C(*ax, *ay) and D(*bx, *by)
  double a2 = b.y - a.y;
  double b2 = a.x - b.x;
  double c2 = a2 * a.x + b2 * (a.y);  

  double det = a1 * b2 - a2 * b1;

  if(det == 0) {
    //printf("Lines are parallel\n");
    intersection->x = -1;
    intersection->y = -1;
    return CHUJEVO;
  } else {
    intersection->x = (b2 * c1 - b1 * c2) / det;
    intersection->y = (a1 * c2 - a2 * c1) / det;
    printf("INTERSECTION : %lf ---- %lf\n", intersection->x, intersection->y);
    return ZAJEBIS;
  }
    // if( insideOfRect(rx1, ry1, rx2, ry2, *ax, *ay) && insideOfRect(rx1, ry1, rx2, ry2, *bx, *by) ) {
    //     //printf("%lf ---- %lf\n", *ax, *ay);
    //     return ZAJEBIS;
    // } 

    //if( insideOfRect(rx1, ry1, rx2, ry2, inter_x, inter_y) ) {  
    //  //printf("x : %lf ------ y : %lf\n", inter_x, inter_y);
    //    int a_set = 0;
    //    int inter_x_first = 0;
    //    int inter_y_first = 0;
//
    //    if(a_set == 0) {
    //      inter_x_first = inter_x;
    //      inter_y_first = inter_y;
    //      a_set = 1;
    //    } else {
    //      *ax = (inter_x < inter_x_first ? inter_x : inter_x_first);
    //      *ay = (inter_x < inter_x_first ? inter_y : inter_y_first);
    //      *bx = (inter_x < inter_x_first ? inter_x_first : inter_x);
    //      *by = (inter_x < inter_x_first ? inter_y_first : inter_y);
    //      a_set = 0;        
    //    }
//
    //    printf("%lf ---- %lf\nB:%lf ---- %lf\n", *ax, *ay, *bx, *by);
//
    //    //printf("%lf ---- %lf\n", *ax, *ay);
    //}
}



int                clipLine                                ( double            rx1,
                                                             double            ry1,
                                                             double            rx2,
                                                             double            ry2,
                                                             double          * ax,
                                                             double          * ay,
                                                             double          * bx,
                                                             double          * by )
{
  Point r1 = {rx1, ry1};
  Point r2 = {rx2, ry1};
  Point r3 = {rx1, ry2};
  Point r4 = {rx2, ry2};

  Point * a = {ax, ay};
  Point * b = {bx, by};

  Point * inter;
  Point prev_inter = {-1, -1};

  
  if( crossed(r1, r2, *a, *b)) {
  printf("Is crossed\n");

    if(intersectionOfTwoLines(r1, r3, *a, *b, inter) ) {
      // prev_inter.x = inter->x;
      // prev_inter.y = inter->y;
      if( insideOfRect(r1, r4, *inter) ) {
        *ax = inter->x;
        *ay = inter->y;
        printf("%lf ------ %lf\n", *ax, *ay);
      }
    }

    if(intersectionOfTwoLines(r3, r4, *a, *b, inter)) {
      if( insideOfRect(r1, r4, *inter) ) {
        *ax = inter->x;
        *ay = inter->y;
        printf("%lf ------ %lf\n", *ax, *ay);

      }
    }
    
    if( intersectionOfTwoLines(r2, r4, *a, *b, inter) ) {
      if( insideOfRect(r1, r4, *inter) ) {
        *ax = inter->x;
        *ay = inter->y;
        printf("%lf ------ %lf\n", *ax, *ay);

      }
    }

    if( intersectionOfTwoLines(r1, r3, *a, *b, inter) ) {
      if( insideOfRect(r1, r4, *inter) ) {
        *ax = inter->x;
        *ay = inter->y;
        printf("%lf ------ %lf\n", *ax, *ay);

      }
    }
    
    return ZAJEBIS;
  } 

  printf("Is NOT crossed\n");
  return CHUJEVO;

}




#ifndef __PROGTEST__
int                almostEqual                             ( double            x,
                                                             double            y )
{
  
  
}

int                main                                    ( void )
{
  double x1, y1, x2, y2;

  x1 = 60;
  y1 = 40;
  x2 = 70;
  y2 = 50;

  clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 );

  // assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
  //          && almostEqual ( x1, 60 )
  //          && almostEqual ( y1, 40 )
  //          && almostEqual ( x2, 70 )
  //          && almostEqual ( y2, 50 ) );

  // x1 = 0;
  // y1 = 50;
  // x2 = 20;
  // y2 = 30;
  // assert ( clipLine ( 90, 100, 10, 20, &x1, &y1, &x2, &y2 )
  //          && almostEqual ( x1, 10 )
  //          && almostEqual ( y1, 40 )
  //          && almostEqual ( x2, 20 )
  //          && almostEqual ( y2, 30 ) );

  // x1 = 0;
  // y1 = 30;
  // x2 = 120;
  // y2 = 150;
  // assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
  //          && almostEqual ( x1, 10 )
  //          && almostEqual ( y1, 40 )
  //          && almostEqual ( x2, 70 )
  //          && almostEqual ( y2, 100 ) );

  // x1 = -10;
  // y1 = -10;
  // x2 = -20;
  // y2 = -20;
  // assert ( ! clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 ) );

  // x1 = 0;
  // y1 = 30;
  // x2 = 20;
  // y2 = 10;
  // assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
  //          && almostEqual ( x1, 10 )
  //          && almostEqual ( y1, 20 )
  //          && almostEqual ( x2, 10 )
  //          && almostEqual ( y2, 20 ) );

  // x1 = 0;
  // y1 = 0.3553;
  // x2 = 10.45;
  // y2 = 0;
  // assert ( clipLine ( 0.95, 0.323, 1, 1, &x1, &y1, &x2, &y2 )
  //          && almostEqual ( x1, 0.95 )
  //          && almostEqual ( y1, 0.323 )
  //          && almostEqual ( x2, 0.95 )
  //          && almostEqual ( y2, 0.323 ) );

  return 0;
}
#endif /* __PROGTEST__ */