#ifndef __PROGTEST__
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#endif /* __PROGTEST__ */

#define ZAJEBIS 1
#define CHUJEVO 0

#include <stdbool.h>

typedef struct Point{
    double x;
    double y;
}Point;

bool lessOrEqual(double a, double b) {
    return (fabs(a - b) <= 1e-6 || a < b);
}

bool insideOfRect(Point r1, Point r2, Point inter ) {
    return lessOrEqual(r1.x, inter.x) && lessOrEqual(inter.x, r2.x) &&
           lessOrEqual(r1.y, inter.y) && lessOrEqual(inter.y, r2.y) ;
}
//   return (fabs(r1.x - inter.x) <= 1e-6 || r1.x < inter.x) &&
//          (fabs(inter.x - r2.x) <= 1e-6 || inter.x < r2.x) &&
//          (fabs(r1.y - inter.y) <= 1e-6 || r1.y < inter.y) &&
//          (fabs(inter.y - r2.y) <= 1e-6 || inter.y < r2.y) ; 
// }

bool crossed(Point r1, Point r2, Point a, Point b) {

    //return insideOfRect(r1, r2, a) || insideOfRect(r1, r2, b);

  return fmin(a.x, b.x) <= fmax(r1.x, r2.x) && fmax(a.x, b.x) >= fmin(r1.x, r2.x) &&
         fmin(a.y, b.y) <= fmax(r1.y, r2.y) && fmax(a.y, b.y) >= fmin(r1.y, r2.y);
}

void intersectionOfTwoLines(Point r1, Point r2, Point a, Point b, Point *intersection) {
  
  //Create a line from A(rx1, ry1) and B(rx2, ry2)
  double a1 = r2.y - r1.y;  
  double b1 = r1.x - r2.x;
  double c1 = a1 * r1.x + b1 * r1.y;

  //Create a line from C(*ax, *ay) and D(*bx, *by)
  double a2 = b.y - a.y;
  double b2 = a.x - b.x;
  double c2 = a2 * (a.x) + b2 * (a.y);  

  double det = a1 * b2 - a2 * b1;
  
  if(det == 0) {
    //printf("Lines are parallel\n");
    intersection->x = -100;
    intersection->y = -100;
  } else {
    intersection->x = (b2 * c1 - b1 * c2) / det;
    intersection->y = (a1 * c2 - a2 * c1) / det;
  }

}

Point choose(Point x, Point y, Point a, Point b, Point r1, Point r4) {
  Point inter = {-100, -100};
  intersectionOfTwoLines(x, y, a, b, &inter);

  if(!insideOfRect(r1, r4, inter)) {
      inter.x = -100;
      inter.y = -100;
  }
       
  return inter;
}

void swap(Point * a, Point * b) {
    Point tmp = *a;
    *a = *b;
    *b =  tmp;
}

int                clipLine                                ( double            rx1,
                                                             double            ry1,
                                                             double            rx2,
                                                             double            ry2,
                                                             double          * ax,
                                                             double          * ay,
                                                             double          * bx,
                                                             double          * by )
{
    
  if (rx1 > rx2){
      double tmp = rx1;
      rx1 = rx2;
      rx2 = tmp;
      tmp = ry1;
      ry1 = ry2;
      ry2 = tmp;
  }
  
  Point r1 = {rx1, ry1};
  Point r2 = {rx2, ry1};    
  Point r3 = {rx1, ry2};
  Point r4 = {rx2, ry2};

  if (ry1 > ry2){
      Point to_swap = r1;
      r1 = r3;
      r3 = to_swap;
      to_swap = r2;
      r2 = r4;
      r4 = to_swap;
  }

  Point a = {*ax, *ay};
  Point b = {*bx, *by};

  if(lessOrEqual(*bx, *ax)) {
    swap(&a, &b);
  }

  Point first_struct = {-100, -100};
  Point second_struct = {-100, -100}; 
  Point tmp;

  int counter = 0;

  bool a_set = false;
  bool b_set = false;

  if(!crossed(r1, r4, a, b)) {
    printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    printf("SECOND : %lf ----- %lf\n", *bx, *by);
    printf("----------------------------------\n");
    printf("NOPE\n");
    return CHUJEVO;
  }

    //counter == 1 if one point is inside
    //counter == 2 if two points are outside

  if(insideOfRect(r1, r4, a))
      a_set = true;
  if(insideOfRect(r1, r4, b)) {
      b_set = true;
      if(a_set) {
            printf("FIRST : %lf ----- %lf\n", *ax, *ay);
            printf("SECOND : %lf ----- %lf\n", *bx, *by);
            printf("----------------------------------\n");
          return ZAJEBIS;
      }
  }

  tmp = choose(r1, r2, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else/* if(counter == 1)*/ {
          counter++;
          second_struct = tmp;
      }
  }

  tmp = choose(r3, r4, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else/* if(counter == 1) */{
          counter++;
          second_struct = tmp;
      }
  }
  
  tmp = choose(r2, r4, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else /*if(counter == 1)*/ {
          counter++;
          second_struct = tmp;
      }
  }

  tmp = choose(r1, r3, a, b, r1, r4);
  if(tmp.x != -100 && tmp.y != -100) {
      if(counter == 0) {
          counter++;
          first_struct = tmp;
      } else/* if(counter == 1)*/ {
          counter++;
          second_struct = tmp;
      }
  }

  if (!lessOrEqual(first_struct.x, second_struct.x)){
      Point tmp = first_struct;
      first_struct = second_struct;
      second_struct = tmp;
  }

  if(counter == 1) {
    if(!a_set) {
        a = first_struct; 
    } else if(!b_set) {
        b = first_struct;
    }
    }else /*if(counter == 2) */{
      if (!a_set && !b_set){
          a = first_struct;
          b = second_struct;
      } else if (!a_set) a = (lessOrEqual(a.x, first_struct.x) && lessOrEqual(first_struct.x, b.x) ? first_struct : 
                      (lessOrEqual(first_struct.x, a.x) && lessOrEqual(b.x, first_struct.x) ? first_struct : second_struct) );
        else if (!b_set) b = (lessOrEqual(a.x, first_struct.x) && lessOrEqual(first_struct.x, b.x) ? first_struct : 
                      (lessOrEqual(first_struct.x, a.x) && lessOrEqual(b.x, first_struct.x) ? first_struct : second_struct) );
    }


    //   } else if (!a_set) a = (first_struct.x >= a.x && first_struct.x <= b.x ? first_struct : 
    //                   (first_struct.x <= a.x && first_struct.x >= b.x ? first_struct : second_struct) );
    //   else if (!b_set) b = (first_struct.x >= a.x && first_struct.x <= b.x ? first_struct : 
    //                   (first_struct.x <= a.x && first_struct.x >= b.x ? first_struct : second_struct) );


  if(a.x > b.x) {
    swap(&a, &b);
  }

   *ax = a.x;
   *ay = a.y;
   *bx = b.x;
   *by = b.y;

    printf("FIRST : %lf ----- %lf\n", *ax, *ay);
    printf("SECOND : %lf ----- %lf\n", *bx, *by);
    printf("--------------------------------------------\n");


  return ZAJEBIS;
}

#ifndef __PROGTEST__
int                almostEqual                             ( double            x,
                                                             double            y )
{

}

int                main                                    ( void )
{
  double x1, y1, x2, y2;
 

 /*
  x1 = 60;
  y1 = 40;
  x2 = 70;
  y2 = 50;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 60 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 70 )
           && almostEqual ( y2, 50 ) );

  x1 = 0;
  y1 = 50;
  x2 = 20;
  y2 = 30;
  assert ( clipLine ( 90, 100, 10, 20, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 20 )
           && almostEqual ( y2, 30 ) );

  x1 = 0;
  y1 = 30;
  x2 = 120;
  y2 = 150;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 40 )
           && almostEqual ( x2, 70 )
           && almostEqual ( y2, 100 ) );

  x1 = -10;
  y1 = -10;
  x2 = -20;
  y2 = -20;
  assert ( ! clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 ) );

  x1 = 0;
  y1 = 30;
  x2 = 20;
  y2 = 10;
  assert ( clipLine ( 10, 20, 90, 100, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 10 )
           && almostEqual ( y1, 20 )
           && almostEqual ( x2, 10 )
           && almostEqual ( y2, 20 ) );

  x1 = 0;
  y1 = 0.3553;
  x2 = 10.45;
  y2 = 0;
  assert ( clipLine ( 0.95, 0.323, 1, 1, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 0.95 )
           && almostEqual ( y1, 0.323 )
           && almostEqual ( x2, 0.95 )
           && almostEqual ( y2, 0.323 ) );      */


/*
  x1 = 0;
  y1 = 0.8778;
  x2 = 8.613;
  y2 = 0;
  assert ( clipLine ( 0.783, 0.798, 1, 1, &x1, &y1, &x2, &y2 )
           && almostEqual ( x1, 0.783 )
           && almostEqual ( y1, 0.798 )
           && almostEqual ( x2, 0.783 )
           && almostEqual ( y2, 0.798 ) );

 
   x1 = 1578;
  y1 = 2373;
  x2 = 2070;
  y2 = 3293;
  assert (!clipLine(1086, 1453, 594, 533, &x1, &y1, &x2, &y2));


x1 = -967; y1 = -739; x2 = 253; y2 = -227.5;
assert (clipLine ( -357, -367, -52, -181, &x1, &y1, &x2, &y2)
          && almostEqual(x1, -79.7273)
          && almostEqual(y1, -367)
          && almostEqual(x2, -52)
          && almostEqual(y2, -355.375));


x1 = -1908; y1 = -1034; x2 = 813; y2 = -300;
assert (clipLine ( -94, -667, 813, -300, &x1, &y1, &x2, &y2)
          && almostEqual(x1, -94)
          && almostEqual(y1, -544.667)
          && almostEqual(x2, 813)
          && almostEqual(y2, -300));


x1 = -900;
y1 = -410.25;
x2 = 466;
y2 = -616.75;
assert(clipLine ( 466, -720, 1149, -307,  &x1, &y1, &x2, &y2)
      && almostEqual(x1, 466)
      && almostEqual(y1, -616.75)
      && almostEqual(x2, 466)
      && almostEqual(y2, -616.75));
                                          */

  x1 = 355; 
  y1 = 1452;
  x2 = 63;
  y2 = 670; 
  assert(clipLine ( 63, 670, -83, 279, &x1, &y1, &x2, &y2)
        && almostEqual(x1, 63) 
        && almostEqual(y1, 670) 
        && almostEqual(x2, 63) 
        && almostEqual(y2, 670));
/*
  x1 = -1072;
  y1 = -1746;
  x2 = -545.5;
  y2 = -328.5;
  assert(clipLine(-604, -486, -370, 144, &x1, &y1, &x2, &y2) && 
        almostEqual(x1, -604) && 
        almostEqual(y1, -486) && 
        almostEqual(x2, -545.5) && 
        almostEqual(y2, -328.5));
  
  x1 = -450;
  y1 = -386.25;
  x2 = -904;
  y2 = -978;
  assert(clipLine(-450, -452, -223, -189, &x1, &y1, &x2, &y2) && 
        almostEqual(x1, -450) && 
        almostEqual(y1, -386.25) && 
        almostEqual(x2, -450) && 
        almostEqual(y2, -386.25));

   x1 = -2412;
  y1 = -2721;
  x2 = -654;
  y2 = -885;
  assert(clipLine ( -654, 33, 225, -885, &x1, &y1, &x2, &y2 ) &&
        almostEqual(x1, -654) &&
        almostEqual(y1, -885) && 
        almostEqual(x2, -654) && 
        almostEqual(y2, -885));


x1 = -1072; y1 = -1746; x2 = -545.5; y2 = -328.5; 
assert (clipLine(-604, -486, -370, 144, &x1, &y1, &x2, &y2) &&
        almostEqual(x1, -604) && 
        almostEqual(y1, -486) && 
        almostEqual(x2, -545.5) && 
        almostEqual(y2, -328.5));

x1 = -450; y1 = -386.25; x2 = -904; y2 = -978; 
assert (clipLine(-450, -452, -223, -189, &x1, &y1, &x2, &y2) && 
        almostEqual(x1, -450) &&
        almostEqual(y1, -386.25) && 
        almostEqual(x2, -450) && 
        almostEqual(y2, -386.25));

x1 = 11; y1 = 1316.75; x2 = 11; y2 = 1012.25; 
assert (clipLine(11, 1469, -830, 860, &x1, &y1, &x2, &y2) && 
        almostEqual(x1, x1) && 
        almostEqual(y1, y1) && 
        almostEqual(x2, x2) && 
        almostEqual(y2, y2));
                                  */
// x1 = 2500; y1 = -96; x2 = 1138; y2 = 290; 
// assert (!clipLine(457, -289, 1138, -96, &x1, &y1, &x2, &y2) && 
//         almostEqual(x1, 2500) && 
//         almostEqual(y1, -96) && 
//         almostEqual(x2, 1138) && 
//         almostEqual(y2, 290));

// x1 = -149; y1 = -6; x2 = 691; y2 = 925.5; assert (clipLine(691, 822, 1111, 1236, &x1, &y1, &x2, &y2) && almostEqual(x1, 691) && almostEqual(y1, 925.5) && almostEqual(x2, 691) && almostEqual(y2, 925.5));

// x1 = 1096; y1 = 261; x2 = 1594; y2 = -139; assert (!clipLine(1096, -139, 847, -339, &x1, &y1, &x2, &y2) && almostEqual(x1, 1096) && almostEqual(y1, 261) && almostEqual(x2, 1594) && almostEqual(y2, -139));

// x1 = -2412; y1 = -2721; x2 = -654; y2 = -885; assert (clipLine(-654, 33, 225, -885, &x1, &y1, &x2, &y2) && almostEqual(x1, -654) && almostEqual(y1, -885) && almostEqual(x2, -654) && almostEqual(y2, -885));

// x1 = 0; y1 = 0.8448; x2 = 3.685; y2 = 0; assert (clipLine(0.335, 0.768, 1, 1, &x1, &y1, &x2, &y2) && almostEqual(x1,0.335) && almostEqual(y1,0.768) && almostEqual(x2,0.335) && almostEqual(y2,0.768));

// x1 = 0; y1 = 0.6083; x2 = 3.047; y2 = 0; assert (clipLine ( 0.277, 0.553, 1, 1, &x1, &y1, &x2, &y2) && almostEqual(x1,0.277) && almostEqual(y1,0.553) && almostEqual(x2,0.277) && almostEqual(y2,0.553));

// x1 = 755; y1 = 774; x2 = -1143; y2 = 774; assert (clipLine(755, 774, 1704, 1492, &x1, &y1, &x2, &y2) && almostEqual(x1, 755) && almostEqual(y1, 774) && almostEqual(x2, 755) && almostEqual(y2, 774));

// x1 = -1115; y1 = 145; x2 = -1627; y2 = 145; assert (!clipLine ( -603, 145, -91, 690, &x1, &y1, &x2, &y2) && almostEqual(x1, -1115) && almostEqual(y1, 145) && almostEqual(x2, -1627) && almostEqual(y2, 145));




  return 0;
}
#endif /* __PROGTEST__ */