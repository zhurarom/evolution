#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

// long long int makeReverse(long long number, long long int bits_of_half) {
//     long long int reverse = 0;
//     for(int i = 0; i < bits_of_half; i++) {
//         reverse <<= 1;
//         if( (number & 1) == 1) {
//             reverse ^= 1;
//         }
//         number >>= 1;
//     }
//     return reverse;
// }


long long int makeReverse(long long int number, long long int bits) {
    long long int reverse = 0;
    long long int tmp = number;
    while(tmp > 0) {
        reverse = reverse * 2 + tmp % 2;
        tmp /= 2;
    }
    reverse *= pow(2, bits - (long long int)log2(number) - 1);
    return reverse;
}


long long int countOfNBitsPalindroms(long long int number) {
    long long int number_of_bits = (long long int)log2(number) + 1;
    long long int half_first = 0;
    long long int half_second = 0;
    long long int reverse_half_first;
    long long int mask;
    long long int diff = 0;

    if(number_of_bits % 2 == 0) {
        half_first = number;
        half_first >>= number_of_bits / 2;
        half_first -= pow(2, number_of_bits / 2 - 1);

        half_second = number;

        mask = pow(2, number_of_bits / 2) - 1;
        half_second &= mask;

        half_second >>= 1;

        reverse_half_first = makeReverse(half_first, number_of_bits / 2 - 1);
        diff = reverse_half_first - half_second;
    } else {
        half_first = number;
        half_first >>= (number_of_bits - 1) / 2;
        half_first -= pow(2, (number_of_bits - 1) / 2);

        half_second = number;

        mask = pow(2, (number_of_bits + 1) / 2) - 1;
        half_second &= mask;
        half_second >>= 1;

        reverse_half_first = makeReverse(half_first, (number_of_bits - 1) / 2);
        diff = reverse_half_first - half_second;
    }

    

    //printf("DIFF: %lld ------- %lld ------ %lld--------%lld\n", diff, reverse_half_first, half_first, half_second);


    if(diff < 0) {
        //printf("1.N-th palindrom: %lld\n", half_first + 1);
        return half_first + 1;
    } else if(diff > 0) {
        //printf("2.N-th palindrom: %lld\n", half_first);
        return half_first;
    } else {
        if(number % 2 == 0) {
            //printf("3.N-th palindrom: %lld\n", half_first);
            return half_first;
        }else {
            //printf("4.N-th palindrom: %lld\n", half_first + 1);
            return half_first + 1;   
        }
    }

}

long long int countOfPalindroms(long long int number) {
    if (number == 0){
        return 1;
    }

    long long int palindroms = 0;
    long long int number_of_bits = (long long int)log2(number) + 1;

    if(number_of_bits % 2 == 0) {
        palindroms = pow(2, (number_of_bits + 2) / 2) - pow(2, (number_of_bits - 2) / 2) - 1;
    } else {
        palindroms = pow(2, (number_of_bits + 1) / 2) - 1;
    }

    //printf("PAL(n - 1): %lld\n", palindroms);

    return (palindroms + countOfNBitsPalindroms(number));

}

long long int makeReverseForPrint(long long int number) {
    long long int reverse = 0;
    while(number > 0) {
        reverse <<= 1;
        if( (number & 1) == 1) {
            reverse ^= 1;
        }
        number >>= 1;
    }
    return reverse;
}

bool findPalindrom(long long int number) {
    long long int reverse = makeReverseForPrint(number);
    return (number == reverse);
}

void printPalindrom(long long lo) {
    if(lo > 1)
        printPalindrom(lo >> 1);

    printf("%lld", lo & 1);
}

int main() {
    long long int lo = 0, hi = 0;
    char start;
    int err;

    printf("Vstupni intervaly:\n");
    while( (err = scanf(" %c", &start)) == 1 ) {

        if( scanf(" %lld %lld", &lo, &hi) != 2 ||  (start != 'c' && start != 'l')  
                                                      || lo < 0 || hi < lo ) {
            printf("Nespravny vstup.\n");
            return 0;
        }

        if( start == 'l' ) {
            for( ; lo <= hi; lo++) {
                if (findPalindrom(lo)) {
                    printf("%lld = ", lo);
                    printPalindrom(lo);
                    printf("b\n");
                }
            }
        } else if(start == 'c') {

            long long int for_lo = countOfPalindroms(lo);
            long long int for_hi = countOfPalindroms(hi);

            if(lo == 0 || lo == makeReverse(lo, (long long int)log2(lo) + 1)) {
                printf("Celkem: %lld\n", for_hi - for_lo + 1);
            }else {
                printf("Celkem: %lld\n", for_hi - for_lo);
            }
            
        } 
    }

    if( err != EOF ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    return 0;
}