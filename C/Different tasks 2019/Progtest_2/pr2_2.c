#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

long long int makeReverse(long long int number, long long int bits, long long int r) {
    
    if(number == 0) {
        return 0;
    }
    
    long long int reverse = 0;
    long long int tmp = number;
    while(tmp > 0) {
        reverse = reverse * r + tmp % r;
        tmp /= r;
    }
    reverse *= pow(r, bits - (long long int)(log2l(number) / log2l(r))  - 1);
    return reverse;
}


long long int countOfNBitsPalindroms(long long int number, long long int r) {
    long long int number_of_bits = (long long int)(log2l(number) / log2l(r)) + 1;
    long long int half_first = number;
    long long int half_second = number;
    long long int reverse_half_first;
    long long int diff = 0;
    long long int first_bit = number;
    long long int last_bit = number % r;


    while(first_bit >= r) {
        first_bit /= r;
    }
    

    if(number_of_bits % 2 == 0) {
        //>>=
        for(long long int i = 0; i < number_of_bits / 2; i++) {
            half_first /= r;
        }

        half_first -= pow(r, number_of_bits / 2 - 1) * first_bit;
        half_second %= (long long int)pow(r, number_of_bits / 2);
        half_second /= r;

        reverse_half_first = makeReverse(half_first, number_of_bits / 2 - 1, r);
    } else {

        for (long long int i = 0; i < (number_of_bits - 1) / 2; i++) {   
            half_first /= r;
        }        

        half_first -= pow(r, (number_of_bits - 1) / 2) * first_bit;
        half_second %= (long long int)pow(r, (number_of_bits - 1) / 2 + 1);
        half_second /= r;

        reverse_half_first = makeReverse(half_first, (number_of_bits - 1) / 2, r );
    }

    diff = reverse_half_first - half_second;
    

    //printf("DIFF: %lld ------- %lld ------ %lld--------%lld\n", diff, reverse_half_first, half_first, half_second);

    long long int nikita;

    if(half_first == 0 && first_bit == 1 && first_bit < last_bit) {
        nikita = (long long int)(pow(r, (number_of_bits + 1) / 2 - 1)) * first_bit;
    } else {
        nikita = (long long int)(pow(r, (number_of_bits + 1) / 2 - 1)) * (first_bit - 1);
    }

    //printf("NIKITA: %lld\n", nikita);

    if(diff < 0) {
        //printf("1.N-th palindrom: %lld\n", half_first + nikita + 1);
        return half_first + nikita + 1;
    } else if(diff > 0) {
        //printf("2.N-th palindrom: %lld\n", half_first + nikita);
        return half_first + nikita;
    } else {
        if(first_bit != last_bit) {
            //printf("3.N-th palindrom: %lld\n", half_first + nikita);
            return half_first + nikita;
        }else {
            //printf("4.N-th palindrom: %lld\n", half_first + nikita + 1);
            return half_first + nikita + 1;   
        }
    }

}

long long int countOfPalindroms(long long int number, long long int r) {
    if (number == 0){
        return 1;
    }

    long long int palindroms = 0;
    long long int number_of_bits = (long long int)(log2l(number) / log2l(r)) + 1;

    if(number_of_bits % 2 == 0) {
        palindroms = pow(r, (number_of_bits - 2) / 2) + pow(r, number_of_bits / 2) - 1;
    } else {
        palindroms = 2 * pow(r, (number_of_bits - 1) / 2) - 1;
    }

    //printf("PAL(n - 1): %lld\n", palindroms);

    return (palindroms + countOfNBitsPalindroms(number, r));

}

long long int makeReverseForPrint(long long int number, long long int r) {
    long long int reverse = 0;
    while(number > 0) {
        reverse = reverse * r + number % r;
        number = number / r;
    }
    return reverse;
}

bool findPalindromForPrint(long long int number, long long int r) {
    long long int reverse = makeReverseForPrint(number, r);
    return (number == reverse);
}

void printPalindrom(long long int lo, long long int r) {

    char alphabet[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                         'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                         'w', 'x', 'y', 'z'};

    if(lo == 0) {
        printf("0");
    }

    while(lo != 0) {
        long long int pos = lo % r;        
        if(lo % r != 0) {

            if(pos >= 10) {
                printf("%c", alphabet[pos - 10]);
            } else {
                printf("%lld", lo % r);
            }

            lo = lo / r;
        } else {
            if(pos >= 10) {
                printf("%c", alphabet[pos - 10]);
            } else {
                 printf("%lld", lo % r);
            }
            lo = lo / r;
        }
    }
}


int main() {
    long long int lo = 0, hi = 0;
    char start;
    int err;
    long long int r;

    printf("Vstupni intervaly:\n");
    while( (err = scanf(" %c", &start)) == 1 ) {

        if( scanf(" %lld %lld %lld", &r, &lo, &hi) != 3 ||  (start != 'c' && start != 'l')  
                                                 || lo < 0 || hi < lo 
                                                 || r < 2 || r > 36 ) {
            printf("Nespravny vstup.\n");
            return 0;
        }

        if( start == 'l' ) {
            for( ; lo <= hi; lo++) {
                if (findPalindromForPrint(lo, r)) {
                    printf("%lld = ", lo);
                    printPalindrom(lo, r);
                    printf(" (%lld)\n", r);
                }
            }
        } else if(start == 'c') {

                long long int for_lo = countOfPalindroms(lo, r);
                long long int for_hi = countOfPalindroms(hi, r);

                if(lo == 0 || lo == makeReverse(lo, (long long int)(log2l(lo) / log2l(r)) + 1, r) ){
                    printf("Celkem: %lld\n", for_hi - for_lo + 1);
                } else {
                    printf("Celkem: %lld\n", for_hi - for_lo);
                }
        }

    }

    if( err != EOF ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    return 0;
}