#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define MAX_COUNT 100000
#define CHUJEVO 0
#define ZAJEBIS 1


typedef struct Flight {
    int x;
    int y;
    char *name;
}Flight;


void freeAllFlights(AllFlights * allflights) {
    for(int i = 0; i < allflights->index; i++) {
        free(allflights->flights);
    }

    allflights->index = 0;
}

//[0,0] KLM KL1981

int readPosition(AllFlights * all_flights) {
    char musor[100];
    int x = all_flights->flights->x;
    int y = all_flights->flights->y;

    if(scanf(" %[[] %d %[,] %d %[]]", musor, &x, musor, &y, musor) != 5 ) {
        printf("Nespavny vstup.\n");
        freeAllFlights(all_flights);
        return CHUJEVO;
    }
   
    return ZAJEBIS;
}

int readString(AllFlights * all_flights) {

    char *name = all_flights->flights->name;


    if( scanf(" %s ", name) != 1 ) {
        printf("Nespravny vstup.\n");
        freeAllFlights(all_flights);
        return CHUJEVO;
    }

    if(!feof(stdin)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO;
    }

    return ZAJEBIS;
}


int main() {
    AllFlights all_flights;

    printf("Zadejte lety:\n");

    if(!readPosition(&all_flights)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO;
    }

    if(!readString(&all_flights)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO; 
    }

    freeAllFlights(&all_flights);
    return ZAJEBIS;
}