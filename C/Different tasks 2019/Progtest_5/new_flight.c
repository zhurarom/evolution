/**
 *  The task is to create a program that will help the control tower radar operator.
 *
 *  When controlling air traffic, it is important to monitor for potential aircraft collisions. 
 *  Radars detect aircraft positions and the program checks the distances between aircraft. 
 *  We want to implement a program that decides where aircraft coordinates are likely to collide,
 *  which aircraft are closest to each other.
 * 
 * 
 * created by Roman Zhuravskyi
*/

#include <stdio.h>
#include <stdlib.h> /*malloc, free*/
#include <string.h> /*stlen*/
#include <limits.h> /*DBL_EPSILON*/
#include <math.h>

#define CHUJEVO 0
#define ZAJEBIS 1

/*----------------------------------------------------------------------*/
/**
 * The struct which represent flights.
 * We have coordinates of plane and name. 
 * 
*/
typedef struct Flight{
    double x;
    double y;
    char *name;
}Flight;

/*----------------------------------------------------------------------*/
/**
 * The struct which represents flights with min distances.
 * We have indices of planes and distence between this planes. 
 * 
*/
typedef struct Distance{
    int first_index;
    int second_index;
    double distance;
}Distance;


/*----------------------------------------------------------------------*/
/** The function loads the input data into a dynamically allocated array.
 *  @param[in] flights - array with flights
 *  @param[in, out] count - counter of flights
 *  @param[in, out] size - size for allocation of array of flights
 *  @param[out] ZAJEBIS(1) or CHUJEVO(0)
 *  @return ZAJEBIS(1) or CHUJEVO(0)
 *  @note we made allocation for Flight * flights, flights[*count].name and for line
 * 
*/
int readInput(Flight ** flights, int * count, int * size) {
    char first = '[', second = ',', third = ']';
    char *line;
    char *name_of_flight = NULL;
    int len_of_name;
    size_t len;

    /*Checking for EOF*/
    while( !feof(stdin) ) {
        line = NULL;
        len = 0;        
        len_of_name = 50;
        
        /*Reallocation of array of flights: if we don`t have enough memory,
          made reallocation for 10, if array is big - make it bigger in 1.5*/
        if(*count >= *size) {
            *size += (*size < 100) ? 10 : *size / 2;
            *flights = (Flight*) realloc (*flights, (*size) * sizeof(Flight));
        }
        

        /*Allocation for names of flights for 100 chars*/
        name_of_flight = (char*)calloc(len_of_name, sizeof(char));
        (*flights)[*count].name = (char*)calloc(len_of_name, sizeof(char));

        printf("BEFOR: %ld\n", len);


        if(getline(&line, &len, stdin) == -1)
            break;

        printf("AFTER: %ld\n", len);

        int lenght_of_line = strlen(line);


        /*Reallocation of names of flights if we have more than 100 chars*/
        if (len_of_name <= lenght_of_line)
        {
            len_of_name += lenght_of_line;
            name_of_flight = (char*)realloc(name_of_flight, len_of_name * sizeof(char));
            (*flights)[*count].name = (char *) realloc ((*flights)[*count].name, len_of_name * sizeof (char));
        }  

        /*Read a string*/
        if (sscanf ( line, " %c %lf %c %lf %c %[^\n]s ", 
                       &first, &(*flights)[*count].x, &second, &(*flights)[*count].y, &third, name_of_flight) != 6) {
            break;
        }

        /*Checking for right format*/
        if(first != '[' || second != ',' || third != ']') {
            break;
        }

        strcpy((*flights)[*count].name, name_of_flight);

        (*count)++;
        free(name_of_flight);
        free(line);
    }

    /*If we have less than 2 flights, return CHUJEVO(0)*/
    if(*count < 2) {
        free(name_of_flight);
        free(line);
        return CHUJEVO;
    }

    free(name_of_flight);    
    free(line);
    return ZAJEBIS;
}

/*----------------------------------------------------------------------*/
/** The function will free struct of flights
 *  @param[in] flights - array with flights
 *  @param[in] count_of_elements - counter of flights
 *  @note the function don`t have return, just make free
*/
void freeFlights(Flight *flights, int count_of_elements) {
    for(int i = 0; i <= count_of_elements; i++)
        free(flights[i].name);
    
    free(flights);
}

/*----------------------------------------------------------------------*/
/** The function will print struct of the nearest flights
 *  @param[in] flights - array with flights
 *  @param[in] count_of_elements - counter of flights
 *  @param[in] distances - array of arrays which remember min distances between flights 
 *  @param[in] count_for_j - counter for inside array
 *  @note the function don`t have return, just make print
*/
void printAnswer(Flight *flights, int count_of_elements, Distance ** distances, int * count_for_j) {
    for(int i = 0; i < count_of_elements; i++) {
        for(int j = 0; j < count_for_j[i]; j++) {
            printf("%s - %s\n", flights[distances[i][j].first_index].name, flights[distances[i][j].second_index].name);
        }
    }
}

/*----------------------------------------------------------------------*/
/** The function loads the input data into a dynamically allocated array.
 *  @param[in] flights - array with flights
 *  @param[in] count_of_elements - counter of flights
 *  @param[in] distances - array of arrays which remember min distances between flights  
 *  @param[in, out] counters_for_j - counter for inside array
 *  @return tmp.distance - distances between flights
 *  @note fill array distances with min distances and indices of flights
 * 
*/
double findDistance(Flight *flights, int count_of_elements, Distance ** distances, int * counters_for_j) {
    Distance tmp;
    tmp.distance = 100000000.0; /*set up min*/

    /*checking for all flights*/
    for(int i = 0; i < count_of_elements; i++) {

        distances[i] = (Distance*)malloc(count_of_elements * sizeof(Distance));

        for(int j = i + 1; j < count_of_elements; j++) {
            /*math formula for finding the distance between two points*/
            double lol = sqrt( (flights[i].x - flights[j].x) * (flights[i].x - flights[j].x)
                             + (flights[i].y - flights[j].y) * (flights[i].y - flights[j].y) );

            /*looking for the nearest distance and save it with position of flights*/
            if(lol < tmp.distance) {
                tmp.distance = lol;
                tmp.first_index = i;
                tmp.second_index = j;
            }
        }
    }
    
    /*The same code BUT...*/
    for(int i = 0; i < count_of_elements; i++) {
        int counter = 0;

        for(int j = i + 1; j < count_of_elements; j++) {
            double lol = sqrt( (flights[i].x - flights[j].x)*(flights[i].x - flights[j].x)
                             + (flights[i].y - flights[j].y)*(flights[i].y - flights[j].y) );

            /*looking for min distance between min distances and save it with position of flights*/
            if(fabs(lol - tmp.distance) <= __DBL_EPSILON__ * 1000 * fmax(lol, tmp.distance)) {
                distances[i][counter].distance = tmp.distance;
                distances[i][counter].first_index = i;
                distances[i][counter].second_index = j;
                counter++;
            }
        }
        /*counter for flights with min distances*/
        counters_for_j[i] = counter;
    }

    return tmp.distance;
}


int main() {
    Flight *flights = NULL;

    int count_of_elements = 0;
    int size_of_flights = 100;

    flights = (Flight *)malloc(size_of_flights * sizeof(Flight));

    printf("Zadejte lety:\n");

    if(!readInput(&flights, &count_of_elements, &size_of_flights)) {
        printf("Nespravny vstup.\n");
        freeFlights(flights, count_of_elements);
        return 1;
    }

    Distance ** distances = NULL;
    int * counters_for_j;
    double min_distance;

    distances = (Distance **)malloc(count_of_elements * sizeof(Distance*));
    counters_for_j = (int *)malloc(count_of_elements * sizeof(int)); 

    min_distance = findDistance(flights, count_of_elements, distances, counters_for_j);

    printf("Nejmensi vzdalenost: %lf\n", min_distance);
    printAnswer(flights, count_of_elements, distances, counters_for_j);

    freeFlights(flights, count_of_elements);

    for(int i = 0; i < count_of_elements; i++) 
        free(distances[i]);
    free(distances);
  
    free(counters_for_j);
    return 0;
}
