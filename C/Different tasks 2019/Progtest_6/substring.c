#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ZAJEBIS 1
#define CHUJEVO 0

typedef struct string {
    char * line;
    char * line_low;
    double counter_of_fr;
}STRING;

void freeOfString(STRING * r_string, int counter_of_strings) {
    for(int i = 0; i <= counter_of_strings; i++) {
        free(r_string[i].line);
        free(r_string[i].line_low);
    }
    free(r_string);
}


STRING * readInput(int * counter_of_strings) {
    STRING * r_string = NULL;
    int allocation_size = 0;
    char *help_string;
    int lenght = 100;
    char *line;
    char sign = ':';
    size_t len;

    int counter = 0;
    int err = 0;
    int s;
    double p;

    *counter_of_strings = 0;


    while( 1 ) {

        line = NULL;
        len = 0;
        help_string = NULL;
        lenght = 100;

        if(*counter_of_strings >= allocation_size) {
            allocation_size += (allocation_size < 100) ? 10 : allocation_size / 2;
            r_string = (STRING*)realloc(r_string, allocation_size * sizeof(STRING)); 
        }

        help_string = (char*)calloc(lenght, sizeof(char));
        r_string[*counter_of_strings].line = (char*)calloc(lenght, sizeof(char));
        r_string[*counter_of_strings].line_low = (char*)calloc(lenght, sizeof(char));


        if(getline(&line, &len, stdin) == -1) {
            err++;
            break;
        }

        int len_of_line = strlen(line);

        if(lenght <= len_of_line) {
            lenght += len_of_line;
            help_string = (char*)realloc(help_string, lenght * sizeof(char));
            r_string[*counter_of_strings].line = (char*)realloc(r_string[*counter_of_strings].line, lenght * sizeof(char));
            r_string[*counter_of_strings].line_low = (char*)realloc(r_string[*counter_of_strings].line_low, lenght * sizeof(char));            
        }

        // if(sscanf(line, " %lf %c %[^\n]s ", &r_string[*counter_of_strings].counter_of_fr, 
        //                                    &sign, help_string) != 3) {
        //     err++;
        //     break;
        // }

        s = sscanf(line, " %lf %c %[^\n]s ", &p, &sign, help_string);

        if(s == -1) {
            break;
        }

        if(sign != ':' || s != 3) {
            err++;
            break;
        }

        r_string[*counter_of_strings].counter_of_fr = p;
        strcpy(r_string[*counter_of_strings].line, help_string);
        strcpy(r_string[*counter_of_strings].line_low, help_string);

        free(help_string);
        free(line);

        (*counter_of_strings)++;
        counter++;
    }

    if(err != 0 || counter == 0) {
        freeOfString(r_string, *counter_of_strings);
        free(line);
        free(help_string);
        return NULL;
    }

    free(line);
    free(help_string);
    return r_string;

}

void to_Lower(char *r_string) {
    int lenght = strlen(r_string) + 1;
    for(int i = 0; i < lenght; i++) {
        if(r_string[i] >= 'A' && r_string[i] <= 'Z') {
            r_string[i] = r_string[i] + 32;
        }
    }
}

// void printString(STRING * r_string, int counter_of_strings) {
//     for(int i = 0; i < counter_of_strings; i++) {
//         printf("%lf : %s\n", r_string[i].counter_of_fr, r_string[i].line_low);
//     }
// }

int find_substring(STRING * r_string, int counter_of_strings) {
    char * substring;
    int length;
    size_t len;
    char * line;

    int frequency[50];
    int counter_of_frequency;

    int err = 0;

    while( 1 ) {
        substring = NULL;
        length = 100;
        len = 0;
        line = NULL;

        substring = (char *)calloc(length, sizeof(char));

        if( getline(&line, &len, stdin) == -1 ) {
            break;
        }

        int length_of_line = strlen(line) + 1;

        if(length <= length_of_line) {
            length += length_of_line;
            substring = (char*) realloc(substring, length * sizeof(char));
        }

        if( sscanf(line, " %[^\n]s ", substring) != 1) {
            break;
        }

        to_Lower(substring);

        //Fill with zeros
        for(int i = 0; i < 50; i++) {
            frequency[i] = 0;
        }

        //Refresh counter
        counter_of_frequency = 0;

        char *pointer = NULL;

        for(int i = 0; i < counter_of_strings; i++) {
            if(counter_of_frequency == 50) {
                break;
            }
            
            pointer = strstr(r_string[i].line_low, substring);

            if(pointer != NULL) {
                frequency[counter_of_frequency] = i;
                counter_of_frequency++;
            }
        }

        printf("Nalezeno: %d\n", counter_of_frequency);


        for(int i = 0; i < counter_of_frequency; i++) {
            printf("> %s\n", r_string[frequency[i]].line);
        }

        free(substring);
        free(line);
    }

    if(err != EOF) {
        free(line);
        free(substring);
        return 1;
    }

    free(line);
    free(substring);
    return 0;
}

int compare(STRING *a, STRING *b) {
    return (a->counter_of_fr < b->counter_of_fr) - (b->counter_of_fr < a->counter_of_fr);
}


int main() {
    STRING * r_string;
    int counter_of_strings;

    printf("Casto hledane fraze:\n");        

    r_string = readInput(&counter_of_strings);

    if( !r_string ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    printf("Hledani:\n");

    //Becuse of task here i should use qsort
    qsort(r_string, counter_of_strings, sizeof(STRING), (int(*)(const void *, const void *)) compare );

    for(int i = 0; i < counter_of_strings; i++) {
        to_Lower(r_string[i].line_low);
    }

    if( !find_substring(r_string, counter_of_strings) ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    freeOfString(r_string, counter_of_strings);

    // for(int i = 0; i <= counter_of_strings; i++) {
    //     free(r_string[i].line);
    //     free(r_string[i].line_low);
    // }
    // free(r_string);

    return 0;
}
