#include <stdio.h>
#include <stdlib.h>

int main() {
    int number = 0;
    if( scanf(" %d", &number) != 1 || number < 1 || number > 5 ) {
        printf("ml' nob:\n");
        printf("luj\n");
        return 0;
    }

    switch (number)
    {
        case 1:
            printf("ml' nob:\n");
            printf("Qapla'\n");
            printf("noH QapmeH wo' Qaw'lu'chugh yay chavbe'lu' 'ej wo' choqmeH may' DoHlu'chugh lujbe'lu'.\n");
            break;

        case 2:
            printf("ml' nob:\n");
            printf("Qapla'\n");
            printf("Qu' buSHa'chugh SuvwI', batlhHa' vangchugh, qoj matlhHa'chugh, pagh ghaH SuvwI''e'.\n");
            break;

        case 3:
            printf("ml' nob:\n");    
            printf("Qapla'\n");
            printf("qaStaHvIS wa' ram loS SaD Hugh SIjlaH qetbogh loD.\n");
            break;

        case 4:
            printf("ml' nob:\n");
            printf("Qapla'\n");
            printf("Ha'DIbaH DaSop 'e' DaHechbe'chugh yIHoHQo'.\n");
            break;

        case 5:
            printf("ml' nob:\n");
            printf("Qapla'\n");
            printf("leghlaHchu'be'chugh mIn lo'laHbe' taj jej.\n");
            break;
    }

    return 0;
}