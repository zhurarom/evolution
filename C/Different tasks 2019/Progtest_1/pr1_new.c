#include <stdio.h>
#include <math.h>
#include <float.h>

int main(void) {
    double a1, b1, c1, a2, b2, c2;
    double averange_1, averange_2, maximum_1, maximum_2, minimum_1, minimum_2; 
    char first_sign;
    char second_sign;
    char third_sign;
    double first_angle, second_angle;
    double first_first_angle, second_second_angle;
    char first_first_sign, second_second_sign, third_third_sign;

    printf("Trojuhelnik #1:\n");
    if( scanf(" %c", &first_sign) != 1 || (first_sign != 'S' && first_sign != 'U') ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if(first_sign == 'S') {
            if(scanf("%c%c", &second_sign, &third_sign) != 2 || (second_sign != 'S' && second_sign != 'U') ) {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if( third_sign == 'U') {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if(second_sign == 'S') {    //SSS
                if( scanf(" %lf %lf %lf", &a1, &b1, &c1) != 3 || a1 <= 0 || b1 <= 0 || c1 <= 0) {
                    printf("Nespravny vstup.\n");
                    return 1;
                }

                if( (a1 + b1) - c1 <= DBL_EPSILON * fmax(fabs(a1 + b1), fabs(c1)) ||
                    (a1 + c1) - b1 <= DBL_EPSILON * fmax(fabs(a1 + c1), fabs(b1)) ||
                    (c1 + b1) - a1 <= DBL_EPSILON * fmax(fabs(c1 + b1), fabs(a1)) ) {
                        printf("Vstup netvori trojuhelnik.\n");
                        return 0;
                }
            } else if(second_sign == 'U') { //SUS
                if( scanf(" %lf %lf %lf", &a1, &first_angle, &c1) != 3 || a1 <= 0 || first_angle <= 0 || first_angle >= 180 || c1 <= 0) {
                    printf("Nespravny vstup.\n");
                    return 1;
                }

            }

    } else if(first_sign == 'U') {

            if(scanf("%c%c", &second_sign, &third_sign) != 2 || second_sign != 'S' || third_sign != 'U') {
                printf("Nespravny vstup.\n");
                return 1;
            }

            //USU
            if( scanf(" %lf %lf %lf", &first_angle, &b1, &second_angle) != 3 || (first_angle <= 0 || first_angle >= 180)
                                                                             || (second_angle <= 0 || second_angle >= 180)
                                                                             || b1 <= 0  ) {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if( (first_angle >= 90 && second_angle >= 90) ||
                (second_angle >= 90 && first_angle >= 90) ) {
                    printf("Vstup netvori trojuhelnik.\n");
                    return 0;
            }

    }

    maximum_1 = fmax(a1 , b1);
    maximum_1 = fmax(maximum_1, c1);

    minimum_1 = fmin(a1, b1);
    minimum_1 = fmin(minimum_1, c1);

    averange_1 = (a1 + b1 + c1 - maximum_1 - minimum_1);

    printf("Trojuhelnik #2:\n");
    if( scanf(" %c", &first_first_sign) != 1 || (first_first_sign != 'S' && first_first_sign != 'U') ) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if(first_first_sign == 'S') {
            if(scanf("%c%c", &second_second_sign, &third_third_sign) != 2 || (second_second_sign != 'S' && second_second_sign != 'U') ) {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if( third_third_sign == 'U') {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if(second_second_sign == 'S') {    //SSS
                if( scanf(" %lf %lf %lf", &a2, &b2, &c2) != 3 || a2 <= 0 || b2 <= 0 || c2 <= 0) {
                    printf("Nespravny vstup.\n");
                    return 1;
                }

                if( (a2 + b2) - c2 <= DBL_EPSILON * fmax(fabs(a2 + b2), fabs(c2)) ||
                    (a2 + c2) - b2 <= DBL_EPSILON * fmax(fabs(a2 + c2), fabs(b2)) ||
                    (c2 + b2) - a2 <= DBL_EPSILON * fmax(fabs(c2 + b2), fabs(a2)) ) {
                        printf("Vstup netvori trojuhelnik.\n");
                        return 0;
                }
            } else if(second_second_sign == 'U') { //SUS
                if( scanf(" %lf %lf %lf", &a2, &first_first_angle, &c2) != 3 || a2 <= 0 || first_first_angle <= 0 || first_first_angle >= 180 || c2 <= 0) {
                    printf("Nespravny vstup.\n");
                    return 1;
                }

            }

    } else if(first_first_sign == 'U') {

            if(scanf("%c%c", &second_second_sign, &third_third_sign) != 2 || second_second_sign != 'S' || third_third_sign != 'U') {
                printf("Nespravny vstup.\n");
                return 1;
            }

            //USU
            if( scanf(" %lf %lf %lf", &first_first_angle, &b2, &second_second_angle) != 3 || (first_first_angle <= 0 || first_first_angle >= 180)
                                                                             || (second_second_angle <= 0 || second_second_angle >= 180)
                                                                             || b2 <= 0  ) {
                printf("Nespravny vstup.\n");
                return 1;
            }

            if( (first_first_angle >= 90 && second_second_angle >= 90) ||
                (second_second_angle >= 90 && first_first_angle >= 90) ) {
                    printf("Vstup netvori trojuhelnik.\n");
                    return 0;
            }

    }

    maximum_2 = fmax(a2 , b2);
    maximum_2 = fmax(maximum_2, c2);

    minimum_2 = fmin(a2, b2);
    minimum_2 = fmin(minimum_2, c2);

    averange_2 = (a2 + b2 + c2 - maximum_2 - minimum_2);

    double k1, k2, k3;


    //from 'USU' into 'SSS' FOR FIRST TR
    if(first_sign == 'U' && second_sign == 'S' && third_sign == 'U') {

        //double maximum_left, minimum_left, averange_left;

        double side_1, side_3;
        double uhel_2 = 180 - first_angle - second_angle;
        side_1 = (b1 * sin(first_angle * (M_PI / 180))) / sin(uhel_2 * (M_PI / 180));
        side_3 = (b1 * sin(second_angle * (M_PI / 180))) / sin(uhel_2 * (M_PI / 180));

        maximum_1 = fmax(side_1 , b1);
        maximum_1 = fmax(maximum_1, side_3);

        minimum_1 = fmin(side_1, b1);
        minimum_1 = fmin(minimum_1, side_3);

        averange_1 = (side_1 + b1 + side_3 - maximum_1 - minimum_1);


    }


    //from 'USU' into 'SSS' FOR SECOND TR
    if(first_first_sign == 'U' && second_second_sign == 'S' && third_third_sign == 'U') {
       

        double side_side_1, side_side_3;
        double uhel_uhel_2 = 180 - first_first_angle - second_second_angle;
        side_side_1 = (b2 * sin(first_first_angle * (M_PI / 180))) / sin(uhel_uhel_2 * (M_PI / 180));
        side_side_3 = (b2 * sin(second_second_angle * (M_PI / 180))) / sin(uhel_uhel_2 * (M_PI / 180));

        maximum_2 = fmax(side_side_1 , b2);
        maximum_2 = fmax(maximum_2, side_side_3);

        minimum_2 = fmin(side_side_1, b2);
        minimum_2 = fmin(minimum_2, side_side_3);

        averange_2 = (side_side_1 + b2 + side_side_3 - maximum_2 - minimum_2);

    }


    //from 'SUS' int 'SSS' FOR FIRST TR
    if(first_sign == 'S' && second_sign == 'U' && third_sign == 'S') {
        double side_2;
        side_2 = sqrt( a1 * a1 + c1 * c1 - 2 * a1 * c1 * cos(first_angle * (M_PI / 180)) );

        maximum_1 = fmax(a1 , side_2);
        maximum_1 = fmax(maximum_1, c1);

        minimum_1 = fmin(a1, side_2);
        minimum_1 = fmin(minimum_1, c1);

        averange_1 = (a1 + side_2 + c1 - maximum_1 - minimum_1);

        k1 = fmax(maximum_1, maximum_2) / fmin(maximum_1, maximum_2); 
        k2 = fmax(minimum_1, minimum_2) / fmin(minimum_1, minimum_2);
        k3 = fmax(averange_1, averange_2) / fmin(averange_1, averange_2);
    }


    //from 'SUS' int 'SSS' FOR SECOND TR
    if(first_first_sign == 'S' && second_second_sign == 'U' && third_third_sign == 'S') {
        double side_side_2;
        side_side_2 = sqrt( a2 * a2 + c2 * c2 - 2 * a2 * c2 * cos(first_first_angle * (M_PI / 180)) );

        maximum_2 = fmax(a2 , side_side_2);
        maximum_2 = fmax(maximum_2, c2);

        minimum_2 = fmin(a2, side_side_2);
        minimum_2 = fmin(minimum_2, c2);

        averange_2 = (a2 + side_side_2 + c2 - maximum_2 - minimum_2);

    }
    

        k1 = fmax(maximum_1, maximum_2) / fmin(maximum_1, maximum_2); 
        k2 = fmax(minimum_1, minimum_2) / fmin(minimum_1, minimum_2);
        k3 = fmax(averange_1, averange_2) / fmin(averange_1, averange_2);

        if( fabs(minimum_1 - minimum_2) <= 100 * DBL_EPSILON * fmax(fabs(minimum_1), fabs(minimum_2)) &&
            fabs(maximum_1 - maximum_2) <= 100 * DBL_EPSILON * fmax(fabs(maximum_1), fabs(maximum_2)) &&
            fabs(averange_1 - averange_2) <= 100 * DBL_EPSILON * fmax(fabs(averange_1), fabs(averange_2)) ) {
                printf("Trojuhelniky jsou shodne.\n");
                return 0;
        } else if( fabs(k1 - k2) <= 100 * DBL_EPSILON * fmax(fabs(k1), fabs(k2)) && 
                   fabs(k2 - k3) <= 100 * DBL_EPSILON * fmax(fabs(k2), fabs(k3)) &&
                   fabs(k1 - k3) <= 100 * DBL_EPSILON * fmax(fabs(k1), fabs(k3))    ) {
                printf("Trojuhelniky nejsou shodne, ale jsou podobne.\n");
                return 0;
        } else {
            printf("Trojuhelniky nejsou shodne ani podobne.\n");
        }
    return 0;
}