#include <stdio.h>
#include <math.h>
#include <float.h>

int main(void) {
    double a1, b1, c1, a2, b2, c2;
    double averange_1, averange_2, maximum_1, maximum_2, minimum_1, minimum_2; 

    printf("Trojuhelnik #1:\n");
    if( scanf(" %lf %lf %lf", &a1, &b1, &c1) != 3 || a1 <= 0 || b1 <= 0 || c1 <= 0) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if( (a1 + b1) - c1 <= DBL_EPSILON * fmax(fabs(a1 + b1), fabs(c1)) ||
        (a1 + c1) - b1 <= DBL_EPSILON * fmax(fabs(a1 + c1), fabs(b1)) ||
        (c1 + b1) - a1 <= DBL_EPSILON * fmax(fabs(c1 + b1), fabs(a1)) ) {
        printf("Strany netvori trojuhelnik.\n");
        return 0;
    }

    maximum_1 = fmax(a1 , b1);
    maximum_1 = fmax(maximum_1, c1);

    minimum_1 = fmin(a1, b1);
    minimum_1 = fmin(minimum_1, c1);

    averange_1 = (a1 + b1 + c1 - maximum_1 - minimum_1);

    printf("Trojuhelnik #2:\n");
    if( scanf(" %lf %lf %lf", &a2, &b2, &c2) != 3 || a2 <= 0 || b2 <= 0 || c2 <= 0) {
        printf("Nespravny vstup.\n");
        return 0;
    }

    if( (a2 + b2) - c2 <= DBL_EPSILON * fmax(fabs(a2 + b2), fabs(c2)) ||
        (a2 + c2) - b2 <= DBL_EPSILON * fmax(fabs(a2 + c2), fabs(b2)) ||
        (c2 + b2) - a2 <= DBL_EPSILON * fmax(fabs(c2 + b2), fabs(a2)) ) {
        printf("Strany netvori trojuhelnik.\n");
        return 0;
    }


    maximum_2 = fmax(a2 , b2);
    maximum_2 = fmax(maximum_2, c2);

    minimum_2 = fmin(a2, b2);
    minimum_2 = fmin(minimum_2, c2);

    averange_2 = (a2 + b2 + c2 - maximum_2 - minimum_2);

    double k1, k2, k3;

    k1 = fmax(maximum_1, maximum_2) / fmin(maximum_1, maximum_2); 
    k2 = fmax(minimum_1, minimum_2) / fmin(minimum_1, minimum_2);
    k3 = fmax(averange_1, averange_2) / fmin(averange_1, averange_2);

    if( fabs(minimum_1 - minimum_2) < DBL_EPSILON * fmax(fabs(minimum_1), fabs(minimum_2)) &&
        fabs(maximum_1 - maximum_2) < DBL_EPSILON * fmax(fabs(maximum_1), fabs(maximum_2)) &&
        fabs(averange_1 - averange_2) < DBL_EPSILON * fmax(fabs(averange_1), fabs(averange_2)) ) {
            printf("Trojuhelniky jsou shodne.\n");
    } else if( fabs(k1 - k2) <= 100 * DBL_EPSILON * fmax(fabs(k1), fabs(k2)) && 
               fabs(k2 - k3) <= 100 * DBL_EPSILON * fmax(fabs(k2), fabs(k3)) &&
               fabs(k1 - k3) <= 100 * DBL_EPSILON * fmax(fabs(k1), fabs(k3))    ) {
            printf("Trojuhelniky nejsou shodne, ale jsou podobne.\n");
    } else {
        printf("Trojuhelniky nejsou shodne ani podobne.\n");
    }


    // if( fabs(minimum_1 * averange_2 - minimum_2 * averange_1) < DBL_EPSILON * fmax(fabs(minimum_1 * averange_2), fabs(minimum_2 * averange_1)) &&
    //     fabs(averange_1 * maximum_2 - averange_2 * maximum_1) < DBL_EPSILON * fmax(fabs(averange_1 * maximum_2), fabs(averange_2 * maximum_1)) &&
    //     fabs(minimum_1 * maximum_2 - minimum_2 * maximum_1) < DBL_EPSILON * fmax(fabs(minimum_1 * maximum_2), fabs(minimum_2 * maximum_1)) ) {
    //         pricntf("Trojuhelniky jsou shodne.\n");
    // } 

    return 0;
}