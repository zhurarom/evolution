#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LIST_BY_YEAR       0
#define LIST_BY_TYPE       1

#define TYPE_MAX           100
#define SETUP_MAX          100

typedef struct TEngine
{
  struct TEngine * m_Next;
  struct TEngine * m_Prev;
  int              m_Year;
  char             m_Type  [ TYPE_MAX ];
  int              m_Setup [ SETUP_MAX ];
} TENGINE;

typedef struct TArchive
{
  struct TArchive * m_Next;
  struct TArchive * m_Prev;
  TENGINE         * m_Engines;
} TARCHIVE;

TENGINE          * createEngine                            ( const char      * type,
                                                             int               year )
{
  TENGINE * res = (TENGINE *) malloc ( sizeof  (*res ) );
  res -> m_Next = NULL;
  res -> m_Prev = NULL;
  res -> m_Year = year;
  strncpy ( res -> m_Type, type, sizeof ( res -> m_Type ) );
  for ( int i = 0; i < SETUP_MAX; i ++ )
    res -> m_Setup[i] = 0;
  return res;
}
#endif /* __PROGTEST__ */

//Return old or new root
TENGINE* insertEngineByType(TENGINE * root, TENGINE * engine) {
  TENGINE * prev = root;
  TENGINE * next = root->m_Next;

  //Insert before root
  if( strcmp(prev->m_Type, engine->m_Type) >= 0 ) {
    engine->m_Next = root;
    root->m_Prev = engine;
    engine->m_Prev = NULL;

    root = engine;
    return root;
  }

  //Insert middle
  while(next != NULL) {
      if( strcmp(prev->m_Type, engine->m_Type) < 0 &&
          strcmp(next->m_Type, engine->m_Type) >= 0) {
            prev->m_Next = engine;
            engine->m_Prev = prev;
            engine->m_Next = next;
            next->m_Prev = engine;

            return root;
      }
    prev = prev->m_Next;
    next = next->m_Next;
  }

  //Insert the end
  if( strcmp(prev->m_Type, engine->m_Type) < 0 ) {
    prev->m_Next = engine;
    engine->m_Prev = prev;
    return root;
  }

  return NULL;
}

//Return old or new root
TENGINE* insertEngineByYear(TENGINE * root, TENGINE * engine) {
  TENGINE * prev = root;
  TENGINE * next = root->m_Next;

  //Insert before root
  if( prev->m_Year >= engine->m_Year ) {
    engine->m_Next = root;
    root->m_Prev = engine;
    engine->m_Prev = NULL;

    root = engine;
    return root;
  }

  //Insert middle
  while(next != NULL) {
      if( prev->m_Year < engine->m_Year &&
          next->m_Year >= engine->m_Year) {
            prev->m_Next = engine;
            engine->m_Prev = prev;
            engine->m_Next = next;
            next->m_Prev = engine;

            return root;
      }
    prev = prev->m_Next;
    next = next->m_Next;
  }

  //Insert the end
  if( prev->m_Year < engine->m_Year ) {
    prev->m_Next = engine;
    engine->m_Prev = prev;
    return root;
  }

  return NULL;
}


TARCHIVE * AddByYear(TARCHIVE * list, TENGINE * engine) {

  //We don`t have node for TARCHIVE(free list TARCHIVE)
  if(list == NULL) {

    list = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    list->m_Next = NULL;
    list->m_Prev = NULL;
    list->m_Engines = engine;

    return list;
  }

  //Insert before root
  if(list->m_Engines->m_Year > engine->m_Year) {
    TARCHIVE * the_first;

    the_first = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    the_first->m_Next = list;
    the_first->m_Prev = NULL;
    list->m_Prev = the_first;

    list = the_first;
    list->m_Engines = engine;

    return list;
  } else if(list->m_Engines->m_Year == engine->m_Year) {
    list->m_Engines = insertEngineByType(list->m_Engines, engine);
    return list;
  }


  TARCHIVE * prev = list;
  TARCHIVE * next = list->m_Next;

  //Insert middle
  while(next != NULL) {

    if(prev->m_Engines->m_Year < engine->m_Year && 
       engine->m_Year < next->m_Engines->m_Year) {
        
        TARCHIVE * middle;

        middle = (TARCHIVE *)malloc( sizeof(TARCHIVE) );

        prev->m_Next = middle;
        middle->m_Prev = prev;
        middle->m_Next = next;
        next->m_Prev = middle;

        middle->m_Engines = engine;

        return list;
    } else if(prev->m_Engines->m_Year == engine->m_Year) {
      prev->m_Engines = insertEngineByType(prev->m_Engines, engine);
      return list;
    }

    prev = prev->m_Next;
    next = next->m_Next;

  }

  //Insert the end
  if( prev->m_Engines->m_Year < engine->m_Year) {
    TARCHIVE * the_last;

    the_last = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    the_last->m_Next = NULL;
    the_last->m_Prev = prev;
    prev->m_Next = the_last;

    the_last->m_Engines = engine;

    return list;  
  }else if(prev->m_Engines->m_Year == engine->m_Year) {
    prev->m_Engines = insertEngineByType(prev->m_Engines, engine);
    return list;
  }

  return NULL;
}

TARCHIVE * AddByType(TARCHIVE * list, TENGINE * engine) {
  //We don`t have node for TARCHIVE(free list TARCHIVE)
  if(list == NULL) {

    list = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    list->m_Next = NULL;
    list->m_Prev = NULL;
    list->m_Engines = engine;

    return list;
  }

  //Insert before root
  if( strcmp(list->m_Engines->m_Type, engine->m_Type) > 0 ) {
    TARCHIVE * the_first;

    the_first = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    the_first->m_Next = list;
    the_first->m_Prev = NULL;
    list->m_Prev = the_first;

    list = the_first;
    list->m_Engines = engine;

    return list;
  } else if( strcmp(list->m_Engines->m_Type, engine->m_Type) == 0) {
    list->m_Engines = insertEngineByYear(list->m_Engines, engine);
    return list;
  }


  TARCHIVE * prev = list;
  TARCHIVE * next = list->m_Next;

  //Insert middle
  while(next != NULL) {

    if( strcmp(prev->m_Engines->m_Type, engine->m_Type) < 0 && 
        strcmp(engine->m_Type, next->m_Engines->m_Type) < 0) {
        
        TARCHIVE * middle;

        middle = (TARCHIVE *)malloc( sizeof(TARCHIVE) );

        prev->m_Next = middle;
        middle->m_Prev = prev;
        middle->m_Next = next;
        next->m_Prev = middle;
        middle->m_Engines = engine;

        return list;
    } else if(strcmp(prev->m_Engines->m_Type, engine->m_Type) == 0) {
      prev->m_Engines = insertEngineByYear(prev->m_Engines, engine);
      return list;
    }

    prev = prev->m_Next;
    next = next->m_Next;

  }

  //Insert the end
  if( strcmp(prev->m_Engines->m_Type, engine->m_Type) < 0) {
    TARCHIVE * the_last;

    the_last = (TARCHIVE *)malloc( sizeof(TARCHIVE) );
    the_last->m_Next = NULL;
    prev->m_Next = the_last;
    the_last->m_Prev = prev;

    the_last->m_Engines = engine;

    return list;  
  }else if(strcmp(prev->m_Engines->m_Type, engine->m_Type) == 0) {
    prev->m_Engines = insertEngineByYear(prev->m_Engines, engine);
    return list;
  }

  return NULL;
}


TARCHIVE         * AddEngine                               ( TARCHIVE        * list,
                                                             int               listBy,
                                                             TENGINE         * engine )
{
  return listBy == LIST_BY_TYPE ? AddByType(list, engine) : AddByYear(list, engine);
}

void DelArchive( TARCHIVE * list ) {

  if(!list) return;

  TARCHIVE *  help_archive = list->m_Next;
  TENGINE  *  help_engine = list->m_Engines->m_Next;

  while( list != NULL) {
    while( help_engine != NULL ) {
      free(list->m_Engines);
      list->m_Engines = help_engine;
      help_engine = help_engine->m_Next;
    }
    free(list->m_Engines);

    free(list);
    list = help_archive;
    if (help_archive) help_archive = help_archive->m_Next;
    if (list) help_engine = list->m_Engines->m_Next;
  }
}


void PrintArchive(TARCHIVE * list) {
  if (!list) return;

  printf("------------------------------------------------------------------------\n");

  TARCHIVE *  help_archive = list;
  TENGINE  *  help_engine;

  while( help_archive != NULL) {
    help_engine = help_archive->m_Engines;
    printf("ARCHIVE:");
    while( help_engine != NULL ) {
      printf("%d %s, ", help_engine->m_Year, help_engine->m_Type);
      help_engine = help_engine->m_Next;
    }
    help_archive = help_archive->m_Next;
    printf("\n");
  }

  printf("------------------------------------------------------------------------\n");

}


TARCHIVE         * ReorderArchive                          ( TARCHIVE        * list,
                                                             int               listBy )
{
  TARCHIVE *  help_archive = list;
  TENGINE  *  help_engine;

  TARCHIVE * new_archive = NULL;

  while( help_archive != NULL) {
    help_engine = help_archive->m_Engines;    
    while( help_engine != NULL ) {
      help_archive->m_Engines = help_engine->m_Next;
      if (help_engine->m_Next) help_engine->m_Next->m_Prev = NULL;
      help_engine->m_Next = NULL;
      new_archive = AddEngine(new_archive, listBy, help_engine);
      help_engine = help_archive->m_Engines;
    }
    help_archive = help_archive->m_Next;
    free(list);
    list = help_archive;
  }

  return new_archive;
}

#ifndef __PROGTEST__
int                main                                    ( int               argc,
                                                             char            * argv [] )
{
     

  return 0;
}
#endif /* __PROGTEST__ */