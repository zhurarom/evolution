#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#define MAX_COUNT 200000
#define ZAJEBIS 1
#define CHUJEVO 0

typedef struct Layer {
    int volume;
    int altitude;
    int area;
    int current_area;
}Layer;

typedef struct Stack_of_Layers {
    Layer * layers[MAX_COUNT];
    int volume;
    int count_of_layers;
}Stack_of_Layers;


void freeStack(Stack_of_Layers *stack) {
    for(int i = 0; i < stack->count_of_layers; i++) {
        free(stack->layers[i]);
    }

    stack->volume = 0;
    stack->count_of_layers = 0;
}


void findVolume(Stack_of_Layers *stack) {

    int volume_of_one_block = 0, new_area = 0;
    int new_altitude;

    for(int i = 0; i < stack->count_of_layers - 1; i++) {
        new_area += stack->layers[i]->area;

        stack->layers[i]->current_area = new_area;

        new_altitude = stack->layers[i + 1]->altitude - stack->layers[i]->altitude;
        volume_of_one_block += new_area * abs(new_altitude);

        stack->layers[i + 1]->volume = volume_of_one_block;
    }

}

int sortVolume(Stack_of_Layers *stack, int water, unsigned int *finalPosition) {
    int start = 0;
    int middle = 0;
    int finish = stack->count_of_layers - 1;

    while(start <= finish) {
        middle = (start + finish) >> 1;

        if(water > stack->layers[middle]->volume) {
            start = middle + 1;
        } else if(water < stack->layers[middle]->volume) {
            finish = middle - 1;
        } else {
            *finalPosition = middle;
            return ZAJEBIS; //we find it
        }
    }
    
    *finalPosition = start;
    return CHUJEVO;
}


float findNewAltitude(Stack_of_Layers *stack, int water) {
    unsigned int index = 0;

    sortVolume(stack, water, &index);

    index--;

    float new_h = ((float)water - stack->layers[index]->volume) / stack->layers[index]->current_area;
    return stack->layers[index]->altitude + new_h;
}

int readVolumeOfWater(Stack_of_Layers *stack, int water) {
    printf("Zadejte objem vody:\n");
    while((scanf(" %d", &water)) == 1) {
        if(water < 0) {
            freeStack(stack);
            return CHUJEVO;
        }

        if(water == 0) {
            printf("Prazdne.\n");
        } else if(water > stack->volume) {
            printf("Pretece.\n");
        } else {
            float answer = findNewAltitude(stack, water);
            printf("h = %f\n", answer);
        }

    }

    if( !feof(stdin) ) {
        freeStack(stack);
        return CHUJEVO;
    }

    return ZAJEBIS;
}

int readCountOfTanks(int *count_of_tanks) {
    printf("Zadejte pocet nadrzi:\n");
     if( scanf(" %d" , count_of_tanks) != 1 || *count_of_tanks <= 0 || *count_of_tanks > MAX_COUNT ) {
        return CHUJEVO;
    }
    return ZAJEBIS;
}


//O(logn)
//We will return a posiiton for insert
int sortAltitude(Stack_of_Layers *stack, int altitude, int *finalPosition) {
    int start = 0;
    int middle = 0;
    int finish = stack->count_of_layers - 1;

    while(start <= finish && stack->count_of_layers) {
        middle = (start + finish) >> 1;

        if(altitude > stack->layers[middle]->altitude) {
            start = middle + 1;
        } else if(altitude < stack->layers[middle]->altitude) {
            finish = middle - 1;
        } else {
            *finalPosition = middle;
            return ZAJEBIS; //we find it
        }
    }
    
    *finalPosition = start;
    return CHUJEVO;
}

int readOneLayer(Stack_of_Layers *stack) {
    int altitude, h, w, d;
    int finalPosition;

    if(scanf(" %d %d %d %d", &altitude, &h, &w, &d) != 4 || h <= 0 
                                                         || w <= 0
                                                         || d <= 0) {
            return CHUJEVO;
    }

    Layer *bottom = (Layer * )malloc(sizeof(*bottom));
    Layer *top = (Layer * )malloc(sizeof(*top));

    //Initialize of a bottom level
    bottom->altitude = altitude;
    bottom->volume = 0;
    bottom->current_area = 0;
    bottom->area = w * d;

    //Initialize of a top level
    top->altitude = altitude + h;
    top->volume = 0;
    top->current_area = 0;
    top->area = -1 * w * d;     //for difference

    if(!sortAltitude(stack, bottom->altitude, &finalPosition)) {
        //for changing of positions
        for(int i = stack->count_of_layers; i > finalPosition; i--) {
            stack->layers[i] = stack->layers[i - 1];
        }
        stack->layers[finalPosition] = bottom;
        stack->count_of_layers++;

    } else {
        stack->layers[finalPosition]->area += bottom->area;
        free(bottom);
    }

    if(!sortAltitude(stack, top->altitude, &finalPosition)) {
        //for changing of positions
        for(int i = stack->count_of_layers; i > finalPosition; i--) {
            stack->layers[i] = stack->layers[i - 1];
        }
        stack->layers[finalPosition] = top;
        stack->count_of_layers++;
    } else {
        stack->layers[finalPosition]->area += top->area;
        free(top);
    }


    stack->volume += w * h * d;

    return ZAJEBIS;
}

void printBuffer(const Stack_of_Layers * buffer) {
    unsigned int i;
    printf("Buffer has %d records with %d total volume\n", buffer->count_of_layers, buffer->volume);
    printf("I\tALT\tAREA\tTCUR_AREA\tTVOL\n");
    for (i = 0; i < buffer->count_of_layers; i++) {
        printf("%d\t %d\t %d\t %d\t\t %d\n", i, buffer->layers[i]->altitude, buffer->layers[i]->area, buffer->layers[i]->current_area, buffer->layers[i]->volume);
    }
}



int readAllLayers(Stack_of_Layers * stack, int count_of_tanks) {
    printf("Zadejte parametry nadrzi:\n");
    for(int i = 0; i < count_of_tanks; i++) {
        if(!readOneLayer(stack)) {
            return CHUJEVO;
        }
    }
    return ZAJEBIS;
}
int main() {
    int count_of_tanks = 0;
    Stack_of_Layers stack;
    int water = 0;

    if(!readCountOfTanks(&count_of_tanks)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO;
    }

    stack.volume = 0;
    stack.count_of_layers = 0;

    if(!readAllLayers(&stack, count_of_tanks)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO;
    }

    printBuffer(&stack);

    findVolume(&stack);

    printf("\n");
    printBuffer(&stack);


    if(!readVolumeOfWater(&stack, water)) {
        printf("Nespravny vstup.\n");
        return CHUJEVO;
    }

    freeStack(&stack);
    return ZAJEBIS;
}