#include <stdio.h>
#include <string.h>

#define MAX 500000

// int fillArray(int * array, int *len_array) {
//     int x;
//     int i;

//     printf("Zadejte delky:\n");
//     for(i = 0; i < MAX; i++) {
//         if( scanf("%d", &x) !=1 || x <= 0 ) {
//             printf("Nespravny vstup.\n");
//             return 0;
//         }
//         array[i] = x;
//     }    

//     *len_array = i;
//     return 1;
// }

int findPriceBinaryRight(int * array, int len_array, int *final_price_right);

int findPriceBinaryLeft(int * array, int len_array, int *final_price_left) {
    int lo = 0; 
    int hi = len_array - 1; 
    int mid; 
    int neighbor_price = 0;
    int help_count = 0;

    while(lo < hi) {
        for(int i = lo; i <= hi; i++) {
            neighbor_price += array[i];
        }
        printf("PRICE_LEFT : %d\n", neighbor_price);
        mid = (lo + hi) >> 1;
        hi = mid;

        if(mid != mid - 1 && hi != hi - 1) {

            findPriceBinaryRight(array, len_array, &help_count);
            printf("INSIDE OF LEFT: %d\n", help_count);
        }

    }

    *final_price_left = neighbor_price;    
    return 0;
}


int findPriceBinaryRight(int * array, int len_array, int *final_price_right) {
    int lo = 0;
    int hi = len_array - 1;
    int mid = (lo + hi) >> 1;
    int neighbor_price = 0;

    lo = mid;

    while(lo != hi - 1) {
        for(int i = lo + 1; i <= hi; i++) {
            neighbor_price += array[i];
        }
        printf("PRICE_RIGHT : %d\n", neighbor_price);
        mid = (lo + hi) >> 1;
        lo = mid;
    }

    *final_price_right = neighbor_price;    
    return 0;
}

int sortInt(int *a, int *b) {
    return (*b < *a) - (*a < *b);
}


int fillArray(int * array, int *len_array) {
    int x;
    int counter_inside = 0;

    printf("Zadejte delky:\n");
    while(scanf("%d", &x) == 1) {
        if(x <= 0 || counter_inside >= MAX) {
            printf("Nespravny vstup1.\n");
            return 0;
        }
        array[counter_inside] = x;
        counter_inside++;
    }

    if(!feof(stdin)) {
        printf("Nespravny vstup2.\n");
        return 1;
    }

    *len_array = counter_inside;
    return 1;
}

void printArray(int * array, int len_array) {
    for(int i = 0; i < len_array; i ++) {
        printf("%d-----", array[i]);
    }
    printf("\n");
}

int main() {
    int array[MAX];
    int x = 0;
    int len_array = 0;
    int final_price_right = 0;
    int final_price_left = 0;

    fillArray(array, &len_array);

    printf("Counter: %d\n", len_array);

    //qsort( array, len_array, sizeof(*array), (int(*)(const void *, const void *))sortInt ); 

    printf("After sort:\n");
    printArray(array, len_array);

    findPriceBinaryLeft(array, len_array, &final_price_left);
    findPriceBinaryRight(array, len_array, &final_price_right);

    printf("Final: %d\n", final_price_left + final_price_right);

    return 0;
}