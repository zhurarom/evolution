# The Drake Game (2019)

A board game in Java (OOP design principles, JUnit, immutability, JSON serialization, JavaFX UI)
The Java Programming Course introduces students to object-oriented programming in the Java programming language. In addition to the language itself, basic libraries for work with files, streams, networks, collections, databases, and multi-threaded programs will be processed.

 ## Goal
  Goal of this subject will be to create Drake game in Java. Rules of the game can be found [here](https://gitlab.fit.cvut.cz/zhurarom/evolution/blob/master/Java/game_rules.pdf).


